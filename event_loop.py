from __future__ import print_function
################################################################################
# Event Loop
from threading import Thread
from Queue import Queue, Empty
import logging

#TODO EventLoop should be shut down automatically when the EventQueue is Empty and no Active Entity are registered
class EventLoop(Thread):
    __INIT_EVENT__ = "__INIT_EVENT__"

    """Event loop for event based infrastructures"""
    def __init__(self, ee, name = "Event Loop"):
        super(EventLoop, self).__init__(target=self.run, name=name, kwargs={})

        self.ee = ee
        self.ee.on('*', self._emit)

        self._event_queue = Queue()
        self.ee.emit(EventLoop.__INIT_EVENT__) # push first event

        self._stop = False
        self.application = {}

    def stop(self):
    	logging.info("[__EVENT_LOOP__] now exiting.")
        self._stop = True

    def run(self):
        while not self._stop:
            try:
                # try to retrieve an event from the event queue
                # (blocking behaviour with timeout to check periodically if
                # stop has been requested)
                context, args, kwargs = self._event_queue.get(block=True, timeout=5)

                # execute behaviour associated with all events (* wildcard)
                for callback in self.application['*']:
                    callback(context, *args, **kwargs)

                # execute behaviour associated with the event
                for callback in self.application[context['event']]:
                    callback(context, *args, **kwargs)
            except KeyError:
                # unhandled event, ignore it
                pass
            except Empty:
                # timeout, continue iteration checking the stop flag
                logging.debug("[Event Loop - {}]: Empty".format(self.name))
                pass
            except Exception as err:
                # show exception
                import traceback
                import inspect
                print(inspect.getsourcelines(callback))
                print(args, kwargs)
                traceback.print_exc()
                self.stop()

    # put in queue the event
    def emit(self, event, *args, **kwargs):
        self.ee.emit(event, *args, **kwargs)

    def _emit(self, context, *args, **kwargs):
        logging.debug("[Event Loop - {}]: received event {}".format(self.name, context['event']))

        self._event_queue.put((context, args, kwargs))

    # register callback on application
    def on(self, event, callback):
        if event not in self.application:
            self.application[event] = []

        self.application[event].append(callback)

    # unregister callback on application
    def off(self, event, callback):
        try:
            self.application[event].remove(callback)
        except KeyError:
            # appication[event] not defined
            pass
        except ValueError:
            # value not in application[event] list
            pass
################################################################################
