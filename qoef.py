from __future__             import absolute_import

from .main                  import qoef_main, banner
from .utils                 import notices


class Qoef(): # Master process
    slaves = [] # slaves are running your experiments. They can be remote. They use your Application code and connect to the Master process

    def __init__(self, 
                event_loop, #
                network_constrainer, # defaults to Tc
                data_source # point generator (random, FAST)
                ):
        self.ee = event_loop
        self.nc = network_constrainer
        self.ds = data_source
    
    def start(self, *args, **kargs):
        banner()
        qoef_main(args, kargs,
                 event_loop = self.ee,
                 network_constrainer = self.nc,
                 data_source = self.ds
                 ) # starts core services and waits for a client

    #### REGITER BLOCK ####
    # register_ functions can only be called *before* Qoef is started
    def _register_event(): #TODO
        pass
                
    def _register_probe(): #TODO class that launches commands that don't affect the experiment but may check if it's applied correctly
        pass
        
    def _register_probe_required(): #TODO same as before, but its tests must return True for the current experiment to be validated
        pass
    
    #### SET BLOCK ####
    # set_ functions can be called *before or while* Qoef is running
    def _set_application(): #TODO ex-Sampler class
        pass

    #### GET BLOCK ####
    # get_ functions can be called anytime
    @property
    def get_event_loop(self): #TODO class to which Event loops are given READ access
        return self.ee

