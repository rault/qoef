# active learner
from threading import Thread
import subprocess

import logging

#TODO support namespaces (see Vowpal Wabbit documentation)
def convert_to_vw_format(pt, meta = { 'tag': "'empty" }):
	line = ""

	if 'label' in pt and pt['label'] is not None:
		line = "{}".format(pt['label'])

	if 'importance' in meta and meta['importance'] is not None:
		line += " {:.8}".format(meta['importance'])

	#FIXME tag value is missing ' in empty sometimes (sending the point), since they are not in use tags are disabled
	# if 'tag' in meta and (meta['tag'] is not None or meta['tag'] != ''):
	# 	tag = meta['tag']
	# else:
	# 	tag = "'empty"

	#FIXME quickfix
	tag = "'empty"

	line += " %s" % tag

	line += " |"

	for key, value in pt.iteritems():
		if key in ['label', 'importance', 'tag'] or (key.startswith('__') and key.endswith('__')):
			continue

		try:
			line += " {}:{:.8}".format(key, value)
		except:
			line += " {}:{}".format(key, value)

	line += "\n"

	return line


class VowpalWabbitServer(object):
	"""docstring for VowpalWabbitServer"""
	def __init__(self, port, mellowness, name = "vowpal_wabbit_daemon"):
		super(VowpalWabbitServer, self).__init__()

		self.port 		= port
		self.mellowness = mellowness

		self._process = None
		self._th = Thread(target = self.run, name = name)

	def start(self):
	    self._th.start()

	def run(self):
		cmd = "vw --active --port {} --mellowness {}".format(self.port, self.mellowness)
                print cmd.split(' ')
		self._process = subprocess.Popen(cmd.split(' '))
		self._process.wait()

	def stop(self):
		if self._process is not None:
			self._process.kill()
			self._process = None

	def join(self):
	    try:
	        self._th.join()
	    except:
	        pass


#TODO need refactoring, implementation is not coherent (bad implementation of the with_details flag and how details are stored)
class VowpalWabbitActiveLearner(object):
	"""
	ActiveLearner

	@arg socket Socket socket to communicate with a Vowpal Wabbit daemon
	@arg point_generator Iterable<Point> iterable source of points to submit to the Vowpal Wabbit daemon
	"""
	def __init__(self, socket, point_generator, with_details = False):
		super(VowpalWabbitActiveLearner, self).__init__()
		self.socket 			= socket
		self.with_details 		= with_details
		self.point_generator 	= point_generator

		self.details = None

	#TODO define as iterable?
	# implement:
	# def __iter__(self):
	# 	return self
	#
	# def next(self):
	#	#TODO

	"""
	Get a point that is requested to be labeled by the Vowpal Wabbit daemon (accepted point)

	@arg with_details boolean flag that change the result of the call (Tuple or Point)

	@returns <(Point, Details) or Point> returns the point (if with_details is False) or a tuple with Point and Details (prediction, tag, importance) if with_details is True
	"""
	#TODO with_details flag should be here
	def get_point(self):
		for point in self.point_generator:
			# end of stream
			#FIXME point generator should be a real generator and trigger and exception when there are no more data
			if point is None:
				return None

			# send point to remote active learner
			response = self._send_proposal(point)

			if response['is_accepted']:
				if self.with_details:
					return (point, response)
				else:
					self.details = response # save meta information of the point
					return point
			else:
				# point not accepted: remove saved meta information
				self.details = None

		# no more points...
		return None

	"""
	Send a proposal (point), saves the details of the response
	"""
	#TODO add details to proposal
	def _send_proposal(self, point):
		# send point
		raw_response = self._send_point(point)

		# check if point has been accepted (if unlabeled)
		response = self._check_accepted(raw_response)

		return response

	"""
	@returns Vowpal Wabbit raw response
	"""
	def _send_point(self, point, details = None):
		# convert point to Vowpal Wabbit format
		if details is None:
			line = convert_to_vw_format(point)
		else:
			line = convert_to_vw_format(point, details)

		# send point to remote active learner
		logging.debug("[Vowpal Wabbit]: sending line\n{}".format(line))
		self.socket.sendall(line)

		# wait for response
		raw_response = self._receive_response()

		return raw_response

	"""
	Function to send point to the Vowpal Wabbit daemon (inject knowledge)
	"""
	#TODO and manually inject proposals
	def submit_point(self, point, details = None):
		# check if labeled point
		if not('label' in point) or (point['label'] is None):
			# if not return (this function is made only to send labeled points) #TODO remove (do opposite check)
			return
	    #TODO send suggestion manually and handle response
	    # else
	        # self._send_proposal(point)

		# take stored meta information about the point to submit
		if not self.with_details:
			details = self.details

		# send point
		self._send_point(point, details)

	    #TODO return response

	def _check_accepted(self, res):
		# check if suggested point has been accepted by active learner
		res_list = res.split(' ')

		if len(res_list) == 2:
			return { 'is_accepted': False }

		prediction, tag, importance = res_list

		try:
			importance = float(importance)

			response = {
				'is_accepted': 	True,
				'prediction': 	prediction,
				'tag': 			tag,
				'importance': 	importance
			}
		except:
			# not valid importance, skip
			response = { 'is_accepted': False }

		return response

	def _receive_response(self, buffer_size = 256):
		buf = self.socket.recv(buffer_size)
		ret = len(buf)
		while ret > 0 and len(buf) < buffer_size:
			if buf[-1] == '\n':
				break

			tmp = self.socket.recv(buffer_size)
			ret = len(tmp)
			buf = buf + tmp
		return buf
