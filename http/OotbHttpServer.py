# core
from threading import Thread

# log setup
from app.setup_app_logging import setup_logger

# http server
import bottle
from rocket import Rocket


"""
Initialize an Out-Of-The-Box Http Server that provides files in a basic folder structure:
/index.html
/js/<scripts>
/css/<style,fonts>
/img/<images>

The returned object has to be started (call start()) to be active

@arg host String
@arg port int
@arg app  WSGIApplication (optional) web server application scope (if not defined creates a new one)

@returns startable threaded Http Server
"""
class OotbHttpServer(object):
    def __init__(self, host, port, app = bottle.Bottle(), name = 'http_server'):
        self.host = host
        self.port = port
        self.name = name

        self.app = app
        
        #setup_logger('Rocket','rocket')

        self._init_app()

        self._server_impl = self._get_server_impl()

        self._th = Thread(target = self._server_impl.start, name = name)

    def start(self):
        self._th.start()

    def stop(self):
        self._server_impl.stop()

    def join(self):
        try:
            self._th.join()
        except:
            pass

    def _init_app(self):
        # index HTML page
        @self.app.route('/')
        @self.app.route('/index.htm')
        @self.app.route('/index.html')
        def index():
            return bottle.static_file('index.html', root = './www')

        # serve JS files
        @self.app.route('/js/<filepath:path>')
        def server_static_js(filepath):
            return bottle.static_file(filepath, root='./www/js')

        # serve CSS files
        @self.app.route('/css/<filepath:path>')
        def server_static_css(filepath):
            return bottle.static_file(filepath, root='./www/css')

        # serve Image files
        @self.app.route('/img/<filepath:path>')
        def server_static_img(filepath):
            return bottle.static_file(filepath, root='./www/img')

    def _get_server_impl(self):
        return Rocket((self.host, self.port), 'wsgi', { 'wsgi_app': self.app, 'debug': __debug__ })
