from threading import Thread

import subprocess

class MongoDbDaemon(object):
    """docstring for MongoDbDaemon"""
    def __init__(self, port, data_location, name = "mongodb_daemon"):
        super(MongoDbDaemon, self).__init__()
        self.port           = port
        self.data_location  = data_location

        self._process = None
        self._th = Thread(target = self.run, name = name)

    def start(self):
        self._th.start()

    def run(self):
        cmd = "mongod --port {} --dbpath {} --smallfiles".format(self.port, self.data_location)

        with open("log/mongodb.log", 'w') as logfile:
            self._process = subprocess.Popen(cmd.split(' '), universal_newlines=True, stdout=logfile)#open(os.devnull, 'w'))
            self._process.wait()

    def stop(self):
        if self._process is not None:
            self._process.kill()
            self._process = None

    def join(self):
        try:
            self._th.join()
        except:
            pass
