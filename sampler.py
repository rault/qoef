from automata import Automata, Graph, Transition, json_graph_builder
from event_dispatching import EventEmitter

from app.client import Client
from app.tester import Tester

from experimenter import Experimenter

from enum import Enum
import logging
from time import time

from copy import deepcopy

class Sampler_Events(Enum):
    START               = 1
    POINT_READY         = 2
    CLIENT_READY        = 3
    PRE_TEST_FAIL       = 4
    PRE_TEST_SUCCESS    = 5
    EXPERIMENT_START    = 6
    EXPERIMENT_FAIL     = 7
    EXPERIMENT_TIMEOUT  = 8
    SAMPLE_COLLECTED    = 9
    IRREVERSIBLE_ERROR  = 10
    END                 = 11
    NO_MORE_POINTS      = 12


class Sampler(object):
    Events = Sampler_Events

    def _get_behaviour(self):
        return {
            'states': [
                { '_id': "Start",                               'action': self._start,              'initial': True },
                { '_id': "Waiting for Point",                   'action': self._wait_for_point                      },
                { '_id': "Waiting for Point (Client Ready)",                                                        },
                { '_id': "Init Pre-Tests",                      'action': self._start_tests                         },
                { '_id': "Init Pre-Tests (Client Ready)",       'action': self._start_tests                         },
                { '_id': "Pre-Tests",                                                                               },
                { '_id': "Pre-Tests (Client Ready)",                                                                },
                { '_id': "Pre-Tests Aborted",                   'action': self._abort_tests                         },
                { '_id': "Waiting for Client",                  'action': self._wait_for_client                     },
                { '_id': "Experiment Running",                  'action': self._start_experiment                    },
                { '_id': "Experiment Aborted",                  'action': self._abort_experiment                    },
                # { '_id': "Experiment Failed",                 'action': self.                                     },
                # { '_id': "Sampling Completed",                'action': self._sample_collected                    },
                # { '_id': "Sampling Timedout",                 'action': self._timeout                             },
                # { '_id': "Sampling Failed",                   'action': self.                                     },
                { '_id': "Error",                               'action': self._error,              'final': True   },
                { '_id': "End",                                 'action': self._end,                'final': True   }
            ],
            'transitions': [
                {
                    'trigger': Sampler.Events.START,
                    'from': "Start",
                    'to': "Waiting for Point"
                },
                {
                    'trigger': Sampler.Events.CLIENT_READY,
                    'from': "Waiting for Point",
                    'to': "Waiting for Point (Client Ready)"
                },

                {
                    'trigger': Sampler.Events.POINT_READY,
                    'from': "Waiting for Point",
                    'to': "Init Pre-Tests"
                },
                {
                    'trigger': Sampler.Events.POINT_READY,
                    'from': "Waiting for Point (Client Ready)",
                    'to': "Init Pre-Tests (Client Ready)"
                },

                {
                    'trigger': Transition.__EPSILON_TRANSITION__,
                    'from': "Init Pre-Tests",
                    'to': "Pre-Tests"
                },
                {
                    'trigger': Transition.__EPSILON_TRANSITION__,
                    'from': "Init Pre-Tests (Client Ready)",
                    'to': "Pre-Tests (Client Ready)"
                },

                #FIXME QUICK FIX (since the Epsilon Transitions are not executed immediately but are queued in the event loop)
                #FIXME seems to not work anyway
                {
                    'trigger': Sampler.Events.CLIENT_READY,
                    'from': "Init Pre-Tests",
                    'to': "Pre-Tests (Client Ready)"
                },

                # failure branch
                {
                    'trigger': Sampler.Events.PRE_TEST_FAIL,
                    'from': "Pre-Tests",
                    'to': "Waiting for Point"
                },
                {
                    'trigger': Sampler.Events.EXPERIMENT_TIMEOUT,
                    'from': "Pre-Tests",
                    'to': "Pre-Tests Aborted"
                },

                # failure branch
                {
                    'trigger': Sampler.Events.EXPERIMENT_TIMEOUT,
                    'from': "Waiting for Client",
                    'to': "Waiting for Point"
                },

                {
                    'trigger': Sampler.Events.CLIENT_READY,
                    'from': "Pre-Tests",
                    'to': "Pre-Tests (Client Ready)"
                },

                {
                    'trigger': Sampler.Events.PRE_TEST_FAIL,
                    'from': "Pre-Tests (Client Ready)",
                    'to': "Waiting for Point"
                },
                {
                    'trigger': Sampler.Events.EXPERIMENT_TIMEOUT,
                    'from': "Pre-Tests (Client Ready)",
                    'to': "Pre-Tests Aborted"
                },
                {
                    'trigger': Sampler.Events.PRE_TEST_SUCCESS,
                    'from': "Pre-Tests (Client Ready)",
                    'to': "Experiment Running"
                },

                {
                    'trigger': Sampler.Events.PRE_TEST_SUCCESS,
                    'from': "Pre-Tests",
                    'to': "Waiting for Client"
                },


                {
                    'trigger': Sampler.Events.CLIENT_READY,
                    'from': "Waiting for Client",
                    'to': "Experiment Running"
                },

                # failure branch,
                {
                    'trigger': Sampler.Events.EXPERIMENT_FAIL,
                    'from': "Experiment Running",
                    'to': "Waiting for Point"
                },
                {
                    'trigger': Sampler.Events.EXPERIMENT_TIMEOUT,
                    'from': "Experiment Running",
                    'to': "Experiment Aborted"
                },
                {
                    'trigger': Transition.__EPSILON_TRANSITION__,
                    'from': "Experiment Aborted",
                    'to': "Waiting for Point"
                },

                {
                    'trigger': Sampler.Events.SAMPLE_COLLECTED,
                    'from': "Experiment Running",
                    'to': "Waiting for Point"
                },

                {
                    'trigger': Sampler.Events.NO_MORE_POINTS,
                    'from': "Waiting for Point",
                    'to': "End"
                },

                # IRREVERSIBLE ERRORS
                {
                    'trigger': Sampler.Events.IRREVERSIBLE_ERROR,
                    'from': "Waiting for Point",
                    'to': "Error"
                },
                {
                    'trigger': Sampler.Events.IRREVERSIBLE_ERROR,
                    'from': "Pre-Tests",
                    'to': "Error"
                },
                {
                    'trigger': Sampler.Events.IRREVERSIBLE_ERROR,
                    'from': "Pre-Tests (Client Ready)",
                    'to': "Error"
                },
                {
                    'trigger': Sampler.Events.IRREVERSIBLE_ERROR,
                    'from': "Experiment Running",
                    'to': "Error"
                }
            ]
        }

    def _start(self):
        logging.info("[SAMPLER]: Started")

    #FIXME *args is a quickfix because SUCCESS event contains also the label
    def _wait_for_point(self, *args):
        try:
            self.client.reload()
        except:
            pass

        logging.info("[SAMPLER]: Waiting for point to label")
        point = self.point_generator.get_point()

        if point is None:
            self._emit(Sampler.Events.END)

        # new experiment
        self.current_experiment = {
            '_id': self._generate_experiment_id(),
            'point':   point,

            'error':   {},
            'results': {
                'tests':    {},
                'sampling': {}
            },

            'configuration': self.configuration
        }

        self._emit(Sampler.Events.POINT_READY, point)

    def _start_tests(self, point):
        self.current_point = point
        logging.info("[SAMPLER]: Point to label is \n{}".format(point))

        logging.info("[SAMPLER]: Starting Tests")
        self._tester = Tester(self.ee, point, self.current_experiment, self.configuration['tests'])
        self._tester.start()

    def _abort_tests(self):
        logging.info("[SAMPLER]: Aborting Tests")
        self._tester.stop()
        # self._tester.join()  #FIXME remove comment if sure about the tests will not block in any case (#TODO refactor as FSM needed)

    def _wait_for_client(self):
        logging.info("[SAMPLER]: Waiting for Client")

    def _start_experiment(self):
        logging.info("[SAMPLER]: Sampling")
        self._experimenter = Experimenter(
            self.ee,
            self.client,
            self._tc,
            self.current_point,
            self.current_experiment,
            self.configuration['experiment']['output'],
            self.configuration['experiment']['timeout']
        )
        self._experimenter.start()

    def _abort_experiment(self):
        logging.info("[SAMPLER]: Aborting Experiment")
        self._experimenter.stop()
        # self._experimenter.join() #FIXME remove comment if sure about the experimenter will not block in any case

    def _error(self):
        logging.error("[SAMPLER]: IRREVERSIBLE ERROR")
        self._fsm.stop() #TODO do it explicitely?

    def _client_ready(self, context):
        logging.info("[SAMPLER]: Client Ready")
        self._emit(Sampler_Events.CLIENT_READY)

    def _end(self, point):
        logging.info("[SAMPLER]: Ended")
        self._fsm.stop() #TODO do it explicitely?

    def _emit_sample_as_worst_label(self, context):
        self._emit_sample(self.configuration['worst_label'], context)

    def _emit_sample(self, label, context):
        labeled_point = deepcopy(self.current_point)
        labeled_point['label'] = label
        logging.info("[SAMPLER]: Point labeled as {}".format("Good" if label in [0,1,2,3] else "Worst"))
        self._emit(Sampler_Events.SAMPLE_COLLECTED, labeled_point, __context__ = context)

    def _set_event_bindings(self):
        self.ee.on(Client.Events.READY, self._client_ready)

        # set events from used classes with the current ones (e.g., Experimenter.Events.FAILURE --> Sampler.Events.EXPERIMENT_FAIL)
        self.ee.on(Tester.Events.FAILURE, lambda context, error: self._emit(Sampler_Events.PRE_TEST_FAIL,    error, __context__ = context))
        self.ee.on(Tester.Events.SUCCESS, lambda context:        self._emit(Sampler_Events.PRE_TEST_SUCCESS,        __context__ = context))
        self.ee.on(Tester.Events.SUCCESS, lambda context:        logging.info("[SAMPLER]: TEST SUCCESS"))

        self.ee.on(Experimenter.Events.FAILURE, lambda context, error:   self._emit(Sampler_Events.EXPERIMENT_FAIL, error, __context__ = context))
        self.ee.on(Experimenter.Events.SUCCESS, lambda context, label:   self._emit_sample(label, context))
        self.ee.on(Experimenter.Events.TIMEOUT, lambda context, args:    self._emit_sample_as_worst_label(context)) #FIXME check args

    def _generate_experiment_id(self):
        _id = self.session_id + "-{:05}".format(self.experiment_count)
        self.experiment_count += 1
        return _id


    #TODO create a basic entity where everything extends it (this should be done automatically to avoid mistakes)
    def _emit(self, event, *args, **kwargs):
        if '__context__' not in kwargs:
            kwargs['__context__'] = EventEmitter.get_default_context()

        kwargs['__context__']['source'] = self

        self.ee.emit(event, *args, **kwargs)
        
    def get_experiment_count(self):
        return self.experiment_count
        
    def get_experiment_bound(self):
        return 600
        
    def is_bound_soft(self):
        return True

    def __init__(self, ee, point_generator, client, configuration, tc): # TODO , test_timeout, experiment_timeout
        self.ee = ee
        # self.test_timeout = test_timeout
        # self.experiment_timout = experiment_timeout
        self.point_generator = point_generator
        self.client          = client
        self.configuration   = configuration

        self.experiment_count = 0
        self.session_id = "e{0:.0f}".format(time())

        self.current_point = None
        self.current_experiment = None

        self._tc = tc
        self._experimenter = None

        self._set_event_bindings()

        self._fsm = Automata(ee, json_graph_builder(self._get_behaviour()))
        self._fsm.start()

    def start(self):
        self._emit(Sampler.Events.START)
