from __future__ import print_function

from app.client import Client
from app.convertion import convert_input_to_network_configuration

from net.netutils import PcapSniffingEnvironment, Tc

from automata import Automata, Graph, Transition, json_graph_builder

from threading_utils.timer import EventTimedContext


from enum import Enum
import logging

class Experimenter_Events(Enum):
    START   = 1
    SUCCESS = 2
    FAILURE = 3
    TIMEOUT = 4

class Experimenter(object):
    Events = Experimenter_Events

    def _get_behaviour(self):
        return {
            'states': [
                { '_id': "Init", 'initial': True, 'action': self._init },
                { '_id': "Executing", 'action': self._execute },
                { '_id': "Success", 'action': self._success, 'final': True },
                { '_id': "Timeout", 'action': self._timeout, 'final': True },
                { '_id': "Failure", 'action': self._failure, 'final': True },
            ],
            'transitions': [
                {
                    'trigger': Experimenter.Events.START,
                    'from': "Init",
                    'to': "Executing"
                },
                {
                    'trigger': Experimenter.Events.SUCCESS,
                    'from': "Executing",
                    'to': "Success"
                },
                {
                    'trigger': Experimenter.Events.FAILURE,
                    'from': "Executing",
                    'to': "Failure"
                },
                {
                    'trigger': Experimenter.Events.TIMEOUT,
                    'from': "Executing",
                    'to': "Timeout"
                }
            ]
        }

    def _init(self):
        pass

    def _execute(self):
        logging.info("[EXPERIMENTER]: experiment started")

        # start timer
        #self._timer = EventTimedContext(self.ee, self.timeout, Experimenter_Events.TIMEOUT)
        #self._timer.enter()

        # setup constraint with Tc
        self._tc.set_network_condition(self.network_configuration)

        # start pcap capture
        output = self.output_configuration['folder'] + '/' + self.output_configuration['pcap'].format(self.experiment['_id'])
        logging.info("[EXPERIMENTER]: capturing traffic (saved in {})".format(output))
        self._pcap_sniffer = PcapSniffingEnvironment(output)
        self._pcap_sniffer.start()

        # start playback
        self.client.start_playback()

    def _failure(self, error):
        logging.info("[EXPERIMENTER]: experiment failed with client error\n{}".format(error))
        self._tc.reset_network_condition()
        #self._timer.exit()

    #TODO check args (defined in EventTimedContext - legacy code)
    def _timeout(self, args):
        logging.info("[EXPERIMENTER]: experiment timed out")
        self._tc.reset_network_condition()
        self._pcap_sniffer.stop()

    def _success(self, label):
        logging.info("[EXPERIMENTER]: experiment succeeded")
        self._tc.reset_network_condition()
        #self._timer.exit()

    def _check_pcap(self):
        #TODO check ip
        #get_conversations_index(self.output_configuration['pcap'])
        pass

    #TODO update event names in the Client class
    def _bind_events(self):
        def playback_fail(context, result, error):
            self._pcap_sniffer.stop()

            # update experiment
            self.experiment['results']['sampling'] = result
            self.experiment['error']  = error
            
            self.ee.emit(Experimenter.Events.FAILURE, error, __context__ = context)

        def playback_completed(context, result):
            try:
                self._pcap_sniffer.stop()

                # check result (server ip)
                self._check_pcap()

                # update experiment
                self.experiment['results']['sampling'] = result

                logging.info("[EXPERIMENTER]: playback completed with result\n{}".format(result))

                #label = 0 if len(result['buffering_events']) == 1 else 1
                label = result # the previous line was specific to former YouTube experiment
                self.ee.emit(Experimenter.Events.SUCCESS, label, __context__ = context)
            except:
                logging.exception("[__"+self.__class__.__name__+"__]: an error occured on playback_completed")
                pass

        self.ee.on(Client.Events.PLAYBACK_FAILED,    playback_fail)
        self.ee.on(Client.Events.PLAYBACK_COMPLETED, playback_completed)


    def __init__(self, ee, client, tc, point, experiment, output_configuration, timeout):
        self.ee = ee
        self.client = client
        self._tc = tc
        self.point = point
        self.experiment = experiment
        self.output_configuration = output_configuration
        self.timeout = timeout

        self.network_configuration = convert_input_to_network_configuration(point)
        self.label = None

        self._bind_events()

        self._fsm = Automata(ee, json_graph_builder(self._get_behaviour()))
        self._fsm.start()

    def start(self):
        self.ee.emit(Experimenter_Events.START)
