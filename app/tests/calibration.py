# app.tests.calibration.py
from net.netutils import DummyNetEnvironment
from net.netest   import RttTester

from app.tests.Validation import RttConfigurationValidator, ValueOutOfThreshold, NoTransmissionWithServerException, ServerIpChanged, LossRateConfigurationValidator

from app.events import EVENTS

from threading_utils.Timer import TimedContext

def _print_estimation(network, point, test_result):
    rtt         = test_result['rtt']['avg'] * 1000 # convert from seconds to msec
    delta_delay = rtt / 2
    lossrate    = float(test_result['retransmissions']['download'] / test_result['packet_count']['download'])

    print "Estimated:"
    print "delay    {:10.3f}ms ({})".format(delta_delay,    network['download']['delay'])
    print "rtt      {:10.3f}ms ({})".format(rtt,            point['rtt'])
    print "lossrate {:10.3f}%  ({})".format(lossrate * 100, network['download']['lossrate'])

def _update_configuration(network, point, test_result):
    input_changed = False

    rtt         = test_result['rtt']['avg'] * 1000 # convert from seconds to msec
    delta_delay = rtt / 2
    lossrate    = float(test_result['retransmissions']['download'] / test_result['packet_count']['download'])

    print "Estimated:"
    print "delay    {:10.3f}ms ({})".format(delta_delay,    network['download']['delay'])
    print "rtt      {:10.3f}ms ({})".format(rtt,            point['rtt'])
    print "lossrate {:10.3f}%  ({})".format(lossrate * 100, network['download']['lossrate'])

    if delta_delay < network['download']['delay']: # assumption: RTT symmetrically distributed (check one or the other doesn't make any difference)
        # possible to apply this configuration
        # remove external network contribution from DummyNet configuration)
        # assumption: RTT symmetrically distributed
        network['download']['delay'] -= delta_delay
        network['upload']['delay']   -= delta_delay
    else:
        # impossible to reach target configuration
        # modify the current one
        ## DummyNet does not enforce any delay
        network['download']['delay'] = network['upload']['delay'] = 0
        ## Point is updated with the current minimum value (external network contribution)
        point['rtt'] = rtt

        input_changed = True

    if lossrate < network['download']['lossrate']:
        # possible to apply this configuration
        # remove external network contribution from DummyNet configuration)
        network['download']['lossrate'] -= lossrate
    else:
        # impossible to reach target configuration
        # modify the current one
        ## DummyNet does not enforce any lossrate
        network['download']['lossrate'] = 0
        ## Point is updated with the current minimum value (external network contribution)
        point['download_lossrate'] = point['upload_lossrate'] = lossrate

        input_changed = True

    return input_changed


def _is_out_of_threshold(value1, value2, thresholds, threshold_keys, name = ''):
    try:
        # find threshold value
        threshold = thresholds
        for key in threshold_keys:
            threshold = thresholds[key]

        return abs(value1 - value2) > threshold
    except KeyError:
        # no threshold defined
        print("[WARNING]: threshold {} not found, assuming {} it's not constrained".format(threshold_keys, name))
        # assume it's unconstrained
        return False


def calibration(test_configuration, experiment, out, ee):

    # setup timed context
    flags = { 'abort': False }
    current_operation = None
    def abort_calibration():
        print "[CALIBRATION]: time expired - aborting calibration"
        flags['abort'] = True
        try:
            current_operation.stop()
        except:
            pass

    try:
        with TimedContext(test_configuration['timeout'], abort_calibration, 'calibration-timer-{round:03}-{repetition:03}'.format(round = out['round_count'], repetition = out['repetition_count'])):
            print "[CALIBRATION]: estimating external RTT and Loss Rate"
            # estimate RTT and Loss Rate
            rtt_test     = current_operation = RttTester()
            rtt_test_res = rtt_test.perform_rtt_test(
                test_configuration['output']['folder'],
                test_configuration['output']['root'] + '-estimation',
                experiment['server_url'],
                number_conn_requests = test_configuration['requests']
            )
            _print_estimation(experiment['network'], experiment['point'], rtt_test_res)

            # save estimation
            experiment['noise_estimation'] = rtt_test_res

            # check if time has not expired
            if flags['abort']:
                # time expired
                # impossible to perform test, abort experiment
                if __debug__: print "[CALIBRATION]: Failed - time expired"
                return False
            # check if server is always the same
            if rtt_test_res['packet_count'] == 0:
                # no packet from the expected source, server changed
                if __debug__: print "[CALIBRATION]: Failed - no packets received from the expected source"
                experiment['tests']['reset_all'] = True
                return False

            print "[CALIBRATION]: WARNING - update configuration disabled"

            print "[CALIBRATION]: checking configuration"
            # check RTT and Loss Rate using DummyNet

            current_operation = RttConfigurationValidator()
            try:
                if not current_operation.validate(experiment, test_configuration):
                    print "[CALIBRATION]: RTT test failed"
                    return False
            except ValueOutOfThreshold as e:
                print "[CALIBRATION]: RTT test failed - value out of threshold\n" + str(e)
                #TODO set flag to repeat experiment
                return False
            except (NoTransmissionWithServerException, ServerIpChanged) as e:
                print "[CALIBRATION]: RTT test failed - server changed\n" + str(e)
                #TODO set flag to repeat experiment
                return False
            if flags['abort']:
                # time expired
                # impossible to perform test, abort experiment
                if __debug__: print "[CALIBRATION]: Failed - time expired"
                return False

            current_operation = LossRateConfigurationValidator()
            try:
                if not current_operation.validate(experiment, test_configuration):
                    print "[CALIBRATION]: Loss rate test failed"
                    return False
            except ValueOutOfThreshold as e:
                print "[CALIBRATION]: Loss rate test failed - value out of threshold\n" + str(e)
                #TODO set flag to repeat experiment
                return False
            except (NoTransmissionWithServerException, ServerIpChanged) as e:
                print "[CALIBRATION]: Loss rate test failed - server changed\n" + str(e)
                #TODO set flag to repeat experiment
                return False
            if flags['abort']:
                # time expired
                # impossible to perform test, abort experiment
                if __debug__: print "[CALIBRATION]: Failed - time expired"
                return False

            # calibration successful
            return True
    finally:
        ee.off(EVENTS.experiment_timeout, abort_calibration)

    # calibration timed out
    if __debug__: print "[CALIBRATION]: Failed - time expired"
    return False


def limited_calibration(test_configuration, experiment, out, ee):
    if out['repetition_count'] >= test_configuration['repetition_limit']:
        return False

    return calibration(test_configuration, experiment, out, ee)
