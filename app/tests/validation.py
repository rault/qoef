from net.netutils import TcEnvironment, Tc
from net.netest import RttTester

from copy import deepcopy

class IllegalTransmissionWithServerException(RuntimeError):
    """Exception that notify a not valid transmission with the expected Experiment Server"""
    def __init__(self, message, expected):
        super(IllegalTransmissionWithServerException, self).__init__(message)
        self.expected = expected

class NoTransmissionWithServerException(IllegalTransmissionWithServerException):
    """Exception that notify that no transmission with the expected Experiment Server has been done"""
    def __init__(self, message, expected):
        super(IllegalTransmissionWithServerException, self).__init__(message, expected)

class ServerIpChanged(IllegalTransmissionWithServerException):
    """Exception that notify if the Experiment Server has changed"""
    def __init__(self, message, expected, current):
        super(ServerIpChanged, self).__init__(message, expected)
        self.current = current

class ServerUrlChanged(IllegalTransmissionWithServerException):
    """Exception that notify if the Experiment Server has changed"""
    def __init__(self, message, expected, current):
        super(ServerUrlChanged, self).__init__(message, expected)
        self.current = current

class ValueOutOfThreshold(RuntimeError):
    """Exception that notify when a value is detected as out of threshold during a test"""
    def __init__(self, message, current, expected, threshold, key = None):
        super(ValueOutOfThreshold, self).__init__(message)
        self.current   = current
        self.expected  = expected
        self.threshold = threshold
        self.key       = key

class ValidationAborted(RuntimeError):
    """Notify that a validation has been interrupted explicitely (aborted)"""
    def __init__(self, message):
        super(ValidationAborted, self).__init__(message)


def _is_out_of_threshold(expected, current, threshold):
    return abs(expected - current) > threshold



def validate_experiment_source(experiment, result):
    assert 'experiment' in result
    assert 'server' in result['experiment']
    assert 'ip' in result['experiment']['server']
    assert result['experiment']['server']['ip'] is not None

    current_ip  = result['experiment']['server']['ip']

    # check IP
    if 'server_ip' not in experiment or experiment['server_ip'] is None:
        ## set current IP if not defined (first resolution)
        expected_ip = experiment['server_ip'] = current_ip
    else:
        ## check if the current_ip is the same of the expected_ip
        expected_ip = experiment['server_ip']
        validate_ip_source(expected_ip, current_ip)

    # check data transmitted
    validate_conversation(expected_ip, result)

def validate_ip_source(expected_ip, current_ip):
    ## check if the IP of the test is the expected one
    if current_ip != expected_ip:
        # server IP changed
        if __debug__: print "[VALIDATION]: Failed - unexpected ip (expected {}, current {})".format(expected_ip, current_ip)
        raise ServerIpChanged("Validation Failed - unexpected ip (expected {}, current {})".format(expected_ip,  current_ip))

def validate_conversation(expected_ip, result):
    ## check if a transmission has been done with the expected IP
    if result['packet_count']['all'] == 0:
        # no packet from the expected source, server
        if __debug__: print "[VALIDATION]: Failed - no packets received from the expected source ({})".format(expected_ip)
        raise NoTransmissionWithServerException("Validation Failed - no packets received from the expected source ({})".format(expected_ip), expected_ip)


#######################################################################################################################################
class RttConfigurationValidator(object):
    """RttConfigurationValidator to detect if the rtt is correctly enforced given a noise estimation"""
    def __init__(self):
        super(RttConfigurationValidator, self).__init__()
        self.experiment         = None
        self.test_configuration = None
        self.raw_result         = None
        self.aborted            = False
        self.running            = False

    """Interrupt test"""
    def stop(self):
        if not self.running:
            return

        self.aborted = True
        try:
            self._tester.stop()
        except:
            pass

    def _is_rtt_noise_under_threshold(self):
        try:
            expected  = 0.0
            current   = self.experiment['noise_estimation']['rtt']['avg'] * 1000 # seconds to msec
            threshold = self.test_configuration['thresholds']['rtt']

            return _is_out_of_threshold(expected, current, threshold)
        except KeyError:
            print "Key Error"
            return False

    def _perform_rtt_test(self):
        # setup configuration with delay only (other input metrics would affect this value)
        netconf = deepcopy(Tc.NOT_CONSTRAINED)
        netconf['download']['delay'] = self.experiment['network']['download']['delay']
        netconf['upload']['delay']   = self.experiment['network']['upload']['delay']

        # enforce configuration and test
        with TcEnvironment(netconf):
            self._tester = RttTester()

            self.raw_result = self._tester.perform_rtt_test(
                self.test_configuration['output']['folder'],
                self.test_configuration['output']['root'] + '-calibration-rtt',
                self.experiment['server_url']
            )


    def _check_rtt_test_result(self):
        # CHECK SOURCE
        # check if server is always the same
        validate_experiment_source(self.experiment, self.raw_result)

        # CHECK DETECTED VALUE
        ## check if the detected RTT is in threshold
        ### extract result
        current_rtt  = self.raw_result['rtt']['avg'] * 1000 # seconds to msec
        ### get relevant data from dictionaries
        expected_rtt = self.experiment['point']['rtt']
        threshold    = self.test_configuration['thresholds']['rtt']

        if __debug__: print "[CALIBRATION]: rtt is {}ms (expected {}ms)".format(current_rtt, expected_rtt)

        ## compare with threshold
        if _is_out_of_threshold(expected_rtt, current_rtt, threshold):
            # calibration failed
            # repeat calibration
            if __debug__: print "[CALIBRATION]: Failed - rtt out of threshold ({} < {} < {} == false)".format(expected_rtt - threshold, current_rtt, expected_rtt + threshold)
            raise ValueOutOfThreshold(
                "Calibration Failed - RTT out of threshold ({} < {} < {} == false)".format(expected_rtt - threshold, current_rtt, expected_rtt + threshold),
                current_rtt,
                expected_rtt,
                threshold
            )

    """
    @raises NoTransmissionWithServerException if no packets were transmitted with the expected server (see experiment configuration)
    @raises ServerIpChanged if the current server is not the expected one
    @raises ValueOutOfThreshold if the detected value is outside of the defined threshold (see test_configuration)
    """
    def validate(self, experiment, test_configuration):
        print "[CALIBRATION]: validating rtt"

        # test basic attributes in dictionaries
        # EXPERIMENT
        assert experiment is not None
        assert 'noise_estimation' in experiment
        assert 'server_url' in experiment
        assert 'network' in experiment
        assert 'point' in experiment

        # TEST CONFIGURATION
        assert test_configuration is not None
        assert 'thresholds' in test_configuration
        assert 'output' in test_configuration

        # save test context
        self.experiment         = experiment
        self.test_configuration = test_configuration
        self.raw_result         = None
        self.aborted            = False
        self.running            = True


        # perform validation test
        try:
            # check if noise estimation is below threshold
            if self._is_rtt_noise_under_threshold():
                # it's not necessary to perform this test
                # success
                if __debug__: print "[CALIBRATION]: rtt estimation under threshold, test is not necessary (DummyNet assumed as working correctly)"
                return True

            # perform RTT test
            self._perform_rtt_test()
            # check results
            #FIXME test disabled
            if __debug__: print "[CALIBRATION]: Warning - check rtt disabled"
            # self._check_rtt_test_result() # in case of failures exceptions are thrown (NoTransmissionWithServerException, ServerIpChanged, ValueOutOfThreshold)
        finally:
            # set disable 'running' flag
            self.running = False

        # success
        return True
#######################################################################################################################################

#######################################################################################################################################
class LossRateConfigurationValidator(object):
    """LossRateConfigurationValidator to detect if the loss rate is correctly enforced given a noise estimation"""
    def __init__(self):
        super(LossRateConfigurationValidator, self).__init__()
        self.experiment         = None
        self.test_configuration = None
        self.raw_result         = None
        self.aborted            = False
        self.running            = False

    """Interrupt test"""
    def stop(self):
        self.aborted = True
        try:
            self._tester.stop()
        except:
            pass

    def _is_lossrate_noise_under_threshold(self):
        try:
            expected  = 0.0
            current   = self.experiment['noise_estimation']['lossrate']
            threshold = self.test_configuration['thresholds']['download_lossrate']

            return _is_out_of_threshold(expected, current, threshold)
        except KeyError:
            return False

    def _perform_lossrate_test(self):
        # setup configuration with lossrate only (other input metrics could affect this value)
        netconf = deepcopy(Tc.NOT_CONSTRAINED)
        netconf['download']['lossrate'] = self.experiment['network']['download']['lossrate']
        netconf['upload']['lossrate']   = self.experiment['network']['upload']['lossrate']

        # enforce configuration and test
        with TcEnvironment(netconf):
            self._tester = RttTester()
            if self.aborted:
                raise ValidationAborted("Validation Failed - Aborted")

            self.raw_results = self._tester.perform_rtt_test(
                self.test_configuration['output']['folder'],
                self.test_configuration['output']['root'] + '-calibration-lossrate',
                self.experiment['server_url']
            )

    def _check_lossrate_test_result(self):
        # CHECK SOURCE
        # check if server is always the same
        validate_experiment_source(self.experiment, self.raw_result)

        # CHECK DETECTED VALUE
        ## check if the detected LossRate is in threshold
        ### extract result
        current_lossrate = float(self.raw_result['retransmissions']['download'] / self.raw_result['packet_count']['download']) # get ratio (from 0.0 to 1.0)
        ### get relevant data from dictionaries
        expected_lossrate = self.experiment['point']['download_lossrate']
        threshold = self.test_configuration['thresholds']['download_lossrate']

        if _is_out_of_threshold(expected_lossrate, current_lossrate, threshold):
            # calibration failed
            # repeat calibration
            if __debug__: print "[CALIBRATION]: Failed - loss rate out of threshold ({} < {} < {} == false)".format(expected_lossrate - threshold, current_lossrate, expected_lossrate + threshold)
            raise ValueOutOfThreshold(
                "Calibration Failed - RTT out of threshold ({} < {} < {} == false)".format(expected_lossrate - threshold, current_lossrate, expected_lossrate + threshold),
                current_lossrate,
                expected_lossrate,
                threshold
            )

    """
    @raises NoTransmissionWithServerException if no packets were transmitted with the expected server (see experiment configuration)
    @raises ServerIpChanged if the current server is not the expected one
    @raises ValueOutOfThreshold if the detected value is outside of the defined threshold (see test_configuration)
    """
    def validate(self, experiment, test_configuration):
        print "[CALIBRATION]: validating lossrate"

        # test basic attributes in dictionaries
        # EXPERIMENT
        assert experiment is not None
        assert 'noise_estimation' in experiment
        assert 'server_url' in experiment
        assert 'network' in experiment
        assert 'point' in experiment

        # TEST CONFIGURATION
        assert test_configuration is not None
        assert 'thresholds' in test_configuration
        assert 'output' in test_configuration

        # save test context
        self.experiment         = experiment
        self.test_configuration = test_configuration
        self.raw_result         = None
        self.aborted            = False
        self.running            = True

        # perform validation test
        try:
            # check if noise estimation is below threshold
            if self._is_lossrate_noise_under_threshold():
                # it's not necessary to perform this test
                # success
                if __debug__: print "[CALIBRATION]: lossrate estimation under threshold, test is not necessary (DummyNet assumed as working correctly)"
                return True

            # perform RTT test
            self._perform_lossrate_test()
            # check results
            #FIXME test disabled
            if __debug__: print "[CALIBRATION]: Warning - check lossrate disabled"
            # self._check_lossrate_test_result() # in case of failures exceptions are thrown
        finally:
            # set disable 'running' flag
            self.running = False

        return True
#######################################################################################################################################
