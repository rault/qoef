# app.tests.throughput.py

from net.netest import VideoDlTester

from app.tests.validation import ValueOutOfThreshold

# file system utils
import glob
import os

def to_kbyte_throughput_list(throughput_list):
    res = []

    for e in throughput_list:
        res.append(float(e['bytes']) / 1000)

    return res

def calculate_throughput(throughput_list):
    if len(throughput_list) < 5:
        values = throughput_list[-1:]
    else:
        # take the last 3 values
        values = throughput_list[-3:]

    # take bytes field only
    values = to_kbyte_throughput_list(values)

    # return mean
    return float(sum(values)) / len(values) if len(values) > 0 else float('nan')

class BasicThroughputTest(object):
    def __init__(self, configuration, experiment):
        self.configuration = configuration
        self.experiment = experiment

        self.result = None

        self._ydl_test = None
        self._stop = False

    def stop(self):
        try:
            self._stop = True
            self._ydl_test.stop()
        except AttributeError:
            pass

    def _perform_test(self):
        self._ydl_test = VideoDlTester()

        self.result = self._ydl_test.perform_dl_test(
            self.configuration['output']['folder'],
            self.configuration['output']['root'],
            self.experiment['results']['target_url'],
            self.configuration['target_quality'],
            timeout = self.configuration['timeout']
        )

        # cleanup
        for f in glob.glob("{}/{}*.dat".format(self.configuration['output']['folder'], self.configuration['output']['root'])):
            os.remove(f)

    def _validate_test(self):
        # interrupted exception
        if ydl_test_res is None:
            raise RuntimeError("VideoDlTester Error: no result received")

        # check if server is always the same
        actual = self.result['experiment']['server']['url']
        expected = self.experiment['results']['server_url']
        if not actual != expected:
            raise ServerUrlChanged("Server URL changed - expected {}, was {}".format(expected, actual), expected, actual)

        actual = self.result['experiment']['server']['ip']
        expected = self.experiment['results']['server_ip']
        if not actual != expected:
            raise ServerIpChanged("Server IP changed - expected {}, was {}".format(expected, actual), expected, actual)


    def start(self):
        if self._stop:
            return

        self._perform_test()
        self._validate_test()

class ThroughputTest(BasicThroughputTest):
    def _perform_test(self):
        super(ThroughputTest, self)._perform_test()

        # save result
        experiment['results']['tests']['throughput_out_dummynet']  = ydl_test_res


    def _validate_test(self):
        super(ThroughputTest, self)._perform_test()

        # calculate throughput value
        actual = calculate_throughput(ydl_test_res['throughput']['download'])

        # check if throughput measured is not lower than the configuration one
        expected = experiment['point']['download_throughput']
        if actual < expected:
            raise ValueOutOfThreshold("download throughput is too low (Internet is the bottleneck): {} < {}".format(actual, expected), actual, expected, 0)

class TcpThroughputRecorder(BasicThroughputTest):
    def _perform_test(self):
        super(TcpThroughputRecorder, self)._perform_test()

        # save result
        experiment['results']['tests']['throughput_in_dummynet']  = ydl_test_res
