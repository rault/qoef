# app.tests.url.py

from net.netest import solve_url
from urlparse import urlparse

def url_solver(test_configuration, experiment):
    url = test_configuration['target_url'].format(**experiment['configuration']['playback'])

    experiment['results']['target_url']      = url
    experiment['results']['server_url_full'] = solve_url(url)
    experiment['results']['server_url']      = urlparse(experiment['results']['server_url_full']).netloc
    experiment['results']['server_ip']       = None
