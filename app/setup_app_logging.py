import logging, os

BASE_DIRECTORY="log/"
CONSOLE_LOGLEVEL=logging.ERROR

def setup_logging():
    # setup log directory
    if not os.path.exists(BASE_DIRECTORY):
        os.makedirs(BASE_DIRECTORY)

    # setup logger
    logging.basicConfig(filename=BASE_DIRECTORY+'log.txt', level=logging.NOTSET)

    # show log in console too
    console = logging.StreamHandler()
    console.setLevel(CONSOLE_LOGLEVEL)
    logging.getLogger().addHandler(console)
    
def setup_logger(logger_name, log_file, level=logging.INFO):
    l = logging.getLogger(logger_name)
    formatter = logging.Formatter('%(asctime)s : %(message)s')
    fileHandler = logging.FileHandler(BASE_DIRECTORY+log_file+'.log', mode='w')
    fileHandler.setFormatter(formatter)
    l.setLevel(level)
    l.addHandler(fileHandler)
    
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)
    l.addHandler(streamHandler)
    
    return l
