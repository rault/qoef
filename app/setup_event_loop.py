from event_loop import EventLoop

import logging

def start_event_loop(ee):
  logging.info("[__MAIN__]: setup event loop")
  event_loop = EventLoop(ee)
  logging.info("[__MAIN__]: starting event loop")
  event_loop.start()
  logging.info("[__MAIN__]: event loop started")

  return event_loop
