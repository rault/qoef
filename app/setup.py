from app.setup_http                 import start_http_server
from app.setup_ws                   import start_websocket_server
from app.setup_db                   import start_db_daemon
from app.setup_active_learner       import start_vowpal_wabbit_daemon, start_active_learner
from app.setup_target_application   import start_target_application
from app.setup_event_loop           import start_event_loop
from app.setup_app                  import start_sampler, get_configuration

from sampling.generators.fast import get_fast_generator

from event_dispatching import EventEmitter

import logging

def _setup_http_server(configuration):
    return start_http_server(configuration['host'], configuration['port'])

def _setup_websocket_server(ee, configuration):
    return start_websocket_server(configuration['host'], configuration['port'], ee)

def _setup_db_daemon(configuration):
    return start_db_daemon(configuration['port'], configuration['data_location'])

def _setup_vw_daemon(configuration):
    return start_vowpal_wabbit_daemon(configuration['port'], configuration['mellowness'])

def _setup_active_learner(configuration):
    point_generator = get_fast_generator(space_definition)
    return start_active_learner(configuration['host'], configuration['port'], point_generator)

def _setup_target_application(ee, configuration):
    return start_target_application(configuration['cmd'], configuration['host'], configuration['port'], configuration['user'])


# termination routine to close the program (stop all active entities)
def terminate(signum, frame):
    logging.info("[__MAIN__]: closing event loop")
    event_loop.stop()
    event_loop.join()
    logging.info("[__MAIN__]: event loop closed")

    logging.info("[__MAIN__]: closing http server")
    http_server.stop()
    http_server.join()
    logging.info("[__MAIN__]: http server closed")

    logging.info("[__MAIN__]: closing websocket server")
    ws_server.stop()
    ws_server.join()
    logging.info("[__MAIN__]: websocket server closed")

    logging.info("[__MAIN__]: closing db daemon")
    db_server.stop()
    db_server.join()
    logging.info("[__MAIN__]: db daemon closed")

    logging.info("[__MAIN__]: closing vowpal wabbit daemon")
    vw_server.stop()
    vw_server.join()
    logging.info("[__MAIN__]: vowpal wabbit daemon closed")

    # logging.info("[__MAIN__]: closing target application")
    # client.stop()
    # client.join()
    # logging.info("[__MAIN__]: target application closed")

# init core environment
# # setup logging
setup_logging()
# # setup event based framework
ee = EventEmitter()
event_loop = start_event_loop(ee)

# load configuration
configuration = get_configuration()

# init app services
http_server = _setup_http_server(configuration['http'])
ws_server   = _setup_websocket_server(ee, configuration['ws'])
db_server   = _setup_db_daemon(configuration['db'])

# start application
vw_server       = _setup_vw_daemon(configuration['vw'])
active_learner  = _setup_active_learner(configuration['vw'])
client          = _setup_target_application(ee, configuration['client'])
sampler         = start_sampler(event_loop, active_learner, client, configuration['sampler']) #NB: event loop ensures thread safety (being a serialized event emitter)


logging.info("[__MAIN__]: setup signal handling")
from signal import signal, SIGINT, pause
signal(SIGINT, terminate)
pause()
logging.info("[__MAIN__]: press ^C to interrupt")
