from app.client  import Client
from app.browser import Browser

import logging

def start_client(ee, messages):
    logging.info("[__MAIN__]: client initiating")
    client = Client(ee, messages)
    logging.info("[__MAIN__]: client starting")
    logging.info("[__MAIN__]: client started")

    return client

def start_browser(host, port, user, browser_cmd):
    logging.info("[__MAIN__]: browser initiating")
    browser = Browser(browser_cmd, host, port, user)
    logging.info("[__MAIN__]: browser starting")
    browser.start()
    logging.info("[__MAIN__]: browser started")

    return browser

def start_target_application(ee, host, port, user, browser_cmd, messages):
    browser = start_browser(host, port, user, browser_cmd)
    client  = start_client(ee, messages)

    return client
