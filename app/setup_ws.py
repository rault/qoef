# main.websocket_server.py
# setup the WebSocket server

from websocket.OotbWebSocketServer import OotbWebSocketServer

import logging


# WebSocket Server
# class OotbWebSocketServer(host, port, ee, callback = None, name = 'websocket_server')
# implemented in websocket.OotbWebSocketServer
def start_websocket_server(host, port, ee):
	logging.info("[__MAIN__]: WebSocket server initiating")
	websock_serv = OotbWebSocketServer(host, port, ee)
	logging.info("[__MAIN__]: WebSocket server starting")
	websock_serv.start()
	logging.info("[__MAIN__]: WebSocket server started")
	logging.info("[__MAIN__]: waiting for connections on ({}, {})\n".format(host, port))

	return websock_serv
