import time

from copy import deepcopy

import socket

from sampler import Sampler
from vw.vowpal_wabbit 	             import VowpalWabbitActiveLearner, VowpalWabbitServer

import logging
import math

from copy import deepcopy

def submit_point_to_active_learner(active_learner, labeled_point):
    pt = deepcopy(labeled_point)

    if pt['label'] == 100:
        pt['label'] = 1

    logging.info("[ACTIVE LEARNER]: sending to active learner:\n{}".format(pt))
    active_learner.submit_point(pt)


def start_vowpal_wabbit_daemon(port, mellowness):
    # Vowpal Wabbit daemon setup
    # start server process
    logging.info("[__MAIN__]: Vowpal Wabbit daemon initiating")
    vw_server = VowpalWabbitServer(port, mellowness)
    logging.info("[__MAIN__]: Vowpal Wabbit daemon starting")
    vw_server.start()

    # wait for VW server to be ready
    time.sleep(5.0)
    logging.info("[__MAIN__]: Vowpal Wabbit daemon started")

    return vw_server

def start_active_learner(ee, host, port, point_generator):
    ############################################################################
    # TEST GENERATOR CODE
    class DummyLearner(object):
        def __init__(self, generator):
            self.iterator = iter(generator)

        def get_point(self):
            try:
                return next(self.iterator)
            except StopIteration:
                return None

        def submit_point(self, point, details = None):
            pass

    logging.info("[__MAIN__]: using Dummy Learner (active learning disable, using generator only)")
    active_learner = DummyLearner(point_generator)
    ############################################################################


    # logging.info("[__MAIN__]: active learner initiating")
    # # open communication with the remote host
    # sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # sock.connect((host, port))
    #
    # # init active learner
    # active_learner = VowpalWabbitActiveLearner(sock, point_generator, with_details = False)
    # logging.info("[__MAIN__]: active learner starting")

    # wrap method to return a point with rounded values (meaningful / usable values)
    # e.g., remove decimals from rtt since it's in milliseconds (and DummyNet cannot handle this rate)
    def round_values(fn, keys, ndigits = 0):
    	def wrapper():
            point = fn()

            if point is None:
                return point

            for key in keys:
            	if key in point:
            		point[key] = round(point[key], ndigits)
            		if ndigits is 0:
            			point[key] = int(point[key])
            return point
    	return wrapper

    active_learner.get_point = round_values(round_values(active_learner.get_point, ['download_throughput', 'rtt']), ['download_lossrate', 'upload_lossrate'], 2)

    # # routine to give feedback to the active learner (labeled samples)
    # ee.on(Sampler.Events.SAMPLE_COLLECTED, lambda context, point: submit_point_to_active_learner(active_learner, point))
    #
    # logging.info("[__MAIN__]: active learner started")

    return active_learner


def _calculate_euclidean_distance(point1, point2):
    _sum = 0
    for key in point1:
        if key == 'label':
            continue

        _sum += (point1[key] - point2[key])**2

    return math.sqrt(_sum)

def _calculate_euclidean_distances(reference_point, points):
    res = []

    for point in points:
        res.append(_calculate_euclidean_distance(point, reference_point))

    return res

def _find_closest(reference_point, points):
    closest = None
    closest_distance = float("inf")

    for point in points:
        point_distance = _calculate_euclidean_distance(point, reference_point)

        if point_distance < closest_distance:
            closest = point
            closest_distance = point_distance

    return closest

def _entries_to_points(entries):
    res = []

    for entry in entries:
        res.append(entry['point'])

    return res

def _have_different_labels(points):
    current_label = None

    for point in points:
        if point['label'] != current_label and current_label is not None:
            return True
        else:
            current_label = point['label']

    return False

#TODO refactor with functions (get close points, check noise, ...)
def _infer_label(labeled_points_collection, point, thresholds = None):
    higher_query = {
        'point.download_throughput': { '$gt': point['download_throughput'] },
        'point.download_lossrate':   { '$lt': point['download_lossrate']   },
        'point.rtt':                 { '$gt': point['rtt']                 }
    }

    higher_entries = labeled_points_collection.find(higher_query)
    if higher_entries is None:
        return False
    higher_points  = _entries_to_points(higher_entries)
    closest_high = _find_closest(point, higher_points)

    if closest_high is None:
        return False

    if thresholds is not None:
        near_query = {
            'point.download_throughput': { '$gt': closest_high['download_throughput'] - thresholds['download_throughput'], '$lt': closest_high['download_throughput'] + thresholds['download_throughput'] },
            'point.download_lossrate':   { '$gt': closest_high['download_lossrate']   - thresholds['download_lossrate']  , '$lt': closest_high['download_lossrate']   + thresholds['download_lossrate']   },
            'point.rtt':                 { '$gt': closest_high['rtt']                 - thresholds['rtt']                , '$lt': closest_high['rtt']                 + thresholds['rtt']                 }
        }

        close_to_closest_high_entries = labeled_points_collection.find(near_query)
        close_to_closest_high_points  = _entries_to_points(close_to_closest_high_entries)

        if _have_different_labels(close_to_closest_high_points):
            return False # point to sample, we are on a border or there is noise


    lower_query = {
        'point.download_throughput': { '$lt': point['download_throughput'] },
        'point.download_lossrate':   { '$gt': point['download_lossrate']   },
        'point.rtt':                 { '$lt': point['rtt']                 }
    }

    lower_entries = labeled_points_collection.find(lower_query)
    lower_points  = _entries_to_points(lower_entries)
    closest_low = _find_closest(point, lower_points)

    if closest_low is None:
        return False

    #TODO NB: BETTER QoE have LOWER CLASS VALUE, WORSE QoE have HIGHER VALUES
    if closest_low['label'] < closest_high['label']:
        return False # noise (labeling is needed)

    if closest_low['label'] != closest_high['label']:
        return False # cannot apply inference (labelleing is needed)

    if thresholds is not None:
        near_query = {
            'point.download_throughput': { '$gt': closest_high['download_throughput'] - thresholds['download_throughput'], '$lt': closest_high['download_throughput'] + thresholds['download_throughput'] },
            'point.download_lossrate':   { '$gt': closest_high['download_lossrate']   - thresholds['download_lossrate']  , '$lt': closest_high['download_lossrate']   + thresholds['download_lossrate']   },
            'point.rtt':                 { '$gt': closest_high['rtt']                 - thresholds['rtt']                , '$lt': closest_high['rtt']                 + thresholds['rtt']                 }
        }

        close_to_closest_low_entries = labeled_points_collection.find(near_query)
        close_to_closest_low_points  = _entries_to_points(close_to_closest_low_entries)

        if _have_different_labels(close_to_closest_low_points):
            return False # point to sample, we are on a border or there is noise

    point['label'] = closest_low['label'] # which is equal to closest_high['label']

    return True



def _push_basic_knowledge(active_learner):
    logging.info("[__MAIN__]: pushing basic knowledge (corner points)")

    basic_knowledge = [
        { "download_lossrate" : 0.0, "label" : 0,   "download_throughput" : 10000, "rtt" : 0    },
        { "download_lossrate" : 0.5, "label" : 100, "download_throughput" : 0,     "rtt" : 5000 },
        { "download_lossrate" : 0.5, "label" : 100, "download_throughput" : 5000,  "rtt" : 5000 }
    ]

    for labeled_point in basic_knowledge:
        submit_point_to_active_learner(active_learner, labeled_point)

def _push_not_worst_class_data(active_learner):
    labeled_entries = labeled_points_collection.find({"point.label": {"$ne": 100}})

    for labeled_entry in labeled_entries:
        labeled_point = labeled_entry['point']

        submit_point_to_active_learner(active_learner, labeled_point)

def _push_known_data(active_learner, labeled_points_collection):
    labeled_entries = labeled_points_collection.find({})

    for labeled_entry in labeled_entries:
        labeled_point = labeled_entry['point']

        submit_point_to_active_learner(active_learner, labeled_point)

def active_learner_with_seeding(active_learner, collections):
    _push_known_data(active_learner, collections['labeled_points'])
    _push_basic_knowledge(active_learner)
    return active_learner

#FIXME quickfix
POINT_INFERRED = "__point_inferred_event__"

#TODO heavy refactor needed (inference should not be included in the active learner)
def active_learner_with_inference(active_learner, collections, ee, thresholds = None):
    global POINT_INFERRED
    def check_inference(fn):
        def wrapper():
            found = False
            while not found:
                point = fn()
                #TODO put in Active Learner implementation
                logging.info("[ACTIVE_LEARNER]: point selected by active learner is\n{}".format(point))

                _infer_label(collections['labeled_points'], point, thresholds)

                if 'label' in point and point['label'] is not None:
                    logging.info("[ACTIVE_LEARNER]: point inferred as {}".format(point['label']))
                    submit_point_to_active_learner(active_learner, point)
                    ee.emit(POINT_INFERRED, point)
                    continue

                found = True

            return point
        return wrapper

    active_learner.get_point = check_inference(active_learner.get_point)

    return active_learner
