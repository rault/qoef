import json
import logging

from websocket.EventBasedWebSocket import EventBasedWebSocket

from enum import Enum

class Client_Events(Enum):
	READY              = 1
	PLAYBACK_COMPLETED = 2
	PLAYBACK_FAILED    = 3
	FAILURE            = 4 #TODO use in case of failures. e.g.: socket failure

class Client(object):
	Events = Client_Events

	def __init__(self, ee, MESSAGES):
		super(Client, self).__init__()

		self.ee = ee
		self.MESSAGES = MESSAGES

		self.client_websocket = None

		# bind to WebSocket events
		## register current socket
		def on__socket_connected(context, evt_args):
			self.client_websocket = evt_args['source']
			
		self.ee.on(EventBasedWebSocket.EVENTS.CONNECTED, on__socket_connected)

		## handle socket failure
		#TODO handle unexpected client disconnection
		def on__socket_closed(context, evt_args):
			self.client_websocket = None
			
		self.ee.on(EventBasedWebSocket.EVENTS.CLOSED, on__socket_closed)

		## handle messages
		### Experiment__ReadyToStart
		# ee.on(EventBasedWebSocket.EVENTS.MESSAGE__RECEIVED, self._define_message_handler(MESSAGES['Experiment__ReadyToStart'], 	Client_Events.READY))
		# ### Experiment__Completed
		# ee.on(EventBasedWebSocket.EVENTS.MESSAGE__RECEIVED, self._define_message_handler(MESSAGES['Experiment__Completed'], 	Client_Events.PLAYBACK_COMPLETED))
		# ### Experiment__Error
		# ee.on(EventBasedWebSocket.EVENTS.MESSAGE__RECEIVED, self._define_message_handler(MESSAGES['Experiment__Error'], 		Client_Events.PLAYBACK_FAILED))

		def _emit_ready(context, args):
			# from json string to dict
			data = json.loads(args['data'])

			if not (data['code'] == MESSAGES['Experiment__ReadyToStart']['code']):
				return

			self.ee.emit(Client_Events.READY, __context__ = context)


		def _emit_completed(context, args):
			# from json string to dict
			data = json.loads(args['data'])

			if not (data['code'] == MESSAGES['Experiment__Completed']['code']):
				return

			self.ee.emit(Client_Events.PLAYBACK_COMPLETED, data['result'], __context__ = context)

		def _emit_failed(context, args):
			# from json string to dict
			data = json.loads(args['data'])

			if not (data['code'] == MESSAGES['Experiment__Error']['code']):
				return

			self.ee.emit(Client_Events.PLAYBACK_FAILED, *(data['result'], data['error']), __context__ = context)

		### Experiment__ReadyToStart
		ee.on(EventBasedWebSocket.EVENTS.MESSAGE__RECEIVED, _emit_ready)
		### Experiment__Completed
		ee.on(EventBasedWebSocket.EVENTS.MESSAGE__RECEIVED, _emit_completed)
		### Experiment__Error
		ee.on(EventBasedWebSocket.EVENTS.MESSAGE__RECEIVED, _emit_failed)

	# def _define_message_handler(self, message_descriptor, raised_event):
	# 	def handler(context, evt_args):
	# 		# from json string to dict
	# 		data = json.loads(evt_args['data'])
	#
	# 		if not (data['code'] is message_descriptor['code']):
	# 			return
	#
	# 		print data
	#
	# 		if 'result' in data:
	# 			if 'error' in data and data['error'] is not False:
	# 				args = (data['result'], data['error'])
	# 			else:
	# 				args = (data['result'])
	# 		else:
	# 			args = ()
	#
	# 		self.ee.emit(raised_event, *args, __context__ = context)
	# 	return handler



	def _send_msg(self, message):
		raw_message = json.dumps(message)
		logging.debug("[__Client__]: sending message",message)
		try:
			self.client_websocket.sendMessage(raw_message)
		except:
			logging.warning("[__Client__]: sending message failed (probably a socket error, check the client_websocket is still set (it is set to None on socket close event))")

	def reload(self):
		self._send_msg(self.MESSAGES['ReloadRequest'])

	def start_playback(self):
		logging.info("[__Client__]: starting call")
		self._send_msg(self.MESSAGES['Playback__Start_Request'])
