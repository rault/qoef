# server.http_server.py
# setup the HTTP web server
import urllib2
import bottle
from   http.OotbHttpServer import OotbHttpServer

import logging


def start_http_server(host, port):
    logging.info("[__MAIN__]: Http server initiating")
    http_server = setup_http_server(host, port)
    logging.info("[__MAIN__]: Http server starting")
    http_server.start()
    logging.info("[__MAIN__]: Http server started")
    logging.info("[__MAIN__]: waiting for connections on ({}, {})\n".format(host, port))

    return http_server


# class OotbHttpServer(host, port, app = bottle.Bottle(), name = 'http_server')
# implemented in http.OotbHttpServer
def setup_http_server(host, port):
    app = bottle.Bottle()

    # configuration of the infrastructure
    @app.route('/config/<filepath:path>')
    def config(filepath):
        return bottle.static_file(filepath, root = './config')

    # server youtube API JavaScript script for browser
    @app.route('/player_api.min.js')
    def player_api():
        #TODO
        # if configuration['serve_remote_player_api']:
        #     # serve from remote source
        #      # new url (before was player_api)
        #     url = "https://www.youtube.com/iframe_api"
        #     response = urllib2.urlopen(url).read()
        #     return response
        # else:
            # serve static file
        return bottle.static_file('youtube_api/player_api.min.js', root = './www')

    return OotbHttpServer(host, port, app)
