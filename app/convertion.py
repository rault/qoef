def convert_input_to_network_configuration(point):
    download_bandwidth  = int(round(point['download_throughput'] / (1 - point['download_lossrate'])))

    balanced_delay  = point['rtt'] / 2
    download_delay  = int(round(balanced_delay))
    upload_delay    = int(round(balanced_delay))

    return {
        'download': {
            'lossrate':  point['download_lossrate'], # from 0 to 1 (100%)
            'bandwidth': download_bandwidth,         # in Kbps
            'delay':     download_delay,             # in ms
            'jitter':    point['jitter']
        },
        'upload': {
            'lossrate':  0,            # from 0 to 1 (100%)  ## unconstrained #TODO should be symmetrical?
            'bandwidth': 10000,        # in Kbps             ## unconstrained #TODO should be symmetrical?
            'delay':     upload_delay, # in ms
            'jitter':    point['jitter']
        }
    }

