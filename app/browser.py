import pwd
import os

import subprocess
from threading import Thread

class Browser(object):
    def __init__(self, cmd, host, port, user, name = "browser"):
        super(Browser, self).__init__()
        self.cmd = cmd
        self.host = host
        self.port = port
        self.user = user

        self._process = None
        self._th = Thread(target = self.run, name = name)

    def start(self):
        self._th.start()

    def run(self):
        location = "http://{host}:{port}".format(host=self.host, port=self.port)
        pw_record = pwd.getpwnam(self.user)
        user_uid = pw_record.pw_uid
        user_gid = pw_record.pw_gid
        env = os.environ.copy()
        cmd = self.cmd.format(target=location)

        #self._process = subprocess.Popen(
        #    cmd.split(' '), preexec_fn=demote(user_uid, user_gid), env=env
        #)
        #self._process.wait()
        #self._process = None

    def stop(self):
        if self._process is not None:
            self._process.kill()
            self._process = None

    def join(self):
        try:
            self._th.join()
        except:
            pass


def start_process_as_user(cmd, user):
    pw_record = pwd.getpwnam(self.user)
    user_uid = pw_record.pw_uid
    user_gid = pw_record.pw_gid
    env = os.environ.copy()

    return subprocess.Popen(cmd, preexec_fn=demote(user_uid, user_gid), env=env)

def demote(user_uid, user_gid):
    def result():
        os.setgid(user_gid)
        os.setuid(user_uid)
    return result
