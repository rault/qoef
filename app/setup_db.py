from db.mongodb import MongoDbDaemon

import pymongo

import logging

import time

def start_db_client(host, port, name, collection_names):
    # setup connection
    try:
        logging.info("[__MAIN__]: DB client connecting")
        conn = pymongo.MongoClient(host, port)
        logging.info("[__MAIN__]: DB client connected")
        logging.info("[DB]: using database \"{}\"\n".format(name))
    except pymongo.errors.ConnectionFailure, e:
       logging.info("[__MAIN__]: DB client cannot connect: %s" % e)
       raise RuntimeException("DB Connection Failure")

    # setup db
    db = conn[name]

    # setup collections
    collections = {
        'labeled_points':   db[collection_names['labeled_points']],
        'samples':          db[collection_names['samples']]
    }

    return (conn, db, collections)

def start_db_daemon(port, data_location):
    logging.info("[__MAIN__]: MongoDB daemon initiating")
    db_daemon = MongoDbDaemon(port, data_location)
    logging.info("[__MAIN__]: MongoDB daemon starting")
    db_daemon.start()

    # wait for the daemon to be ready
    time.sleep(5.0)

    logging.info("[__MAIN__]: MongoDB daemon started")
    logging.info("[__MAIN__]: waiting for connections on port {}\n".format(port))
    logging.info("[__MAIN__]: db location is {}\n".format(data_location))

    return db_daemon
