from sampler import Sampler

import logging

def start_sampler(ee, point_generator, client, configuration, tc):
    logging.info("[__MAIN__]: setup sampler")
    sampler = Sampler(ee, point_generator, client, configuration, tc)
    logging.info("[__MAIN__]: starting sampler")
    sampler.start()
    logging.info("[__MAIN__]: sampler started")

    return sampler
