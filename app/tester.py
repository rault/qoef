from convertion import convert_input_to_network_configuration

from app.tests.url import url_solver
from app.tests.throughput import TcpThroughputRecorder, ThroughputTest

from threading import Thread
from enum import Enum
import json

class Tester_Events(Enum):
    START   = 1
    FAILURE = 2
    SUCCESS = 3

class Tester_Tests(Enum):
    SOLVE_ADDRESS                = 6
    DOWNLOAD_OUTSIDE_DUMMYNET    = 1
    DOWNLOAD_INSIDE_DUMMYNET     = 2
    PROBING_OUTSIDE_DUMMYNET     = 3
    RTT_PROBING_INSIDE_DUMMYNET  = 4
    LOSS_PROBING_INSIDE_DUMMYNET = 5

class InterruptedException(RuntimeError):
    """Notify that a test has been interrupted explicitely (aborted)"""
    def __init__(self, message):
        super(InterruptedException, self).__init__(message)


class Tester_Tests_Encoder(json.JSONEncoder):
     def default(self, obj):
        if isinstance(obj, Enum):
            return {"__tester_tests_enum__": str(obj)}
        return json.JSONEncoder.default(self, obj)

#TODO refactor not extending Thread
class Tester(Thread):
    Events = Tester_Events

    def __init__(self, ee, point, experiment, configuration):
        super(Tester, self).__init__()

        self.ee = ee
        self.point = point
        self.experiment = experiment
        self.configuration = configuration

        self._network_configuration = convert_input_to_network_configuration(point)
        self._stop = False
        self._current_test_type = None
        self._current_test = None

    def stop(self):
        try:
            self._stop = True
            self._current_test.stop()
        except AttributeError:
            pass

    def _solve_address(self):
        self._current_test_type = Tester_Tests.SOLVE_ADDRESS
        self._current_test = None

        url_solver(self.configuration['url_solver'], self.experiment)

    def _ydl_no_dummynet(self):
        self._current_test_type = Tester_Tests.DOWNLOAD_OUTSIDE_DUMMYNET
        self._current_test = ThroughputTest()
        self._current_test.start(self.configuration['throughput_out_dummynet'], self.experiment)

    def _ydl_dummynet(self):
        self._current_test_type = Tester_Tests.DOWNLOAD_INSIDE_DUMMYNET
        self._current_test = ThroughputTest()
        self._current_test.start(self.configuration['throughput_in_dummynet'], self.experiment)

    def _probing_no_dummynet(self):
        self._current_test_type = Tester_Tests.PROBING_OUTSIDE_DUMMYNET
        #TODO


    # probing with only the delay enforced by DummyNet
    def _probing_rtt_dummynet(self):
        self._current_test_type = Tester_Tests.RTT_PROBING_INSIDE_DUMMYNET
        #TODO


    # probing with only the loss rate enforced by DummyNet
    def _probing_loss_dummynet(self):
        self._current_test_type = Tester_Tests.LOSS_PROBING_INSIDE_DUMMYNET
        #TODO

    def _check(self, fn):
        if self._stop:
            raise InterruptedException

    def run(self):
        self.ee.emit(Tester_Events.START)

        try:
            #TODO compile experiment output

            # self._check(self._solve_address)
            # self._check(self._ydl_no_dummynet)
            # self._check(self._ydl_dummynet)
            # self._check(self._probing_no_dummynet)
            # self._check(self._probing_rtt_dummynet)
            # self._check(self._probing_loss_dummynet)

            self.ee.emit(Tester_Events.SUCCESS)
        except (NoTransmissionWithServerException, ServerIpChanged) as e:
            self.ee.emit(Tester_Events.FAILURE, { 'type': "TEST_ERROR", 'subtype': "Server Changed", 'test': self._current_test_type, 'string': str(e) })
        except ValueOutOfThreshold as e:
            self.ee.emit(Tester_Events.FAILURE, { 'type': "TEST_ERROR", 'subtype': "Value out of threshold", 'test': self._current_test_type, 'string': str(e) })
        except InterruptedException as e:
            return
        #TODO remove after debugging tests
        # except e:
        #     self.ee.emit(Tester_Events.FAILURE, { 'type': "TEST_ERROR", 'subtype': None, 'test': self._current_test_type, 'string': str(e) })
