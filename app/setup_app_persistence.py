from sampler import Sampler

from setup_active_learner import POINT_INFERRED

from enum import Enum

from copy import deepcopy

import logging

import traceback

#TODO implement enum with to string simple enough to be used in the configuration file
# class Collections(Enum):
#     SAMPLES = 1
#     LABELED_POINTS = 2

def setup_persistence(ee, collections):

    # setup event handlers
    def on__sample_collected__save_labeled_point(context, labeled_point):
        entry = { }
        entry['point']      = labeled_point
        entry['session_id'] = context['source'].session_id
        entry['_id']        = context['source'].current_experiment['_id']
        entry['timestamp']  = context['time']

        #FIXME quickfix due to runtime error (DB duplicate _id key when saving) #FIXME note to future self: this fixme is no longer needed, but if it breaks you know how to fix it -> re-establish former state checking for exception ;)
        collections['labeled_points'].insert(entry)
        logging.info("[DB]: labeled point saved")
        
    ee.on(Sampler.Events.SAMPLE_COLLECTED, on__sample_collected__save_labeled_point)

    def save_experiment(context, *args, **kwargs):
        entry = context['source'].current_experiment
        try:
            collections['samples'].insert(entry)
            logging.info("[DB]: experiment saved")
        except:
            logging.error("[DB]: failed to save experiment due to exception\n{}\nexperiment was\n{}".format(traceback.format_exc(), entry))
    ee.on(Sampler.Events.PRE_TEST_FAIL, save_experiment)
    ee.on(Sampler.Events.EXPERIMENT_FAIL, save_experiment)
    ee.on(Sampler.Events.EXPERIMENT_TIMEOUT, save_experiment)
    ee.on(Sampler.Events.SAMPLE_COLLECTED, save_experiment)

    class Inference(object):
        def __init__(self, ee, collections):
            self.ee = ee
            self.collections = collections

            self.sampler = None

            ee.on(Sampler.Events.START, self.__save_sampler)
            ee.on(POINT_INFERRED, self._save_inferred_point)

        def __save_sampler(self, context, *args, **kwargs):
            self.sampler = context['source']

        def save_inferred_entry(self, entry):
            try:
                self.collections['labeled_points'].insert(entry)
                logging.info("[DB]: inferred labeled point saved")
            except:
                logging.error("[DB]: failed to save inferred sample due to exception (saved sample with _id = {} in db.unsaved)\n{}".format(entry['_id'], traceback.format_exc()))
                collections['unsaved'].insert(entry)

        def _save_inferred_point(self, context, labeled_point):
            entry = { }
            entry['point']      = labeled_point
            entry['session_id'] = self.sampler.session_id
            entry['_id']        = self.sampler._generate_experiment_id() + 'i'
            entry['timestamp']  = context['time']
            entry['__inferred__'] = True

            self.save_inferred_entry(entry)

    Inference(ee, collections)
