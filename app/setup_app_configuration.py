import logging
import json

# configuration
## configuration file location
CONFIGURATION_FILEPATH = "./config/static.json"

def get_configuration(path = None):
	if path:
	    return load_configuration(path)
	else:
	    return load_configuration(CONFIGURATION_FILEPATH)

def load_configuration(filepath):
    # load configuration file
    with open(CONFIGURATION_FILEPATH) as fp:
        configuration = json.load(fp)
        logging.info("[__MAIN__]: configuration loaded successfully from %s",filepath)
        return configuration
