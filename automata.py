################################################################################
# FSM
from enum import Enum
class Automata_Events(Enum):
    START, TRANSITION, ENTER_STATE, EXIT_STATE, ACCEPTANCE_STATE, END = range(6)

from datetime import datetime
import logging
from uuid import uuid4

class Automata(object):
    Events = Automata_Events
    # __INIT_EVENT__ = "__init_evt__"

    """FSM implementation"""
    def __init__(self, ee, graph, name=uuid4()):
        super(Automata, self).__init__()
        self.ee = ee
        self.graph = graph
        self.name = name

        self.reset()

    def start(self):
        assert self.current_state is None
        assert self.ended is False

        # register all events
        self.ee.on('*', self._on_event)

        # set initial state
        self.current_state = self.graph.initial_state
        self.ee.emit(Automata.Events.START, __context__=self._get_evt_args())

    def reset(self):
        self.current_state = None
        self.ended   = False
        self.crashed = False


    def stop(self):
        # disconnect event handler
        #self.ee.off('*', self._on_event)

        #TODO implementation (callable only when in an acceptance state? - otherwise signal as crashed)
        self.ended = True

    def _on_event(self, context, *args, **kwargs):
        # check if event triggers a transition
        event = context['event']
        if event not in self.current_state.transitions:
            return # ignore event

        next_state = self.current_state.transitions[event]

        #TODO define function (set_state(self, state))
        # change state
        #  leave state
        #   notify end of the state
        self.ee.emit(Automata.Events.EXIT_STATE, __context__=self._get_state_evt_args())

        #  perform transition
        previous_state = self.current_state
        self.current_state = next_state
        #   notify transition
        self.ee.emit(Automata.Events.TRANSITION, __context__=self._get_transition_evt_args(previous_state, event))

        #  enter next state
        #   notify begin new state
        self.ee.emit(Automata.Events.ENTER_STATE, __context__=self._get_state_evt_args())

        # # check if an action is registered to the current state
        #TODO implement actions using only the event mapping?
        #if 'action' in self.current_state and self.current_state['action'] is not None:
        if self.current_state.action is not None:
            # run it
            try:
                self.current_state.action(*args, **kwargs)
            except:
                logging.error("[Automata - {}]: Exception raised during action execution in State({}, {})".format(self.name, self.current_state._id, self.current_state.name))
                import traceback
                traceback.print_exc()
                self.crashed = True


        #   check if it's an acceptance state
        # if it's an acceptance state mark as ended
        #TODO check FSM formalism and how and when they should stop (an acceptance
        # state does not necessarly coicide with an end of the FSM execution)
        if self.current_state.final:
            self.ee.emit(Automata.Events.ACCEPTANCE_STATE, __context__=self._get_state_evt_args())

            # FIXME reaching a final state means to end the FSM
            self.ee.emit(Automata.Events.END, __context__=self._get_evt_args())
            self.ended = True
            self.stop()

        #   check for epsilon transitions
        #TODO remove (put in proper handler)
        logging.info("\nSTATE:       {}\n".format(self.current_state._id))
        if Transition.__EPSILON_TRANSITION__ in self.current_state.transitions:
            # FIXME add proper implementation
            # quickfix: notify the epsilon transition as an event (push in the event queue)
            #  - NOT SAFE! - if other events are in the queue they could be lost before being handled correctly
            self.ee.emit(Transition.__EPSILON_TRANSITION__, __context__=self._get_evt_args())

    # event args generation
    def _get_evt_args(self):
        return {
            'source': self,
            'time': datetime.now()
        }

    def _get_state_evt_args(self):
        res = self._get_evt_args()

        res['state'] = self.current_state,

        return res

    def _get_transition_evt_args(self, previous_state, transition_event):
        res = self._get_evt_args()

        res['source'] = previous_state
        res['target']   = self.current_state
        res['event'] = transition_event

        return res
################################################################################


################################################################################
# Graph
def json_graph_builder(json):
    res = Graph()

    for state in json['states']:
        res.states.append(json_state_factory(state))

    for transition in json['transitions']:
        source_id = transition['source'] if 'source' in transition else transition['from']
        source = res.get_state_by_id(source_id)

        target_id = transition['target'] if 'target' in transition else transition['to']
        target = res.get_state_by_id(target_id)

        trigger = transition['trigger']

        source.transitions[trigger] = target

        res.transitions.append(Transition(trigger, source, target))

    return res


def json_state_factory(json):
    return State(**json)

class State(object):
    def __init__(self, _id, name=None, initial=False, final=False, description="", action=None):
        assert _id is not None

        self._id = _id
        self.name = _id if name is None else name
        self.initial = initial
        self.final = final
        self.description = description
        self.action = action # TODO check if implementing actions here is a good solution

        self.transitions = {}


class Transition(object):
    __EPSILON_TRANSITION__ = "__epsilon__"

    def __init__(self, trigger, source, target):
        self.trigger = trigger
        self.source = source
        self.target = target

class Graph(object):
    """FSM behaviour graph"""
    def __init__(self):
        super(Graph, self).__init__()

        self.states = []
        self.transitions = []

        self.__initial_state = None

    def get_state_by_id(self, _id):
        for state in self.states:
            if state._id is _id:
                return state

    @property
    def initial_state(self):
        # get initial state checking integrity of the graph (one and only one)
        res = None
        for state in self.states:
            if state.initial:
                if res is None:
                    res = state
                else:
                    raise ValueError("Cannot define more than one initial state in a FSM")

        return res
################################################################################
