// condition.js
// requires: jQuery
/*
Define an object that models a condition to raise an event:
if the check_fn returns true then the associated event will be triggered
*/
Condition = function(check_fn, ee, evt_name, evt_args) {
	this.check_fn = check_fn
	this.ee       = ee
	this.evt_name = evt_name
	this.evt_args = evt_args
}
Condition.prototype.check = function() {
	if (this.check_fn()) {
		var args = {
			'source': this,
			'time': new Date()
		}

		if (this.evt_args != null) {
			$.extend(args, this.evt_args)
		}

		this.ee.emit(this.evt_name, this.evt_args)
		return true
	}
	return false
}
