// youtube.experiment.sensing.jointime.js

// / log jointime
// var enable_jointime_sensing = function(sampler, EVENTS) {
YoutubeSampler.prototype.enable_jointime_sensing = function() {
	sampler = this

	// flags
	var is_first_buffering_event 	= true
	var is_first_playing_event 		= true

	// first buffering event handling
	ee.on(sampler.EVENTS.YoutubePlayer__StateChanged, function(evt_args) {
		// check source
		if (evt_args.source !== sampler.player)
			return

		// if first buffering event:
		if (evt_args.state === YT.PlayerState.BUFFERING && is_first_buffering_event) {
			// init jointime attribute
			sampler.experiment_result.jointime = { }

			// shortcut
			var jointime = sampler.experiment_result.jointime

			// save experiment start time
			jointime.begin = evt_args.time

			is_first_buffering_event = false
		}
	})
	// first playing event handling
	ee.on(sampler.EVENTS.YoutubePlayer__StateChanged, function(evt_args) {
		// check source
		if (evt_args.source !== sampler.player)
			return

		// if first playing event:
		if (evt_args.state === YT.PlayerState.PLAYING && is_first_playing_event) {
			// shortcut
			var jointime = sampler.experiment_result.jointime

			// save jointime (delta and absolute time)
			jointime.end   = evt_args.time
			jointime.delta = jointime.end - jointime.begin

			is_first_playing_event = false
		}
	})

	// #TODO add support for multiple experiments (event required to reset flags)
	/*
	ee.on(sampler.EVENTS.Experiment__Started, function(evt_args) {
		// check source
		if (evt_args.source !== sampler.player)
			return

		// reset flags
		is_first_buffering_event 	= true
		is_first_playing_event 		= true		
	})
	*/
}