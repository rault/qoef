// websocket.encoding.json.js
JsonEncodedWebSocket = function(websocket) {
	// wrap onmessage handler to decode JSON encoded message
	var _onmessage = websocket.onmessage
	_onmessage_wrapper = function(e) {
		e = $.extend({}, e)
		e.data = JSON.parse(e.data)

		// super.onmessage(e)
		_onmessage_wrapper._wrapped_fn.call(websocket, e)
	}
	_onmessage_wrapper._wrapped_fn = _onmessage

	websocket.onmessage = _onmessage_wrapper

	// wrap send to encode JSON the message
	var _send = websocket.send
	_send_wrapper = function(data) {
		data = JSON.stringify(data)

		// super.send(data)
		_send_wrapper._wrapped_fn.call(websocket, data)
	}
	_send_wrapper._wrapped_fn = _send

	websocket.send = _send_wrapper

	return websocket
}
