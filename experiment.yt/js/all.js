// Client Sampler
var STATIC_CONFIGURATION_FILEPATH = '../../config/static.json'

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// events.js
var EVENTS = {
	ServerConnection__Opened: 				"ServerConnection__opened",								// { 'source': server_connection, 'time': new Date(), 'event': e }
	ServerConnection__Closed: 				"ServerConnection__closed",								// { 'source': server_connection, 'time': new Date(), 'event': e }
	ServerConnection__Error: 				"ServerConnection__error",								// { 'source': server_connection, 'time': new Date(), 'event': e }
	ServerConnection__MessageReceived: 		"ServerConnection__message_received",					// { 'source': server_connection, 'time': new Date(), 'event': e, 'data': e.data }

	YoutubeApi__Ready:    					"Youtube_API__ready",

// #TODO is it a YoutubeSampler event more than a YoutubePlayer one?
	YoutubePlayer__ReadyToInit: 			"Youtube_Player__ready_to_init", 						// { 'source': condition, 'time': new Date() }

	YoutubePlayer__Ready: 					"Youtube_Player__ready",								// { 'source': e.target, 'time': new Date(), 'event': e, 'source_id': id }
	YoutubePlayer__StateChanged: 			"Youtube_Player__state_changed",						// { 'source': e.target, 'time': new Date(), 'event': e, 'source_id': id }
	YoutubePlayer__Error: 					"Youtube_Player__error",								// { 'source': e.target, 'time': new Date(), 'event': e, 'source_id': id }

	YoutubePlayer_Playback_QualityChanged: 	"Youtube_Player_Playback__quality_changed",
	YoutubePlayer_Playback_RateChanged: 	"Youtube_Player_Playback__rate_changed"

// #TODO experiment is part of YoutubeSampler (who manages the experiment)?
	Experiment_Configuration__Loaded: 		"Experiment_Configuration__loaded",						// { 'source': $, 'time': new Date(), 'configuration': configuration, 'loaded': json }
	Experiment__ReadyToStart: 				"Experiment__ready_to_start",
	Experiment__StartRequested: 			"Experiment__start_requested",
	Experiment__Started: 					"Experiment__started", 
	Experiment__Completed: 					"Experiment__completed",
	Experiment__Error: 						"Experiment__error",

	StaticConfiguration__Loaded: 			"Static_Configuration__loaded",							// { 'source': $, 'time': new Date(), 'configuration': configuration, 'loaded': json }
	ReloadRequest: 							"Reload_Request"
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// setup runtime environment
page 												= { }						// information about the page
page.start 											= new Date()				// start time
ee 													= new EventEmitter()		// event emitter
server_connection 									= undefined 				// websocket connection
sampler 											= undefined 				// sampler (experiment manager)

configuration 										= { } 						// global configuration
configuration.websocket 							= { }
configuration.websocket.host 						= undefined
configuration.websocket.port 						= undefined
configuration.experiment 							= { }
configuration.experiment.configuration_path 		= undefined
configuration.player								= { }
configuration.player.target_dom_selector 			= undefined
configuration.player.target_dom_element 			= undefined
configuration.player.target 						= undefined
configuration.player.width 							= undefined
configuration.player.height 						= undefined
configuration.playback 								= { }
configuration.playback.video_id 					= undefined
configuration.playback.time 						= { }
configuration.playback.time.begin 					= undefined
configuration.playback.time.end 					= undefined
configuration.playback.quality 						= undefined

configuration.messages 								= { } 						// messages descriptors { 'code': int, 'text': String }
configuration.messages.Experiment__ReadyToStart 	= undefined					// message.code =  1
configuration.messages.Experiment__Completed 		= undefined 				// message.code =  2
configuration.messages.Experiment__Error 			= undefined 				// message.code =  3
configuration.messages.ReloadRequest 				= undefined 				// message.code = 20
configuration.messages.Playback__Start_Request 		= undefined 				// message.code = 40
configuration.errors 								= { }						// error descriptors { 'code': int, 'text': String }
configuration.errors.RequestNotValid				= undefined					// error.code =   2, Youtube Player Error: request is not valid
configuration.errors.PlayerError					= undefined					// error.code =   5, Youtube Player Error: impossible to play the video (player error)
configuration.errors.VideoNotAvailable				= undefined					// error.code = 100, Youtube Player Error: video not found, private or removed
configuration.errors.VideoNotEmbeddable				= undefined					// error.code = 101, Youtube Player Error: video cannot be played in embedded player (forbidden)
configuration.errors.VideoNotEmbeddable2			= undefined					// error.code = 150, Youtube Player Error: video cannot be played in embedded player (forbidden) - same as 101
configuration.errors.PlaybackQualityChanged			= undefined					// error.code = 500, Custom Experiment Error: video playback changed quality during experiment
configuration.errors.PlaybackRateChanged 			= undefined 				// error.code = 501, Custom Experiment Error: video playback changed rate during experiment
configuration.errors.PlaybackPaused 				= undefined 				// error.code = 502, Custom Experiment Error: video playback explicitely paused during experiment

// shortcuts
var conf 									= configuration
// var ws_conf 								= configuration.websocket
// var player 								= configuration.player
// var playback 							= configuration.playback

// Load Configuration
// / Load static configuration
$.getJSON(STATIC_CONFIGURATION_FILEPATH).done(
	function(json) {
		// merge object
		$.extend(configuration, json)

		ee.emit(EVENTS.StaticConfiguration__Loaded, { 'source': $, 'time': new Date(), 'configuration': configuration, 'loaded': json })
	}
).fail(
	function(jqxhr, textStatus, error) {
		// impossible to load static configuration
		var errorStr = textStatus + ', ' + error

		console.log('Request Failed: ' + errorStr)
		console.log('Static configuration is missing')
	}
)


// / Download experiment configuration from the server
$.getJSON(configuration.experiment.configuration_path).done(
	function(json) {
		// merge object
		$.extend(configuration, json)

		ee.emit(EVENTS.Experiment_Configuration__Loaded, { 'source': $, 'time': new Date(), 'configuration': configuration, 'loaded': json })
	}
).fail(
	function(jqxhr, textStatus, error) {
		// impossible to load static configuration
		var errorStr = textStatus + ', ' + error

		console.log('Request Failed: ' + errorStr)
		console.log('Experiment configuration is missing')
	}
)

// define core actions
reload_page = function(ee, evt_args) {
	// notify that the reload_page has been called
	ee.emit(EVENTS.ReloadRequest, evt_args)

	location.reload(true) // true to avoid the use of the cache

	// #TODO implement abortion?
	// if (!runtime_args.abort_reload) {
	// 	reload()
	// }
	// runtime_args.abort_reload = false
}

// define core classes
// #TODO remove big mess with global configuration (remove when not strictly needed)
YoutubeSampler = function(ee, configuration) {
	var _this = this

	// public properties
	this.player = undefined

	// public methods
	this.start_playback = function() {
		if (this.player === undefined) {
			throw Error("Cannot start playback, youtube player not ready (did you wait for YoutubeSampler__Ready event?)")
		}

		this.player.loadVideoById({
			'videoId': 			configuration.playback.video_id,
			'startSeconds': 	configuration.playback.time.begin,
			'endSeconds': 		configuration.playback.time.end,
			'suggestedQuality': configuration.playback.quality
		})

		ee.emit(EVENTS.Experiment__Started, { 'source': this})
	}

	// init youtube player
	var YoutubeAPI = evt_args.source

	// take target DOM element (not jQuery one)
	configuration.player.target_dom_element = $(configuration.player.target_dom_selector)[0]

	// init Youtube Player
	initYoutubePlayer(YoutubeAPI, configuration.player)

	// event bindings
	// / set ready when youtube player is ready
	ee.on(EVENTS.YoutubePlayer__Ready, function(evt_args) {
		_this.player = evt_args.source

		ee.emit(EVENTS.YoutubeSampler__Ready, { 'source': _this, 'time': new Date() })
	})

	// / 
}

// sampler initialization
ee.on(EVENTS.YoutubePlayer__ReadyToInit, function(evt_args) {
	// init sampler
	sampler = new YoutubeSampler(ee, configuration)
})

// condition to notify that the experiment is ready to start
ee.on(EVENTS.YoutubeSampler__Ready, function(evt_args) {
	// notify that all conditions to start the experiment are met
	ee.emit(EVENTS.Experiment__ReadyToStart, { 'source': null, 'time': evt_args.time }) // #TODO setup a condition and use it as source (condition to be met before triggering Experiment Ready to Start)
})




get_error_descriptor_by_code = function(error_code) {
	for (var error in configuration.errors) {
		if (error.code === error_code) {
			return error
		}
	}
	// return undefined // (implicit)
}

// status observers

/*
Defined states by Youtube API:
 - unstarted 	(Not Defined) 					= -1
 - ended		YT.PlayerState.ENDED 			=  0
 - playing 		YT.PlayerState.PLAYING 			=  1
 - pause		YT.PlayerState.PAUSED 			=  2
 - buffering 	YT.PlayerState.BUFFERING 		=  3
 - cued 		YT.PlayerState.CUED 			=  5
*/

var experiment_result = { }

// / log jointime
var experiment_result.jointime 	= { }
var jointime 					= experiment_result.jointime // shortcut
var is_first_buffering_event 	= true
var is_first_playing_event 		= true
ee.on(EVENTS.YoutubePlayer__StateChanged, function(evt_args) {
	// if first buffering event:
	if (evt_args.state === YT.PlayerState.BUFFERING && is_first_buffering_event) {
		// save experiment start time
		jointime.begin = evt_args.time

		is_first_buffering_event = false
	}
})
ee.on(EVENTS.YoutubePlayer__StateChanged, function(evt_args) {
	// if first playing event:
	if (evt_args.state === YT.PlayerState.PLAYING && is_first_playing_event) {
		// save jointime (delta and absolute time)
		jointime.end   = evt_args.time
		jointime.delta = jointime.end - jointime.begin

		is_first_playing_event = false
	}
})

// / experiment end conditions
ee.on(EVENTS.YoutubePlayer__StateChanged, function(evt_args) {
	// if ended:
	if (evt_args.state === YT.PlayerState.ENDED) {
		// notify experiment success
		ee.emit(EVENTS.Experiment__Completed, { 'source': sampler, 'time': evt_args.time, 'result': experiment_result, 'error': false })
	}
})
// requires Sampler
ee.on(EVENTS.YoutubePlayer__Error, function(evt_args) {
	// error detected: notify experiment error
	ee.emit(EVENTS.Experiment__Error, { 'source': sampler, 'time': evt_args.time, 'result': experiment_result, 'error': get_error_descriptor_by_code(evt_args.error) })
})

// / log buffering events and duration
var experiment_result.buffering_events 	= []
var buffering_events 					= experiment_result.buffering_events
var current_buffering_event 			= {}
ee.on(EVENTS.YoutubePlayer__StateChanged, function(evt_args) {
	// if buffering:
	if (evt_args.state === YT.PlayerState.BUFFERING) {
		// save time
		current_buffering_event.begin = evt_args.time
	}
})
ee.on(EVENTS.YoutubePlayer__StateChanged, function(evt_args) {
	// if playing:
	if (evt_args.state === YT.PlayerState.PLAYING) {
		// save time (delta with buffering event + absolute time)
		current_buffering_event.end   = evt_args.time
		current_buffering_event.delta = current_buffering_event.end - current_buffering_event.begin

		// save and reset current buffering event
		buffering_events.push(current_buffering_event)
		current_buffering_event = { }		
	}
})

// / detect when experiment is compromized
ee.on(EVENTS.YoutubePlayer__StateChanged, function(evt_args) {
	// if pause:
	if (evt_args.state === YT.PlayerState.PAUSED) {
		// notify error (video stopped manually, experiment compromized)
		ee.emit(EVENTS.Experiment__Error, { 'source': sampler, 'time': evt_args.time, 'result': experiment_result, 'error': configuration.errors.PlaybackPaused})
	}
})
ee.on(EVENTS.YoutubePlayer_Playback_QualityChanged, function(evt_args) {
	// if not suggested one:
	if (evt_args.quality === configuration.playback.quality) {
		// notify experiment error with result
		ee.emit(EVENTS.Experiment__Error, { 'source': sampler, 'time': evt_args.time, 'result': experiment_result, 'error': configuration.errors.PlaybackPaused})
	}
})

// #TODO rate not supported in experiment configuration yet
/*
ee.on(EVENTS.YoutubePlayer_Playback_RateChanged, function(evt_args) {
	// #TODO if not suggested one:
	// 	stop experiment and send result as error
})
*/


// #TODO write handler for sending data (experiment error and experiment completed)
ee.on(EVENTS.Experiment__Completed, function(evt_args) {
	server.send_result(evt_args.result)
})
ee.on(EVENTS.Experiment__Error, function(evt_args) {
	server.send_error(evt_args.error, evt_args.result)
})

// #TODO old (now in YoutubeSampler)
/*
// page reload
var reload_page = function(ee, evt_args) {
	ee.emit(EVENTS.ReloadRequest, evt_args)

	location.reload(true) // true to avoid the use of the cache

	// #TODO implement abortion?
	// if (!runtime_args.abort_reload) {
	// 	reload()
	// }
	// runtime_args.abort_reload = false
}

// start playback
var start_playback = function() {
	configuration.player.target.loadVideoById({
		'videoId': 			playback.video_id,
		'startSeconds': 	playback.time.begin,
		'endSeconds': 		playback.time.end,
		'suggestedQuality': playback.quality
	})

	ee.emit(EVENTS.Experiment__Started, { 'source': this})
}
*/


// Connect WebSocket
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// event-websocket.js
EventWebSocket = function(ee, configuration) {
	var server_connection = new WebSocket('ws://' + configuration.host + ':' + configuration.port)

	server_connection.onopen = function(e) {
		ee.emit(EVENTS.ServerConnection__Opened, 	{ 'source': server_connection, 'time': new Date(), 'event': e })
	}
	server_connection.onerror = function(e) {
		ee.emit(EVENTS.ServerConnection__Error, 	{ 'source': server_connection, 'time': new Date(), 'event': e })
	}
	server_connection.onclose = function(e) {
		ee.emit(EVENTS.ServerConnection__Closed, 	{ 'source': server_connection, 'time': new Date(), 'event': e })
	}

	server_connection.onmessage = function(e) {
		ee.emit(EVENTS.ServerConnection__MessageReceived, { 'source': server_connection, 'time': new Date(), 'event': e, 'data': e.data })
	}

	return server_connection
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// websocket_json_encoding.js
JsonEncodedWebSocket = function(websocket) {
	// wrap onmessage handler to decode JSON encoded message
	var _onmessage = server_connection.onmessage
	server_connection.onmessage = function(e) {
		e.data = JSON.parse(e.data)

		// super.onmessage(e)
		server_connection.onmessage._wrapped_fn.call(server_connection, e)
	}
	server_connection.onmessage._wrapped_fn = _onmessage

	// wrap send to encode JSON the message
	var _send = server_connection.send
	server_connection.send = function(data) {
		data = JSON.stringify(data)

		// super.send(data)
		server_connection.send._wrapped_fn.call(server_connection, data)
	}
	server_connection.send._wrapped_fn = _send
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
server_connection = JsonEncodedWebSocket(
	EventWebSocket(ee, configuration.websocket)
)


// setup server object
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// server.js
Server = function(ee, server_connection) {
	var _this = this

	this.ee = ee
	this.server_connection = server_connection

	var getMessage = function(message_descriptor) {
		// Shallow copy
		return jQuery.extend({}, message_descriptor)
	}

	this.notify_ready_to_start_experiment = function() {
		// setup message
		var msg = getMessage(configuration.messages.Experiment__ReadyToStart)

		// send message
		server_connection.send(msg)
	}

	this.send_error = function(error, result) {
		// setup message
		var msg = getMessage(configuration.messages.Experiment__Error)
		msg.result = result
		msg.error  = error

		// send message
		server_connection.send(msg)
	}

	this.send_result = function(result) {
		// setup message
		var msg = getMessage(configuration.messages.Experiment__Completed)
		msg.result = result
		msg.error  = false

		// send message
		server_connection.send(msg)
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
server = Server(ee, server_connection)

// setup server interaction
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// websocket.message-handler.js
/*
Template function to manage ServerConnection__MessageReceived events from a websocket (event based).
If the desired message type arrives an action is performed

@args ee EventEmitter
@args message_descriptor { code: ID }
@args action callable action to be performed if the message type of the message arrived is the one in the message descriptor
@args message_source (optional) source of the messages (to filter from other sources); if omitted accepts messages from any source
*/
MessageHandler = function(ee, message_descriptor, action, message_source) {
	var _fn = function(evt_args) {
		// check if we received a message of interest
		if(evt_args.data.code !== message_descriptor.code)
			return

		// perform action
		action(evt_args)
	}

	if (message_source != null) {
		// define handler as single source handler (only one source)
		_fn = function(evt_args) {
			// check if the source is the desired one
			if (evt_args.source !== message_source)
				return

			_fn(evt_args)
		}
	}

	return _fn
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// / message handling
// // reload page message
ee.on(
	EVENTS.ServerConnection__MessageReceived, 
	new MessageHandler(
		ee, 
		configuration.messages.ReloadRequest,
		function(evt_args) { 
			reload_page(ee, { 'source': server, 'time': evt_args.time })
		}
	)
)

// // start playback message
ee.on(
	EVENTS.ServerConnection__MessageReceived,
	new MessageHandler(
		ee, 
		configuration.messages.Playback__Start_Request,
		function(evt_args) {
			ee.emit(EVENTS.Experiment__StartRequested, { 'source': server, 'time': evt_args.time })
		}
	)
)

// / message dispatching
// // notify to server when ready
ee.on(EVENTS.Experiment__ReadyToStart, function(evt_args) {
	// send message to server
	server.notify_ready_to_start_experiment()
})


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// message_handler.js
// #TODO substitute with a "rule" based setup (define a single message_handler for each message type - source is Server object)
// #FIX implemented single message handlers
// #TODO remove when I'm sure it's useless
/*
MessageHandler = function(ee, configuration, message_source, runtime_args) {
	_this = this

	ee.on(EVENTS.ServerConnection__MessageReceived, function(evt_args) {
		if (evt_args.source !== message_source)
			return

		evt_args = { 'source': _this, 'time': evt_args.time }

		switch(evt_args.data.code) {
			case configuration.messages.ReloadRequest.code:
				reload_page(ee, evt_args, configuration, runtime_args)
				break
			case configuration.messages.Playback__Start_Request.code:
				ee.emit(EVENTS.Experiment__StartRequested)
				break
		}
	})
}
*/
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// init youtube player
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// event_yt_player.js #TODO?
// event support for the Youtube API
function onYoutubeAPI__Ready() {
	ee.emit(EVENTS.YoutubeApi__Ready, { 'source': YT, 'time': new Date() })
}

// #TODO support for full configuration
/*
@arg id Object (optional) player id - useful if multiple player are defined
*/
var initYoutubePlayer = function(YoutubeAPI, configuration, id) {
	configuration.player.target = new YoutubeAPI.Player(
		configuration.target_dom_element,
		{
			width: 	configuration.player.width,
			height: configuration.player.height,
			events: {
				onReady: function(e) {
					if (e.target === configuration.player.target) {
						ee.emit(
							EVENTS.YoutubePlayer__Ready, 
							{ 'source': e.target, 'time': new Date(), 'event': e, 'source_id': id }
						)
					}
				},
				onStateChange: function(e) {
					if (e.target === configuration.player.target) {
						ee.emit(
							EVENTS.YoutubePlayer__StateChanged, 
							{ 'source': e.target, 'time': new Date(), 'event': e, 'state': e.data, 'source_id': id }
						)
					}
				},
				onError: function(e) {
					if (e.target === configuration.player.target) {
						ee.emit(
							EVENTS.YoutubePlayer__Error, 
							{ 'source': e.target, 'time': new Date(), 'event': e, 'error': get_error_descriptor_by_code(e.data), 'source_id': id } // converts error code in error descriptor
							// error =   2, if request is not valid
							// error =   5, if it's impossible to play the video
							// error = 100, video not found, private or removed
							// error = 101, video cannot be played in embedded player (forbidden)
							// error = 150, video cannot be played in embedded player (forbidden) - same as 101
						)
					}
				},

				onPlaybackQualityChange: function(e) {
					if (e.target === configuration.player.target) {
						ee.emit(
							EVENTS.YoutubePlayer_Playback_QualityChanged, 
							{ 'source': e.target, 'time': new Date(), 'event': e, 'quality': e.data, 'source_id': id }
						)
					}
				},
				onPlaybackRateChange: function(e) {
					if (e.target === configuration.player.target) {
						ee.emit(
							EVENTS.YoutubePlayer_Playback_RateChanged, 
							{ 'source': e.target, 'time': new Date(), 'event': e, 'rate': e.data, 'source_id': id }
						)
					}
				}
			}
		}		
	)
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// #TODO old (integrated in YoutubeSampler)
/*
ee.on(EVENTS.YoutubeApi__Ready, function(evt_args) {
	var YoutubeAPI = evt_args.source

	// take target DOM element
	configuration.player.target_dom_element = $(configuration.player.target_dom_selector)[0]

	// init Youtube Player
	initYoutubePlayer(YoutubeAPI, configuration.player)
})
*/

// setup checkpoints (blocking conditions waiting for multiple asynchronous conditions)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// condition.js
Condition = function(check_fn, ee, evt_name, evt_args, name) {
	this.check_fn = check_fn
	this.ee       = ee
	this.evt_name = evt_name
	this.evt_args = evt_args
	this.name 	  = name

	// #TODO add to prototype (uses only this keyword to access data - no constructor closure is necessary)
	this.check = function() {
		if (this.check_fn()) {
			var args = {
				'source': this,
				'time': new Date()
			}

			if (this.evt_args != null) {
				$.extend(args, this.evt_args)
			}

			this.ee.emit(this.evt, this.evt_args)
			return true
		}
		return false
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// condition-event_based-binary_state.js
// EventBasedBinaryStateCondition
/*
@arg state_descriptors Array<{ state: String, trigger: Event }>
*/
// NB: supports only positive logic
// #TODO support negative states (describe in state_descriptor and handle with inverse logic)
EventBasedBinaryStateCondition = function(ee, state_descriptors, evt_name, evt_args, name) {
	this.ee 				= ee
	this.state_descriptors 	= state_descriptors

	// init state descriptor (if value is true it won't be overwritten)
	for (var i = state_descriptors.length - 1; i >= 0; i--) {
		var d = state_descriptors[i]

		// set event handler
		ee.on(
			d.trigger,
			function() {
				d.value = true
				this.check()
			})

		// initialize state values
		if (d.value)
			break
		d.value = false
	}

	var check_fn = function() {
		// check that everything is true
		for (var i = state_descriptors.length - 1; i >= 0; i--) {
			if (!d.value) {
				return false
			}
		}
		return true
	}

	Condition.call(this, check_fn, ee, evt_name, evt_args, name) // #TODO setup prototype chain (JavaScript inheritance) - @see http://javascript.info/tutorial/pseudo-classical-pattern or Enrico Denti's course on languages and compilers


}
EventBasedBinaryStateCondition.prototype.check = Condition.prototype.check // #TODO setup prototype chain (JavaScript inheritance) - @see http://javascript.info/tutorial/pseudo-classical-pattern or Enrico Denti's course on languages and compilers

new EventBasedBinaryStateCondition(
	ee, 
	[
		{ 'trigger': EVENTS.ServerConnection__Opened },
		{ 'trigger': EVENTS.Experiment_Configuration__Loaded },
		{ 'trigger': EVENTS.YoutubeApi__Ready }
	],
	EVENTS.YoutubePlayer__ReadyToInit,
	{ },
	"sampler_preconditions"
)


// youtube_player_ready_to_be_initiated_condition
// Check that the Configuration and the API are correctly loaded before declaring that the player can be istantiated (sampler preconditions)

setup_sampler_preconditions = function(ee, EVENTS) {
	// Setup State Variables (flags)
	// #TODO declare state object? (possibility to send object and generalize code - see flag handlers)
	var is_websocket_ready                = false
	var is_youtube_api_ready              = false
	var is_experiment_configuration_ready = false

	// Setup Condition
	var is_ready = function() {
		return is_websocket_ready && 
			   is_youtube_api_ready && 
			   is_experiment_configuration_ready
	}

	var condition = Condition(is_ready, ee, EVENTS.YoutubePlayer__ReadyToInit) // PreconditionsPassed
	condition.name = "sampler_preconditions"

	// Setup Flag Handlers
	ee.on(EVENTS.ServerConnection__Opened,         	function() { is_websocket_ready                = true; condition.check() })
	ee.on(EVENTS.YoutubeApi__Ready,                	function() { is_youtube_api_ready              = true; condition.check() })
	ee.on(EVENTS.Experiment_Configuration__Loaded, 	function() { is_experiment_configuration_ready = true; condition.check() })
}
setup_sampler_preconditions(ee, EVENTS)


// #TODO old
/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// experiment_ready_to_start_condition
// triggers Experiment__ReadyToStart event when all the preconditions are met
// preconditions: is_youtube_player_ready (triggered by YoutubePlayer__Ready event)
setup_experiment_preconditions = function(ee, EVENTS) {
	ee.on(EVENTS.YoutubePlayer__Ready, function(evt_args) {
		ee.emit(EVENTS.Experiment__ReadyToStart, {})
	})
}

// #TODO old
setup_experiment_preconditions = function(ee, EVENTS) {
	// Setup State Variables (flags)
	// #TODO declare state object? (possibility to send object and generalize code - see flag handlers)
	var is_websocket_ready                = false
	var is_youtube_api_ready              = false
	var is_youtube_player_ready           = false
	var is_experiment_configuration_ready = false

	// Setup Condition
	var is_ready_to_play = function() {
		return is_websocket_ready && 
			   is_youtube_api_ready && 
			   is_youtube_player_ready &&
			   is_experiment_configuration_ready
	}

	var condition = Condition(is_ready_to_play, ee, EVENTS.Experiment__ReadyToStart)

	// Setup Flag Handlers
	ee.on(EVENTS.ServerConnection__Opened,         	function() { is_websocket_ready                = true; condition.check() })
	ee.on(EVENTS.YoutubeApi__Ready,                	function() { is_youtube_api_ready              = true; condition.check() })
	ee.on(EVENTS.YoutubePlayer__Ready,             	function() { is_youtube_player_ready           = true; condition.check() })
	ee.on(EVENTS.Experiment_Configuration__Loaded, 	function() { is_experiment_configuration_ready = true; condition.check() })


	return condition
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
experiment_preconditions = setup_experiment_preconditions(ee, EVENTS)
*/
