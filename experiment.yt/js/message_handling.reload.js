// message_handling.reload.js
// requires: MessageHandler
// // reload page message
var enable_ReloadMessage_handling = function(ee, server, EVENTS, MESSAGES) {

	var reload_page = function(evt_args) {
		// notify that the reload_page has been called
		ee.emit(EVENTS.ReloadRequest, evt_args)

		location.reload(true) // true to avoid the use of the cache

		// #TODO implement abortion?
		// if (!runtime_args.abort_reload) {
		// 	location.reload()
		// }
		// runtime_args.abort_reload = false
	}

	ee.on(
		EVENTS.ServerConnection__MessageReceived, 
		new MessageHandler(
			ee, 
			MESSAGES.ReloadRequest,
			function(evt_args) { 
				reload_page(ee, { 'source': server, 'time': evt_args.time })
			}
		)
	)
}
