// youtube_sampler.error_flags.quality_changed.js
// /// quality changed to unexpected value
YoutubeSampler.prototype.setup_quality_changed_as_error = function(EVENTS) {
	var _this = this

	ee.on(EVENTS.YoutubePlayer_Playback_QualityChanged, function(evt_args) {
		// check if the player is the one binded to the target sampler object
		if (evt_args.source !== _this.player)
			return

		if (evt_args.quality === undefined)
			return

		// if not suggested one:
		if (evt_args.quality !== _this.configuration.playback.quality) {
			// notify experiment error with result
			error = $.extend({}, _this.ERRORS.PlaybackQualityChanged)
			error.quality = {
				supposed: _this.configuration.playback.quality,
				current:  evt_args.quality
			}
			ee.emit(EVENTS.Experiment__Error, { 'source': _this, 'time': evt_args.time, 'result': _this.experiment_result, 'error': error })
		}
	})
}
