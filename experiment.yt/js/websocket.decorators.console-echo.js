// websocket.decorators.console-echo.js
EchoedWebSocket = function(websocket) {
    // wrap onmessage handler
    var _onmessage = websocket.onmessage
    var _onmessage_wrapper = function(e) {
        console.log("[WEBSOCKET]: received")
        console.log(e.data)

        // super.onmessage(e)
        _onmessage_wrapper._wrapped_fn.call(websocket, e)
    }
    _onmessage_wrapper._wrapped_fn = _onmessage

    websocket.onmessage = _onmessage_wrapper

    // wrap send method
    var _send = websocket.send
    var _send_wrapper = function(data) {
        console.log("[WEBSOCKET]: sending")
        console.log(data)

        // super.send(data)
        _send_wrapper._wrapped_fn.call(websocket, data)
    }
    _send_wrapper._wrapped_fn = _send

    websocket.send = _send_wrapper

    return websocket
}
