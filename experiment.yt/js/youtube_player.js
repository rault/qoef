// youtube_player.js
// event support for the Youtube API
var initYoutubeApi = function(EVENTS, script_url) {
	script_url = (script_url === undefined) ? "https://www.youtube.com/iframe_api" : script_url;

	// load IFrame Player API code asynchronously
	var tag = document.createElement('script')
	tag.src = script_url
	
	var firstScriptTag = document.getElementsByTagName('script')[0]
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag)

	// declares global handler (required by YoutubeAPI)
	onYouTubeIframeAPIReady = function() {
		ee.emit(EVENTS.YoutubeApi__Ready, { 'source': YT, 'time': new Date() })
	}
}



var get_error_descriptor_by_code = function(ERRORS, error_code) {
	for (var error in ERRORS) {
		if (error.code === error_code) {
			return error
		}
	}

	// unknown error
	return ERRORS.unknown_error
}

// #TODO support for full configuration
/*
@arg YoutubeAPI YT youtube API reference - reminder that the youtube API must be ready before calling this method (Youtube API is defined in the global variable YT)
@arg configuration Object key value pairs to configure player (the player object will be injected here - in the 'target' key when ready)
@arg id Object (optional) player id - useful if multiple player are defined to recognize which one triggered the events (especially ready)
*/
var initYoutubePlayer = function(YoutubeAPI, configuration, EVENTS, ERRORS, id) {
	configuration.target = new YoutubeAPI.Player(
		configuration.target_dom_element,
		{
			width: 	configuration.width,
			height: configuration.height,
			events: {
				onReady: function(e) {
					if (e.target === configuration.target) {
						ee.emit(
							EVENTS.YoutubePlayer__Ready, 
							{ 'source': e.target, 'time': new Date(), 'event': e, 'source_id': id }
						)
					}
				},
				onStateChange: function(e) {
					if (e.target === configuration.target) {
						ee.emit(
							EVENTS.YoutubePlayer__StateChanged, 
							{ 'source': e.target, 'time': new Date(), 'event': e, 'state': e.data, 'source_id': id }
							/*
							Defined states by Youtube API:
							 - unstarted 	(Not Defined) 					= -1
							 - ended		YT.PlayerState.ENDED 			=  0
							 - playing 		YT.PlayerState.PLAYING 			=  1
							 - pause		YT.PlayerState.PAUSED 			=  2
							 - buffering 	YT.PlayerState.BUFFERING 		=  3
							 - cued 		YT.PlayerState.CUED 			=  5
							*/
						)
					}
				},
				onError: function(e) {
					if (e.target === configuration.target) {
						ee.emit(
							EVENTS.YoutubePlayer__Error, 
							{ 'source': e.target, 'time': new Date(), 'event': e, 'error': get_error_descriptor_by_code(e.data), 'source_id': id } // converts error code in error descriptor
							// error =   2, if request is not valid
							// error =   5, if it's impossible to play the video
							// error = 100, video not found, private or removed
							// error = 101, video cannot be played in embedded player (forbidden)
							// error = 150, video cannot be played in embedded player (forbidden) - same as 101
						)
					}
				},

				onPlaybackQualityChange: function(e) {
					if (e.target === configuration.target) {
						ee.emit(
							EVENTS.YoutubePlayer_Playback_QualityChanged, 
							{ 'source': e.target, 'time': new Date(), 'event': e, 'quality': e.data, 'source_id': id }
						)
					}
				},
				onPlaybackRateChange: function(e) {
					if (e.target === configuration.target) {
						ee.emit(
							EVENTS.YoutubePlayer_Playback_RateChanged, 
							{ 'source': e.target, 'time': new Date(), 'event': e, 'rate': e.data, 'source_id': id }
						)
					}
				}
			}
		}
	)
}
