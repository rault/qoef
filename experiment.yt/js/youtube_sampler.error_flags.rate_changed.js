// youtube_sampler.error_flags.rate_changed.js
// /// rate changed to unexpected value
YoutubeSampler.prototype.setup_rate_changed_as_error = function(EVENTS) {
	_this = this

	ee.on(EVENTS.YoutubePlayer_Playback_RateChanged, function(evt_args) {
		// check if the player is the one binded to this object
		if (evt_args.source !== sampler.player)
			return

		// if not suggested one:
		if (evt_args.rate === sampler.configuration.playback.rate) {
			// stop experiment and send result as error
			ee.emit(EVENTS.Experiment__Error, { 'source': _this, 'time': evt_args.time, 'result': _this.experiment_result, 'error': _this.ERRORS.PlaybackRateChanged})
		}
	})
}
