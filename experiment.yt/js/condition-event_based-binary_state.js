// condition-event_based-binary_state.js
// EventBasedBinaryStateCondition
/*
Reacts on events defined in state_descriptors and triggers evt_name when all the events has been raised in any order

@arg state_descriptors Array<{ state: String, trigger: Event }>
*/
// NB: supports only positive logic
// #TODO support negative states (describe in state_descriptor and handle with inverse logic)
// #TODO support possibility to give only event names in the state_descriptors array (implicit value false, object to create)
EventBasedBinaryStateCondition = function(ee, state_descriptors, evt_name, evt_args, name) {
	this.ee 				= ee
	this.state_descriptors 	= state_descriptors

	var _this = this

	var check_fn = function() {
		// check that everything is true
		for (var i = state_descriptors.length - 1; i >= 0; i--) {
			d = state_descriptors[i]

			if (!d.value) {
				return false
			}
		}
		return true
	}

	// super()
	Condition.call(this, check_fn, ee, evt_name, evt_args, name) // #TODO setup prototype chain (JavaScript inheritance) - @see http://javascript.info/tutorial/pseudo-classical-pattern or Enrico Denti's course on languages and compilers

	// init state descriptor (if value is true it won't be overwritten)
	for (var i = state_descriptors.length - 1; i >= 0; i--) {
		var d = state_descriptors[i]

		// set event handler
		ee.on(
			d.trigger,
			function() {
				d.value = true
				_this.check()
			})

		// initialize state values
		if (d.value)
			break
		d.value = false
	}
}
EventBasedBinaryStateCondition.prototype.check = Condition.prototype.check // #TODO setup prototype chain (JavaScript inheritance) - @see http://javascript.info/tutorial/pseudo-classical-pattern or Enrico Denti's course on languages and compilers
