// events.js
var EVENTS = {
	ServerConnection__Opened: 				"ServerConnection__opened",								// { 'source': server_connection, 'time': new Date(), 'event': e }
	ServerConnection__Closed: 				"ServerConnection__closed",								// { 'source': server_connection, 'time': new Date(), 'event': e }
	ServerConnection__Error: 				"ServerConnection__error",								// { 'source': server_connection, 'time': new Date(), 'event': e }
	ServerConnection__MessageReceived: 		"ServerConnection__message_received",					// { 'source': server_connection, 'time': new Date(), 'event': e, 'data': e.data }

	YoutubeApi__Ready:    					"Youtube_API__ready",

// #TODO is it a YoutubeSampler event more than a YoutubePlayer one?
	YoutubePlayer__ReadyToInit: 			"Youtube_Player__ready_to_init", 						// { 'source': condition, 'time': new Date() }

	YoutubePlayer__Ready: 					"Youtube_Player__ready",								// { 'source': e.target, 'time': new Date(), 'event': e, 'source_id': id }
	YoutubePlayer__StateChanged: 			"Youtube_Player__state_changed",						// { 'source': e.target, 'time': new Date(), 'event': e, 'source_id': id }
	YoutubePlayer__Error: 					"Youtube_Player__error",								// { 'source': e.target, 'time': new Date(), 'event': e, 'source_id': id }

	YoutubePlayer_Playback_QualityChanged: 	"Youtube_Player_Playback__quality_changed",
	YoutubePlayer_Playback_RateChanged: 	"Youtube_Player_Playback__rate_changed",

	YoutubeSampler__Ready: 					"YoutubeSampler__Ready",

// #TODO experiment is part of YoutubeSampler (who manages the experiment)?
	Experiment_Configuration__Loaded: 		"Experiment_Configuration__loaded",						// { 'source': $, 'time': new Date(), 'configuration': configuration, 'loaded': json }
	Experiment__ReadyToStart: 				"Experiment__ready_to_start",
	Experiment__StartRequested: 			"Experiment__start_requested",
	Experiment__Started: 					"Experiment__started", 
	Experiment__Completed: 					"Experiment__completed",
	Experiment__Error: 						"Experiment__error",

	StaticConfiguration__Loaded: 			"Static_Configuration__loaded",							// { 'source': $, 'time': new Date(), 'configuration': configuration, 'loaded': json }
	ReloadRequest: 							"Reload_Request"
}
