// websocket.message-handler.js
// requires: "Message types explicitely modeled and identified by a code (object with 'code' attribute as type ID)"
/*
Template function to manage ServerConnection__MessageReceived events from a websocket (event based).
If the desired message type arrives an action is performed

@args ee EventEmitter
@args message_descriptor { code: ID }
@args action callable action to be performed if the message type of the message arrived is the one in the message descriptor
@args message_source (optional) source of the messages (to filter from other sources); if omitted accepts messages from any source
*/
MessageHandler = function(ee, message_descriptor, action, message_source) {
	var _fn = function(evt_args) {
		// check if we received a message of interest
		if(evt_args.data.code !== message_descriptor.code)
			return

		// perform action
		action(evt_args)
	}

	if (message_source != null) {
		// define handler as single source handler (only one source)
		_fn = function(evt_args) {
			// check if the source is the desired one
			if (evt_args.source !== message_source)
				return

			_fn(evt_args)
		}
	}

	return _fn
}
