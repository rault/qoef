// youtube.experiment.sensing.buffering.js

// / log buffering events and duration
// var enable_buffering_sensing = function(sampler, EVENTS) {
YoutubeSampler.prototype.enable_buffering_sensing = function() {
	sampler = this

	// current observed buffering event object
	var current_buffering_event = { }
	ee.on(sampler.EVENTS.YoutubePlayer__StateChanged, function(evt_args) {
		// check source
		if (evt_args.source !== sampler.player)
			return

		// if buffering:
		if (evt_args.state === YT.PlayerState.BUFFERING) {
			// init buffering object
			current_buffering_event = { }

			// save time
			current_buffering_event.begin = evt_args.time
		}
	})

	var _filter_buffering_events = function(buffering_events) {
		// don't take into account buffering events that last than 100 ms
		var count = 0
		for (var i = 0; i < buffering_events.length; i++) {
			var be = buffering_events[i]
			if(be.delta < 100) {
				count++
			}
		}
		return count
	}
	ee.on(sampler.EVENTS.YoutubePlayer__StateChanged, function(evt_args) {
		// check source
		if (evt_args.source !== sampler.player)
			return

		// if playing:
		if (evt_args.state === YT.PlayerState.PLAYING) {
			// save time (delta with buffering event + absolute time)
			current_buffering_event.end   = evt_args.time
			current_buffering_event.delta = current_buffering_event.end - current_buffering_event.begin

			// save and reset current buffering event
			sampler.experiment_result.buffering_events.push(current_buffering_event)

			//////////////////////////////////////////////////////////////////////////
			// FIXME temporary quickfix to accelerate tests (record only one buffer event, if others, conclude experiment and notify as bad quality (on server))
			if(sampler.experiment_result.buffering_events.length > 1 && current_buffering_event.delta > 1000) {
				ee.emit(EVENTS.Experiment__Completed, { 'source': _this, 'time': evt_args.time, 'result': _this.experiment_result, 'error': false })
			}

			if(_filter_buffering_events(sampler.experiment_result.buffering_events) == 11) {
				ee.emit(EVENTS.Experiment__Completed, { 'source': _this, 'time': evt_args.time, 'result': _this.experiment_result, 'error': false })
			}
			//////////////////////////////////////////////////////////////////////////
		}
	})

	// #TODO add support for multiple experiments (event required to reset flags)
	ee.on(EVENTS.Experiment__Started, function(evt_args) {
		// check source
		if (evt_args.source !== sampler)
			return

		// reset buffering results
		sampler.experiment_result.buffering_events 	= [ ]
	})
}
