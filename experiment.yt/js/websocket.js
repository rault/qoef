// websocket.js
/*
Event based web socket wrapper
*/
EventWebSocket = function(ee, configuration) {
	var server_connection = new WebSocket('ws://' + configuration.host + ':' + configuration.port)

	server_connection.onopen = function(e) {
		ee.emit(EVENTS.ServerConnection__Opened, 	{ 'source': server_connection, 'time': new Date(), 'event': e })
	}
	server_connection.onerror = function(e) {
		ee.emit(EVENTS.ServerConnection__Error, 	{ 'source': server_connection, 'time': new Date(), 'event': e })
	}
	server_connection.onclose = function(e) {
		ee.emit(EVENTS.ServerConnection__Closed, 	{ 'source': server_connection, 'time': new Date(), 'event': e })
	}

	server_connection.onmessage = function(e) {
		ee.emit(EVENTS.ServerConnection__MessageReceived, { 'source': server_connection, 'time': new Date(), 'event': e, 'data': e.data })
	}

	return server_connection
}
