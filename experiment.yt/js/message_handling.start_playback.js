// message_handling.start_playback.js
// requires: MessageHandler
// // start playback message
var enable_StartPlayback_handling = function(ee, server, EVENTS, MESSAGES) {
	ee.on(
		EVENTS.ServerConnection__MessageReceived,
		MessageHandler(
			ee, 
			MESSAGES.Playback__Start_Request,
			function(evt_args) {
				ee.emit(EVENTS.Experiment__StartRequested, { 'source': server, 'time': evt_args.time })
			}
		)
	)
}
