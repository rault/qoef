// actions.reload.js
// #TODO not in use (embedded in message_handling.reload.js)
var ReloadAction = function(ee, EVENTS) {
	return function(evt_args) {
		// notify that the reload_page has been called
		ee.emit(EVENTS.ReloadRequest, evt_args)

		location.reload(true) // true to avoid the use of the cache

		// #TODO implement abortion?
		// if (!runtime_args.abort_reload) {
		// 	location.reload()
		// }
		// runtime_args.abort_reload = false
	}
}
