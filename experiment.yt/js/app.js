//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// events.js
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Client Sampler
var STATIC_CONFIGURATION_FILEPATH = '../../config/static.json'


// setup runtime environment
ee 													= new EventEmitter()		// event emitter
server_connection 									= undefined 				// websocket connection
sampler 											= undefined 				// sampler (experiment manager)

// runtime variables
// #TODO configuration is misleading, this is the public status of the program
configuration 										= { } 						// global configuration
configuration.websocket 							= { }
configuration.websocket.host 						= undefined
configuration.websocket.port 						= undefined

configuration.experiments 							= { }
// #TODO #multiple_experiment_configurations multiple experiment configurations (load dynamically playback settings)
/*
configuration.experiments.configuration_path 		= undefined
*/
configuration.experiments.player								= { }
configuration.experiments.player.target_dom_selector 			= undefined
configuration.experiments.player.target_dom_element 			= undefined
configuration.experiments.player.target 						= undefined
configuration.experiments.player.width 							= undefined
configuration.experiments.player.height 						= undefined
configuration.experiments.playback 								= { }
configuration.experiments.playback.video_id 					= undefined
configuration.experiments.playback.time 						= { }
configuration.experiments.playback.time.begin 					= undefined
configuration.experiments.playback.time.end 					= undefined
configuration.experiments.playback.quality 						= undefined

configuration.messages 								= { } 						// messages descriptors { 'code': int, 'text': String }
configuration.messages.Experiment__ReadyToStart 	= undefined					// message.code =  1
configuration.messages.Experiment__Completed 		= undefined 				// message.code =  2
configuration.messages.Experiment__Error 			= undefined 				// message.code =  3
configuration.messages.ReloadRequest 				= undefined 				// message.code = 20
configuration.messages.Playback__Start_Request 		= undefined 				// message.code = 40
configuration.errors 								= { }						// error descriptors { 'code': int, 'text': String }
configuration.errors.RequestNotValid				= undefined					// error.code =   2, Youtube Player Error: request is not valid
configuration.errors.PlayerError					= undefined					// error.code =   5, Youtube Player Error: impossible to play the video (player error)
configuration.errors.VideoNotAvailable				= undefined					// error.code = 100, Youtube Player Error: video not found, private or removed
configuration.errors.VideoNotEmbeddable				= undefined					// error.code = 101, Youtube Player Error: video cannot be played in embedded player (forbidden)
configuration.errors.VideoNotEmbeddable2			= undefined					// error.code = 150, Youtube Player Error: video cannot be played in embedded player (forbidden) - same as 101
configuration.errors.PlaybackQualityChanged			= undefined					// error.code = 500, Custom Experiment Error: video playback changed quality during experiment
configuration.errors.PlaybackRateChanged 			= undefined 				// error.code = 501, Custom Experiment Error: video playback changed rate during experiment
configuration.errors.PlaybackPaused 				= undefined 				// error.code = 502, Custom Experiment Error: video playback explicitely paused during experiment

// Load Configuration
// / Load static configuration
$.getJSON(STATIC_CONFIGURATION_FILEPATH).done(
	function(json) {
		// merge object
		$.extend(configuration, json)

		ee.emit(EVENTS.StaticConfiguration__Loaded, { 'source': $, 'time': new Date(), 'configuration': configuration, 'loaded': json })
	}
).fail(
	function(jqxhr, textStatus, error) {
		// impossible to load static configuration
		var errorStr = textStatus + ', ' + error

		console.log('Request Failed: ' + errorStr)
		console.log('Static configuration is missing')
	}
)

ee.on(
	EVENTS.StaticConfiguration__Loaded, 
	function(evt_args) {
		configuration = evt_args.configuration

		// / set current experiment configuration (static)
		configuration.experiment = {
			'player': 	$.extend({}, configuration.experiments.player),
			'playback': $.extend({}, configuration.experiments.playback)
		}

		// #TODO #multiple_experiment_configurations multiple experiment configurations (load dynamically playback settings)
		// / Download current experiment configuration from the server
		/*
		$.getJSON(configuration.experiment.configuration_path).done(
			function(json) {
				// merge object
				$.extend(configuration, json)

				ee.emit(EVENTS.Experiment_Configuration__Loaded, { 'source': $, 'time': new Date(), 'configuration': configuration, 'loaded': json })
			}
		).fail(
			function(jqxhr, textStatus, error) {
				// impossible to load static configuration
				var errorStr = textStatus + ', ' + error

				console.log('Request Failed: ' + errorStr)
				console.log('Experiment configuration is missing')
			}
		)
		*/



		// Connect WebSocket
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// websocket.js
		// websocket.decorators.console-echo.js
		// websocket.encoding.json.js
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		server_connection = EchoedWebSocket(
			JsonEncodedWebSocket(
				EventWebSocket(ee, configuration.websocket)
			)
		)


		// setup server object
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// server.js
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		server = new Server(ee, server_connection, configuration.messages)

		// setup server interaction
		enable_server_interaction(server)

		// setup server interaction
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// websocket.message-handler.js
		// actions.reload.js
		// message_handling.reload.js
		// message_handling.start_playback.js
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// / message handling
		// // reload page message
		enable_ReloadMessage_handling(ee, server, EVENTS, configuration.messages)
		// // start playback message
		enable_StartPlayback_handling(ee, server, EVENTS, configuration.messages)

		// setup sampler
		// / preconditions
		// setup checkpoints (blocking conditions waiting for multiple asynchronous conditions)
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// condition.js
		// condition-event_based-binary_state.js
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// youtube_player_ready_to_be_initiated_condition
		// Check that the Configuration and the API are correctly loaded before declaring that the player can be istantiated (sampler preconditions)
		new EventBasedBinaryStateCondition(
			ee, 
			[
				{ 'trigger': EVENTS.ServerConnection__Opened },
		//		{ 'trigger': EVENTS.Experiment_Configuration__Loaded }, // #TODO #multiple_experiment_configurations multiple experiment configurations (load dynamically playback settings)
				{ 'trigger': EVENTS.YoutubeApi__Ready }
			],
			EVENTS.YoutubePlayer__ReadyToInit,
			{ },
			"sampler_preconditions"
		)

		// / initialization
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// youtube_player.js
		// youtube_sampler.js
		// youtube_sampler.error_flags.pause.js
		// youtube_sampler.error_flags.quality_changed.js
		// youtube_sampler.error_flags.rate_changed.js
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		ee.on(EVENTS.YoutubePlayer__ReadyToInit, function(evt_args) {
			console.log("YoutubePlayer__ReadyToInit")
			// init sampler
			sampler = new YoutubeSampler(YT, ee, configuration.experiment, EVENTS, configuration.errors)

			// status observers
			// / log jointime
			sampler.enable_jointime_sensing()
			// / log buffering events and duration
			sampler.enable_buffering_sensing()

			// setup error flags
			sampler.setup_pause_as_error(EVENTS)
			sampler.setup_quality_changed_as_error(EVENTS)
			sampler.setup_rate_changed_as_error(EVENTS)
		})

		// condition to notify that the experiment is ready to start
		ee.on(EVENTS.YoutubeSampler__Ready, function(evt_args) {
			sampler = evt_args.source

			// notify that all conditions to start the experiment are met
			ee.emit(EVENTS.Experiment__ReadyToStart, { 'source': null, 'time': evt_args.time }) // #TODO setup a condition and use it as source (condition to be met before triggering Experiment Ready to Start)
		})

		// condition to start the playback
		ee.on(EVENTS.Experiment__StartRequested, function(evt_args) {
			sampler.start_playback()
		})

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Console GUI
		// / notify to server when ready
		ee.on(EVENTS.Experiment__ReadyToStart, function(evt_args) {
			console.log("Experiment Ready to Start")
		})
		// / experiment completed notification
		ee.on(EVENTS.Experiment__Completed, function(evt_args) {
			console.log("Experiment Completed")
		})
		// / experiment error notification
		ee.on(EVENTS.Experiment__Error, function(evt_args) {
			console.log("Experiment Error: ")
			console.log(evt_args)
		})

		ee.on(EVENTS.YoutubeSampler__Ready, function(evt_args) {
			console.log("YoutubeSampler__Ready")
		})

		ee.on(EVENTS.ServerConnection__Opened, function(evt_args) {
			console.log("ServerConnection__Opened")
		})
		ee.on(EVENTS.YoutubeApi__Ready, function(evt_args) {
			console.log("YoutubeApi__Ready")
		})

		ee.on(EVENTS.Experiment__StartRequested, function(evt_args) {
			console.log("Experiment Start Requested")
		})
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



		// initialize youtube API
		initYoutubeApi(EVENTS, configuration.player_api.location)
	}
)


