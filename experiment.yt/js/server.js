// server.js
// requires: jQuery
/*
Models the Sampling Server and the actions that can be performed

@arg ee EventEmitter
@arg server_connection WebSocket
@arg MESSAGES { Experiment__ReadyToStart, Experiment__Error, Experiment__Completed }
*/
Server = function(ee, server_connection, MESSAGES) {
	var _this = this

	this.ee = ee
	this.server_connection = server_connection

	var getTemplateMessage = function(message_descriptor) {
		// Shallow copy
		return $.extend({}, message_descriptor)
	}

	this.notify_ready_to_start_experiment = function() {
		// setup message
		var msg = getTemplateMessage(MESSAGES.Experiment__ReadyToStart)

		// send message
		this.server_connection.send(msg)
	}

	this.send_error = function(error, result) {
		// setup message
		var msg = getTemplateMessage(MESSAGES.Experiment__Error)
		msg.result = result
		msg.error  = error

		// send message
		this.server_connection.send(msg)
	}

	this.send_result = function(result) {
		// setup message
		var msg = getTemplateMessage(MESSAGES.Experiment__Completed)
		msg.result = result
		msg.error  = false

		// send message
		this.server_connection.send(msg)
	}
}
