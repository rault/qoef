// server.interaction.js
var enable_server_interaction = function(server) {
	// / notify to server when ready
	ee.on(EVENTS.Experiment__ReadyToStart, function(evt_args) {
		server.notify_ready_to_start_experiment()
	})
	// / experiment completed notification
	ee.on(EVENTS.Experiment__Completed, function(evt_args) {
		server.send_result(evt_args.result)
	})
	// / experiment error notification
	ee.on(EVENTS.Experiment__Error, function(evt_args) {
		server.send_error(evt_args.error, evt_args.result)
	})
}
