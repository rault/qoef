// youtube_sampler.js
// / experiment end conditions
YoutubeSampler = function(YoutubeAPI, ee, configuration, EVENTS, ERRORS) {
	var _this = this

	// public properties
	this.YoutubeAPI 		= YoutubeAPI    // why? as a reminder to start the YoutubeSampler after the YoutubeAPI is ready (YoutubeAPI always defined under the global variable YT)
	this.configuration 		= configuration
	this.player 			= undefined
	this.experiment_result 	= undefined
	this.EVENTS 			= EVENTS
	this.ERRORS 			= ERRORS

	// public methods
	this.start_playback = function() {
		if (this.player === undefined) {
			throw Error("Cannot start playback, youtube player not ready (did you wait for YoutubeSampler__Ready event?)")
		}

		this.experiment_result = { }

		this.player.loadVideoById({
			'videoId': 			configuration.playback.video_id,
			'startSeconds': 	configuration.playback.time.begin,
			'endSeconds': 		configuration.playback.time.end,
			'suggestedQuality': configuration.playback.quality
		})

		ee.emit(EVENTS.Experiment__Started, { 'source': this, 'time': new Date() })
	}

	// setup youtube player
	// / define DOM element to use
	// / is it defined?
	if (configuration.player.target_dom_element === undefined) {
		// look for jquery selector to retreive the desired DOM element
		// take target DOM element (not jQuery one)
		configuration.player.target_dom_element = $(configuration.player.target_dom_selector)[0]
	}

	// / init Youtube Player
	initYoutubePlayer(YoutubeAPI, configuration.player, EVENTS, ERRORS, _this)


	// event bindings
	// / set ready when youtube player is ready
	ee.on(EVENTS.YoutubePlayer__Ready, function(evt_args) {
		// check if the player is the one initialized by this object
		if (evt_args.source_id !== _this)
			return

		_this.player = evt_args.source

		ee.emit(EVENTS.YoutubeSampler__Ready, { 'source': _this, 'time': new Date() })
	})

	// / experiment management
	// // playback completed successfully
	ee.on(EVENTS.YoutubePlayer__StateChanged, function(evt_args) {
		// check if the player is the one binded to this object
		if (evt_args.source !== _this.player)
			return

		// if ended:
		if (evt_args.state === YT.PlayerState.ENDED) {
			// notify experiment success
			ee.emit(EVENTS.Experiment__Completed, { 'source': _this, 'time': evt_args.time, 'result': _this.experiment_result, 'error': false })
		}
	})
	// // player error
	ee.on(EVENTS.YoutubePlayer__Error, function(evt_args) {
		// check if the player is the one binded to this object
		if (evt_args.source !== _this.player)
			return

		// error detected: notify experiment error
		ee.emit(EVENTS.Experiment__Error, { 'source': _this, 'time': evt_args.time, 'result': _this.experiment_result, 'error': evt_args.error })
	})

}
