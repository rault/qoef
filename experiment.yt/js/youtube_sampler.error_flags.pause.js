// youtube_sampler.error_flags.pause.js
// // detect when experiment is compromized
// /// pause
YoutubeSampler.prototype.setup_pause_as_error = function(EVENTS) {
	var _this = this

	ee.on(EVENTS.YoutubePlayer__StateChanged, function(evt_args) {
		// check if the player is the one binded to the target sampler object
		if (evt_args.source !== _this.player)
			return

		// if pause:
		// if (evt_args.state === YT.PlayerState.PAUSED) {
		// 	// notify error (video stopped manually, experiment compromized)
		// 	ee.emit(EVENTS.Experiment__Error, { 'source': _this, 'time': evt_args.time, 'result': _this.experiment_result, 'error': _this.ERRORS.PlaybackPaused})
		// }
	})
}
