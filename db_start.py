from app.setup_app_configuration    import get_configuration
from app.setup_db                   import start_db_daemon
configuration = get_configuration()
db_server = start_db_daemon(configuration['db']['port'], configuration['db']['data_location'])
