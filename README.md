    MM'"""""`MMM          MM""""""""`M MM""""""""`M 
    M  .mmm,  MM          MM  mmmmmmmM MM  mmmmmmmM 
    M  MMMMM  MM .d8888b. M`      MMMM M'      MMMM 
    M  MM  M  MM 88'  `88 MM  MMMMMMMM MM  MMMMMMMM 
    M  `MM    MM 88.  .88 MM  MMMMMMMM MM  MMMMMMMM 
    MM.    .. `M `88888P' MM        .M MM  MMMMMMMM 
    MMMMMMMMMMMM          MMMMMMMMMMMM MMMMMMMMMMMM

# QoE Factory

A network interface constraint-based testbed in Python 2.7 developed at [DIANA@INRIA](https://team.inria.fr/diana/).

Provided you make a program use a network interface in particular (see our two examples), you can test its behaviour under network constraints set by the testbed.
You can use it to test applications that cannot run through a particular interface by embedding them in a VM (see second example).

## Getting Started

Clone the repository. Check the prerequisistes below. Type `make` to get an idea of useful commands, or `main.py --help` to get an idea of useful parameters.

Create a virtualenv for the project and run `<virtualenv pip bin> install -r requirements.txt`.

Run `sudo <virtualenv python bin> main.py <interface to constraint>` to launch. The program will then be waiting for a client to connect. Beware, it only supports one client at a time.

A simple client example is provided along: *tests/ws_client.py*.

### Prerequisites

It runs only on a Linux system, since it relies on *tc* (traffic control). You
need a python 2.7 and a virtualenv to avoid package collisions (e.g: *enum* and *enum34*).

You need to set the *conf/static.json* according to your system, and change the path
to your virtualenv binary folder in *net/netutils.py*.

## Built With

* [tea](https://giphy.com/gifs/9K0L7RMlssvUQ/html5) - The new coffee
* [pip](https://pip.pypa.io/en/stable/) - Dependency Management
* [SALib](https://github.com/SALib/SALib) - Used to generate points in the experimentation space
* [Vowpal Wabbit](https://github.com/JohnLangford/vowpal_wabbit/wiki) - Used for active learning (default: disabled)
* [MongoDB](https://www.mongodb.com/) - Used to store the experiment points (default: running)

## Live visual representation of your results

You probably know how to represent your results to your audience better than us, but know that you can quickly run various visualization examples via `make vue`:

![Visualization of the sampled space and labels](experiment.yt.visualization.example.png)

## Contributing

You can contact the authors directly, although no support is guaranted, we like helping other research teams.

## Authors

* **Thierry Spetebroot** - *Initial work* - [DIANA@INRIA](https://team.inria.fr/diana/team-members/thierry-spetebroot/)
* **Olha Prodan** - *Tc refactoring* - [Unice student](mailto://olincess1@gmail.com)
* **Pierre-Antoine Rault** - *Refactoring* - [Unice student](http://rigelk.eu)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* The whole DIANA team for supervision and suggestions

