# subprocessutils

from subprocess import Popen, PIPE

import parse # to support output parsing

'''
subprocess execution function
'''
def exec_command(cmd_string, parser_string = None, is_single_result = False, verbose_mode = False):
    result_list = []
    
    # run process (blocking)
    process = Popen(cmd_string, shell=True, stdout=PIPE, stderr=PIPE)
    process_stdout, process_stderr = process.communicate()
    process_exit_code = process.wait()
    
    # verbose / debug only
    if verbose_mode:
        print("CMD: {}".format(cmd_string))
        print(process_stdout)
        print(process_stderr)

    if(parser_string):
        # compile parser
        parser = parse.compile(parser_string)

        # read and parse output (line by line)
        for line in process_stdout.split('\n'):
            parsed = parser.parse(line)
            if(parsed):
                result_list.append(parsed.named)
    else:
        # return output splitted by line
        result_list = process_stdout.split('\n')

    if(is_single_result):
        return result_list[0]

    return result_list    