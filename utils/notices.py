from __future__ import print_function
import click

def logMain(path):
    click.echo('  to see logs, just do: tail -f %s' % click.format_filename(path))

def logError(path, name):
    click.echo('  --> experiment '+click.style(name, fg='red')+' failed. log available at %s' % click.format_filename(path))

def serviceStarted(*args):
    if len(args) == 2 and isinstance(args[1], int):
        click.echo(click.style('* ',fg='green',bold=True)+args[0]+', listening on port: '+str(args[1]))
    elif len(args) >= 1:
        click.echo(click.style('* ',fg='green',bold=True)+args[0])
    else:
        pass

def clientConnected(name, source):
    click.echo('  client has connected on '+name+', from '+str(source))

def serviceNotStarted(text):
    click.echo(click.style('* ',fg='red',bold=True)+text)

def serviceDisabled(text):
    click.echo(click.style('* ',fg='yellow',bold=True)+text)

if __name__ == "__main__":
    serviceNotStarted("sampler started")
    serviceStarted("sampler started")
    logMain("../log.txt")
    logError("../log.txt", "1")
    logError("../log.txt", "2")
