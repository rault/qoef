import sys

'''
Dumps an object (JSON format)
'''
def dump(obj, nested_level=0, output=sys.stdout):
    spacing = '   '
    if type(obj) == dict:
        print >> output, '%s{' % ((nested_level) * spacing)
        for k, v in obj.items():
            if hasattr(v, '__iter__'):
                print >> output, '%s%s:' % ((nested_level + 1) * spacing, k)
                dump(v, nested_level + 1, output)
            else:
                print >> output, '%s%s: %s' % ((nested_level + 1) * spacing, k, v)
        print >> output, '%s}' % (nested_level * spacing)
    elif type(obj) == list:
        print >> output, '%s[' % ((nested_level) * spacing)
        for v in obj:
            if hasattr(v, '__iter__'):
                dump(v, nested_level + 1, output)
            else:
                print >> output, '%s%s' % ((nested_level + 1) * spacing, v)
        print >> output, '%s]' % ((nested_level) * spacing)
    else:
        print >> output, '%s%s' % (nested_level * spacing, obj)

'''
Dumps an object to string (JSON format)
'''
def dump_to_string(obj):
    lines = _dump_to_string(obj)
    return "\n".join(lines)

def _dump_to_string(obj, nested_level=0, build_strs = []):
    spacing = '   '
    if type(obj) == dict:
        build_strs.append('%s{' % ((nested_level) * spacing))
        for k, v in obj.items():
            if hasattr(v, '__iter__'):
                build_strs.append('%s%s:' % ((nested_level + 1) * spacing, k))
                _dump_to_string(v, nested_level + 1, build_strs)
            else:
                build_strs.append('%s%s: %s' % ((nested_level + 1) * spacing, k, v))
        build_strs.append('%s}' % (nested_level * spacing))
    elif type(obj) == list:
        build_strs.append('%s[' % ((nested_level) * spacing))
        for v in obj:
            if hasattr(v, '__iter__'):
                _dump_to_string(v, nested_level + 1, build_strs)
            else:
                build_strs.append('%s%s' % ((nested_level + 1) * spacing, v))
        build_strs.append('%s]' % ((nested_level) * spacing))
    else:
        build_strs.append('%s%s' % (nested_level * spacing, obj))

    return build_strs
