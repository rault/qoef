class EventsObserver(object):
	"""
	If all the events in trigger_events are triggered (in any order) emits the emitted_event

	@arg trigger_events List<Event> events that needs to be triggered in order to emit emitted_event
	@arg emitted_event Event event that will be triggered when all the trigger_events has been triggered
	@arg ee EventEmitter event emitter that handles events
	"""
	def __init__(self, trigger_events, emitted_event, ee):
		super(EventsObserver, self).__init__()

		self.trigger_events = trigger_events
		self.emitted_event  = emitted_event
		self.ee 			= ee
		self.state 			= { }
		self._handlers 		= { }

		# setup
		for evt in trigger_events:
			## state variables
			self.state[evt] = False

			## handlers
			def handler():
				self.state[evt] = True
				
				if(self.check()):
					self.ee.emit(self.emitted_event, { 'source': self, 'timestamp': time.time() })
			self._handlers[evt] = handler
			self.ee.on(evt, handler)


	"""
	Check if all the events required to emit the emitted_event has been triggered

	@returns true if all the events required to emit the emitted_event has been triggered
	"""
	def check(self):
		for is_event_triggered in self.state.itervalues():
			if not is_event_triggered:
				return False
		return True

	"""
	Reset all state variables
	"""
	def reset(self):
		for evt in trigger_events:
			self.state[evt] = False

	"""
	Remove all event handlers
	"""
	def release(self):
		for evt, handler in self._handlers.iteritems():
			self.ee.off(evt, handler)


from uuid import uuid4

class AllEventWaiter(object):
	"""
	Define a checkpoint that blocks a thread execution (execute method caller) until all required_events (preconditions) has been raised
	"""
	def __init__(self, required_events, ee):
		super(AllEventWaiter, self).__init__()
		
		self.required_events 	= required_events
		self.ee 				= ee

		self.id 			= uuid4()
		self.emitted_event 	= "AllEventWaiter-{}-resume".format(self.id)
		self._observer 		= EventsObserver(self.required_events, self.emitted_event, ee)
		self.condition 		= Condition()

		def handler():
			# unlock executer
			self.condition.acquire()
			self.condition.notify()
			self.condition.release()

		self.ee.on(self.emitted_event, handler)

	"""
	blocks the thread until all required_events (preconditions) has been raised
	"""
	def execute(self):
		self.condition.acquire()

		# wait until all the required events has been raised
		while not self._observer.check():
			self.condition.wait()

		self.condition.release()

	"""
	Removes all event handlers
	"""
	def release(self):
		self._observer.release()
