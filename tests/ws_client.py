import copy
import json
import logging
import time # for reconnect
import socket # to handle the connection retries


from ws4py.client.threadedclient import WebSocketClient

class Client(WebSocketClient):
    def __init__(self, url, server, protocols=None, extensions=None, heartbeat_freq=None,
                 ssl_options=None, headers=None):
        WebSocketClient.__init__(self, url, protocols, extensions, heartbeat_freq,
                                     ssl_options, headers=headers)
        self.server = server
        self.server.set_connection(self)
        
    def setup(self, timeout=0):
        """
        function used to easily reconnect. wraps connect() and run_forever() so
        don't use these functions
        """
        try:
            self.connect()
            self.run_forever()
        except KeyboardInterrupt:
            self.close()
        except:
            newTimeout = timeout + (0 if (timeout>=15) else 3)
            logging.info("[__"+self.__class__.__name__+"__]: timing out for %i seconds", newTimeout)
            time.sleep(newTimeout)
            logging.info("[__"+self.__class__.__name__+"__]: attempting reconnect...")
            self.setup(newTimeout)

    def opened(self):
        logging.info("[__"+self.__class__.__name__+"__]: ws client connected")
        self.server.notify_ready_to_start_experiment()
        
    def closed(self, code, reason=None):
        print(("Closed down", code, reason))

    def received_message(self, m):
    	code = json.loads(m.data)["code"]
        if code == 40: # { "code": 40, "type": "action" , "text": "playback start request" }
        	logging.debug("start request received -> %s",m.data.decode("utf-8"))
        	logging.info("[__"+self.__class__.__name__+"__]: starting an experiment")
        	# launch the Skype experiment
        	time.sleep(5)
        	self.server.send_result(3)
        elif code == 20: # { "code": 20, "type": "action" , "text": "reload request" }
        	logging.debug("reload request received -> %s",m.data.decode("utf-8"))
        	logging.info("[__"+self.__class__.__name__+"__]: reloading/interrupting for a new experiment")
        	self.server.notify_ready_to_start_experiment()
        else:
        	logging.debug("normal message received -> %s",m.data.decode("utf-8"))


class Server(object):
    def __init__(self, MESSAGES):
        """
		Models the Sampling Server and the actions that can be performed

		It just implements 3 functions corresponding to the three messages the Sampling Server expects to be implemented.

		:param MESSAGES:            { Experiment__ReadyToStart, Experiment__Error, Experiment__Completed }
		"""
        self.MESSAGES = MESSAGES

    def send(f): # decorator to send messages
        def _send(self, *args, **kwargs): # decorator wrapper to access self
            self.server_connection.send(json.dumps(f(self, *args, **kwargs))) # sends return value of the decorated function
        return _send
        
    def set_connection(self, ws):
        self.server_connection = ws

    @send
    def notify_ready_to_start_experiment(self):
    	logging.info("[__"+self.__class__.__name__+"__]: notifying orchestrator of ready state")
        return copy.copy(self.MESSAGES['Experiment__ReadyToStart'])

    @send
    def send_error(self, result, error):
        msg = copy.copy(self.MESSAGES['Experiment__Error'])
        msg['result'] = result
        msg['error'] = error
        return msg

    @send
    def send_result(self, result):
        msg = copy.copy(self.MESSAGES['Experiment__Completed'])
        msg['result'] = result
        msg['error'] = False
        return msg

if __name__ == '__main__':
    try:
    	ws		   = None # the connection Client that actually connects to the websocket server
        ws_server  = None # the Sampling Server interface model over ws

        #1 setting up the logging (otherwise it won't show)
        logging.getLogger().setLevel(logging.DEBUG)
        
        #2 server setup
        ws_server = Server(
            {
            "Experiment__ReadyToStart": { "code":  1, "type": "experiment" , "text": "experiment ready to start" },
            "Experiment__Completed":    { "code":  2, "type": "experiment" , "text": "experiment completed" },
            "Experiment__Error":        { "code":  3, "type": "experiment" , "text": "experiment failed" }
            }
        )

        # termination routine to close the program (stop all active entities)
        def terminate(signum = None, frame = None):
            try:
                ws.close()
            except:
                import traceback
                traceback.print_exc()

        #3 setup ws client
        logging.info("[__MAIN__]: ws client setup")
        ws = Client('ws://localhost:8092',ws_server)
    	ws.setup()
    except KeyboardInterrupt:
        pass
    except:
        logging.exception("[__MAIN__]: exiting due to exception")
    finally:
    	terminate()
    	logging.info("[__MAIN__]: cleaned. now exiting program")
