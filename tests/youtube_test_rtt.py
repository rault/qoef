'''
test system that perform RTT active probing to the youtube servers
(dynamic resolution of a video id and test on the selected CDN server)
'''
from __future__ import absolute_import

from ..net.netest import solve_url, RttTester
from urlparse import urlparse

from datetime import datetime
from time import time

import pymongo
import logging

################################################################################
# Configuration
## Video
VIDEO_ID      = "Ue4PCI0NamI"

## output
OUT_PATH = "./rtt"
OUT_ROOT = "rtt" # + session_id

## db
DB_HOST = "localhost"
DB_PORT = 8094
DB_NAME = "metatests"
DB_COLLECTIONS = {
    "rtt_tests": 	"rtt"
}

## experiments
EXPERIMENTS = 50
################################################################################
# service definitions
def start_db_client(host, port, name, collection_names):
    # setup connection
    try:
        logging.info("[__MAIN__]: DB client connecting")
        conn = pymongo.MongoClient(host, port)
        logging.info("[__MAIN__]: DB client connected")
        logging.info("[DB]: using database \"{}\"\n".format(name))
    except pymongo.errors.ConnectionFailure, e:
       logging.info("[__MAIN__]: DB client cannot connect: %s" % e)
       raise RuntimeException("DB Connection Failure")

    # setup db
    db = conn[name]

    # setup collections
    collections = {
        'rtt_tests':   db[collection_names['rtt_tests']]
    }

    return (conn, db, collections)
################################################################################

# RTT tester
(conn, db, collections) = start_db_client(DB_HOST, DB_PORT, DB_NAME, DB_COLLECTIONS)

session_id = "{0:.0f}".format(time())
OUT_ROOT += session_id

if __name__ == '__main__':
    print "[__MAIN__]: session {} started".format(session_id)

    url = "http://www.youtube.com/watch?v={}".format(VIDEO_ID)
    tester = RttTester()
    for i in xrange(0, EXPERIMENTS):
        print "[__MAIN__]: iteration {} started".format(str(i).zfill(5))
        print "[__MAIN__]: solving content server address"
        server_url_full = solve_url(url)
        server_url = urlparse(server_url_full).netloc
        print "[__MAIN__]: address solved as:\n{}\nfull address is\n{}\n".format(server_url, server_url_full)

        print "[__MAIN__]: performing rtt test on content server address"
        out_root = OUT_ROOT + '-' + str(i).zfill(5)
        result = tester.perform_rtt_test(OUT_PATH, out_root, server_url)

        rtt = result['rtt']
        print "[__MAIN__]: rtt test concluded, result is\n{}\n".format(rtt)

        entry = {
            '_id': session_id + '-' + str(i).zfill(5),
            'session_id': session_id,
            'iteration_id': str(i).zfill(5),
            'rtt': rtt,
            'time': datetime.now(),
            'video_id': VIDEO_ID,
            'request_url': url,
            'server_url': server_url,
            'server_url_full': server_url_full,
            'result': result
        }
        collections['rtt_tests'].insert_one(entry)

        print "[__MAIN__]: iteration {} terminated successfully".format(str(i).zfill(5))
################################################################################
