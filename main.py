#!/usr/bin/env python
# -*- coding: utf-8 -*-
# name: QoE factory (QoEF)
# origin: DIANA research group, INRIA, circa 2015
# description: a network constraint testbed.
# python_version: 2.7

VERSION = 0.1

##
## Argument parsing block
##
import click
def banner():
    click.echo("""
    MM'\"\"\"\"\"`MMM          MM\"\"\"\"\"\"\"\"`M MM\"\"\"\"\"\"\"\"`M 
    M  .mmm,  MM          MM  mmmmmmmM MM  mmmmmmmM
    M  MMMMM  MM .d8888b. M`      MMMM M'      MMMM
    M  MM  M  MM 88'  `88 MM  MMMMMMMM MM  MMMMMMMM
    M  `MM    MM 88.  .88 MM  MMMMMMMM MM  MMMMMMMM
    MM.    .. `M `88888P' MM        .M MM  MMMMMMMM
    MMMMMMMMMMMM          MMMMMMMMMMMM MMMMMM@INRIA v"""+str(VERSION)+"""
""")

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])

@click.command(context_settings=CONTEXT_SETTINGS)
@click.argument('interface', type=click.STRING)
@click.option('--venv', default=None, help='sets venv path (for the tcconfig binaries) in net.netutils', type=click.Path(exists=True, file_okay=False, dir_okay=True, writable=False, readable=True, resolve_path=True, allow_dash=False))
@click.option('--config', default='./config/static.json', help='path to alternative config file', type=click.File('rb'))
@click.option('--direction', type=click.Choice(['download', 'upload', 'both']), default='both', help='constrain only one (default=both) ways')
@click.version_option(version=VERSION, message='%(prog)s, version %(version)s, baked with love at INRIA')
def qoef(interface, venv, config, direction):
    banner()
    qoef_main(interface, venv, config, direction)

##
## Imports and function wrapper definitions
##

#NB: event loop ensures thread safety (being a serialized event emitter)
from app.setup_app_logging          import setup_logging
setup_logging()

def qoef_main(interface, venv, config, direction):
    from app.setup_app_configuration    import get_configuration
    # load configuration from the default conf/static.json
    configuration = get_configuration(config)
    configuration['venvpath'] = venv
    #configuration['unaffected'] = args.unaffected
    configuration['direction'] = direction
    #if args.ov_mongodb_dir is not None:
    #    configuration['db']['data_location'] = args.ov_mongodb_dir
    #if args.ov_mongodb_port is not None:
    #    configuration['db']['port'] = args.ov_mongodb_port

    from app.setup_http                 import start_http_server
    from app.setup_ws                   import start_websocket_server
    from app.setup_db                   import start_db_daemon, start_db_client
    from app.setup_active_learner       import start_vowpal_wabbit_daemon, start_active_learner, active_learner_with_seeding, active_learner_with_inference, _push_basic_knowledge
    from app.setup_target_application   import start_client
    from app.setup_event_loop           import start_event_loop
    from app.setup_app                  import start_sampler
    from app.setup_app_persistence      import setup_persistence
    from sampling.generators.fast       import get_fast_generator
    from sampler                        import Sampler
    from event_dispatching              import EventEmitter
    from net.netutils                   import Tc
    from utils                          import notices
    import click
    import logging


    def _setup_http_server(configuration, active=False):
        if active:
            http_server = start_http_server(configuration['host'], configuration['port'])
            notices.serviceStarted("http server started",configuration['port'])
            return http_server
        else:
            notices.serviceDisabled("http server disabled")

    def _setup_websocket_server(ee, configuration, active=False):
        if active:
            ws_server = start_websocket_server(configuration['host'], configuration['port'], ee)
            notices.serviceStarted("websocket server started",configuration['port'])
            return ws_server
        else:
            notices.serviceDisabled("websocket server disabled")
            
    def _setup_db_daemon(configuration):
        db_daemon = start_db_daemon(configuration['port'], configuration['data_location'])
        notices.serviceStarted("db daemon started",configuration['port'])
        return db_daemon

    def _setup_db_client(configuration, active=False, notice=True):
        db_client = start_db_client(configuration['host'], configuration['port'], configuration['name'], configuration['collections'])
        if notice:
            notices.serviceStarted("db client started")
        return db_client

    def _setup_vw_daemon(configuration, active=False):
        if active:
            vw_daemon = start_vowpal_wabbit_daemon(configuration['port'], configuration['mellowness'])
            notices.serviceStarted("vw daemon started",configuration['port'])
            return vw_daemon
        else:
            notices.serviceDisabled("vw daemon disabled")

    def _setup_active_learner(ee, collections, configuration):
        point_generator = get_fast_generator(configuration['space'], continuous = True)
        active_learner = start_active_learner(ee, configuration['host'], configuration['port'], point_generator)
       # active_learner = active_learner_with_seeding(active_learner, collections)
       # active_learner = active_learner_with_inference(active_learner, collections, ee, thresholds=configuration['thresholds'])
        notices.serviceStarted("active learner started [DUMMY]")

        active_learner.submit_point({
          'download_lossrate': configuration['space']['download_lossrate']['begin'],
          'download_throughput': configuration['space']['download_throughput']['end'],
          'rtt': configuration['space']['rtt']['begin'],
          'label': 0
        })
        active_learner.submit_point({
           'download_lossrate': configuration['space']['download_lossrate']['end'],
           'download_throughput': configuration['space']['download_throughput']['begin'],
           'rtt': configuration['space']['rtt']['end'],
           'label': 1
        })

        return active_learner

    #def _setup_target_application(ee, server, configuration, active=False):
    #    target = start_target_application(ee, server.host, server.port, 
    #                                    configuration['user'], 
    #                                    configuration['browser_cmd'], 
    #                                    configuration['messages'])
    #    notices.serviceStarted("target application(s) started")
    #    return target
    
    db_server   = None
    vw_server   = None
    http_server = None
    ws_server   = None
    event_loop  = None

	## Termination routine block
	##
    def _terminate(target, name):
        logging.info("[__MAIN__]: closing {}".format(name))
        target.stop()
        #target.join()
        logging.info("[__MAIN__]: {} closed".format(name))

    # termination routine to close the program (stop all active entities)
    def terminate(signum = None, frame = None):
        db_server, ws_server, event_loop

        targets = [
            (event_loop,  "event_loop"          ),
            (db_server,   "db_daemon"           ),
            #(vw_server,   "vowpal wabbit daemon"),
            #(http_server, "http server"         ),
            (ws_server,   "websocket server"    )
            #(client, "target application")
        ]

        for target in targets:
            try:
                _terminate(target[0], target[1])
            except:
                import traceback
                traceback.print_exc()

    ## Init core environment block (logging, tc)
    ## 
    # setup logging
    notices.logMain("log/log.txt")
    # # setup event based framework
    ee = EventEmitter()
    event_loop = start_event_loop(ee)

    # reset traffic control
    tc = Tc(venv=venv,
            interface=interface)
    tc.reset_network_condition()
    #TODO dummynet reference should be passed to the executor and tester 
    #instance (now everything works because it's a singleton but consider 
    #it as a quickfix)

    try:
        ## Init app services block
        ##
        http_server = _setup_http_server(configuration['http'], active=False)
        ws_server   = _setup_websocket_server(event_loop, configuration['websocket'], active=True)
        db_server   = _setup_db_daemon(configuration['db'])

        # start application
        (conn, db, collections) = _setup_db_client(configuration['db'])
        setup_persistence(ee, collections)

        vw_server       = _setup_vw_daemon(configuration['active_learner'], active=False)
        global active_learner
        active_learner  = _setup_active_learner(ee, collections, configuration['active_learner'])

        global count
        count = 0
        def inject(*args, **kwargs):
            global count, active_learner
            if count == 10:
                _push_basic_knowledge(active_learner)
                count = 0
            count += 1

        ee.on(Sampler.Events.SAMPLE_COLLECTED, inject)

        client          = start_client(event_loop, configuration['messages']) #_setup_target_application(event_loop, http_server, configuration['client'])
        sampler         = start_sampler(event_loop, active_learner, client, configuration['sampler'], tc)
        
        notices.serviceStarted("sampler started\n")
        
        def qoef_count(*args):
            experiment_count = sampler.get_experiment_count()
            if not sampler.get_experiment_bound():
                click.echo("now running experiment {}".format(experiment_count))
            elif sampler.is_bound_soft(): # in case the bounding is soft, we'll continue working after bound is reached
                click.echo("now running experiment {:4} / {:>4}+".format(experiment_count,sampler.get_experiment_bound()))
            else:
                click.echo("now running experiment {:4} / {:>4}".format(experiment_count,sampler.get_experiment_bound()))
            #import click_spinner
            #import waiting
            #with click_spinner.spinner():
            #    waiting.wait(lambda: sampler.get_experiment_count() > experiment_count)
        
        from app.client                     import Client
        from experimenter                   import Experimenter
        event_loop.on(Client.Events.READY, qoef_count)#thread.start_new_thread(qoef_count, ()))
        event_loop.on(Client.Events.PLAYBACK_FAILED, lambda context,result,error: notices.logError("[not logged]",str(sampler.get_experiment_count())))
    except:
        import traceback
        traceback.print_exc()
        terminate()

    logging.info("[__MAIN__]: setup signal handling")
    from signal import signal, SIGINT, pause
    signal(SIGINT, terminate)
    logging.info("[__MAIN__]: press ^C to interrupt")
    pause()
                

if __name__ == "__main__":
    qoef()
