################################################################################
# Event Dispatching

from datetime import datetime
import logging

class EventEmitter(object):
    """supports * wildcard"""
    def __init__(self):
        super(EventEmitter, self).__init__()

        self._register = { }

    def on(self, event, callback):
        assert event is not None
        assert callback is not None

        if event not in self._register:
            self._register[event] = []

        self._register[event].append(callback)

    def off(self, event, callback):
        # if event not in self._register:
        #     return

        try:
            self._register[event].remove(callback)
        except:
            logging.error("[EVENT EMITTER]: impossible to remove an unregistered callback")
            import traceback
            traceback.print_exc()

    @staticmethod
    def get_default_context():
        return {
            'time':  datetime.now()
        }

    def _execute_callbacks(self, callbacks, context, event, args, kwargs):
        for callback in callbacks:
            callback(context, *args, **kwargs)

    def emit(self, event, *args, **kwargs):
        # define event context if not defined in the arguments
        #TODO improve how the context is managed (extend, check if time is in...)
        if '__context__' in kwargs:
            context = kwargs['__context__']
            del kwargs['__context__']
        else:
            context = EventEmitter.get_default_context()

        # provide additional context informations
        context['dispatcher'] = self  # provide reference to the dispatcher
        context['event'] =      event # provide reference to event

        # handle * wildcard
        if '*' in self._register:
            self._execute_callbacks(self._register['*'], context, event, args, kwargs)

        if event in self._register:
            self._execute_callbacks(self._register[event], context, event, args, kwargs)
################################################################################

################################################################################
# Remote EventEmitter
#TODO define an event emitter based with socket interaction
class RemoteEventEmitter(EventEmitter):
    def __init__(self):
        super(HttpEventEmitter, self).__init__()


################################################################################
