import os

# network tools
import dpkt, pcap # traffic capture tools

# logging tools
import logging

# threading and external process execution
from threading  import Timer, Thread, Semaphore

# url parser (for url to ip resolution)
from urlparse import urlparse

# utilities for process handling (communication)
from subprocessutils import exec_command

# exception tools
import traceback
import subprocess

class PcapSniffer(object):
    """PcapSniffer"""
    def __init__(self, pcap = pcap.pcap()):
        super(PcapSniffer, self).__init__()
        
        self.pcap = pcap

        self.sniffing   = False
        self._semaphore = Semaphore(1)

    # keep on sniffing until self.sniffing is not False
    def dump(self, output_filename, output_mode = 'w'):
    	return
        self.sniffing = True

        # begin critical section
        self._semaphore.acquire()

        # TODO support also fp instead of file name?
        # if isinstance(output_filename, basestring):
        #     output_filename = open(output_filename, output_mode)

        with open(output_filename, output_mode) as out_fp:
            # with clause is not supported by dpkt.pcap.Writer
            # with dpkt.pcap.Writer(out_fp) as out_pcap:

            out_pcap = dpkt.pcap.Writer(out_fp)

            while self.sniffing:
                res = self.pcap.next()
                if res is not None:
                    ts, pkt = res
                    out_pcap.writepkt(pkt)
           
            out_pcap.close()

        # end critical section
        self._semaphore.release()

    def close(self):    
        self.sniffing = False

        # wait until the sniffing operation is completed
        self._semaphore.acquire()

        # sniffing completed
        self._semaphore.release()

'''
Wrapper for PcapSniffer in order to use it with the python construct 'with' (define sniffing sections)
'''
#TODO support deamon argument for thread
class PcapSniffingEnvironment(object):
    def __init__(self, output_filename, output_mode = 'w', pcap = pcap.pcap(timeout_ms = 500), thread_manager = None, name = 'pcap_sniffer'):
        self.output_filename = output_filename
        self.output_mode     = output_mode

        self.thread_manager  = thread_manager
        self.name            = name

        self._sniffer        = PcapSniffer(pcap)

    def __enter__(self):
        self.start()
        return self

    def start(self):
        #TODO put in __init__?

        if self.thread_manager is not None:
            self._sniffer_th = self.thread_manager.register_thread(target = lambda: self._sniffer.dump(self.output_filename, self.output_mode), close_fn = self.stop, name = self.name)
        else:
            self._sniffer_th = Thread(target = lambda: self._sniffer.dump(self.output_filename, self.output_mode), name = self.name)
        
        self._sniffer_th.start()

    def __exit__(self, exc_type, exc_value, exc_traceback):
        if exc_type is not None:
            #print exc_type, exc_value, traceback # display exception (if any)
            print traceback.format_exc()

        self.stop()
        
        return True # suppress exceptions (if any)

    def stop(self):
        self._sniffer.close()


# tcconfig wrapper (of the wrapper...)
class Tc(object):
    NOT_CONSTRAINED = {
        'download': {
            'lossrate':  0, # from 0 to 1 (100%)
            'bandwidth': 0, # in Kbps
            'delay':     0,  # in ms
            'jitter':    0
        },
        'upload': {
            'lossrate':  0, # from 0 to 1 (100%)
            'bandwidth': 0, # in Kbps
            'delay':     0,  # in ms
            'jitter':    0
        }
    }
    
    # default values that are gonna be used for their self counterparts if unset in init
    VENV                = ""
    INTERFACE           = "eth0"
    UNCONSTRAINED_PORT  = 8092
    
    def __init__(self, venv="", interface=INTERFACE, u_port=UNCONSTRAINED_PORT, direction="both"):
        super(Tc, self).__init__()
        if venv:
            self.VENV = venv+"/bin/"
        else:
            self.VENV = Tc.VENV
        self.INTERFACE = interface
        self.UNCONSTRAINED_PORT = u_port
        self.DIRECTION = direction

    """TCconfig"""
    def _configUnconstrainedPrevious(self,cmd_list):
        lines = subprocess.check_output( cmd_list + ['--tc-command'] )
        lines_list  = lines.splitlines()
        
        # executing lines which won't be modified anyway
        [subprocess.call( ['sudo'] + line.split(' ') ) for line in lines_list[:-1]]
        
        # catching the last line
        last_line   = lines_list[-1]
        
        # in every case (unconstrained port or not), we can still change prio from 1 to 2
        last_line_modified = last_line.split(' ')
        last_line_modified[-7] = '2'
        
        # we can now execute the line, since the unconstrained port will have prio 1, it can wait
        subprocess.call( ['sudo'] + last_line_modified )
        
        if self.UNCONSTRAINED_PORT != -1:
            # calling the filter for the unconstrained port
            new_cmd_list = last_line.split(' ')  
            new_cmd_list[-4] = 'dport' if (new_cmd_list[-4] == 'dst') else 'sport' # switch if upload/download
            new_cmd_list[-7] = '1'
            new_cmd_list[-3:-2] = (str(self.UNCONSTRAINED_PORT),'0xffff') # slice assignment
            lines_list += new_cmd_list
        
        # we can now execute the added line (to filter ports using a priority lane of prio 1)
        subprocess.call( ['sudo'] + new_cmd_list )
    
    def _configDownload(self,bandwidth,delay,lossrate,jitter):
        cmd_list = ["sudo",self.VENV+"tcset","--device",self.INTERFACE,"--delay","%s"%(delay),"--loss","%s"%(lossrate),"--rate","%sk"%(bandwidth),"--direction","incoming","--delay-distro","%s"%(jitter)]
        subprocess.call(cmd_list)
        #if self.UNCONSTRAINED_PORT != -1:
        #self._configUnconstrainedPrevious(cmd_list)

    def _configUpload(self,bandwidth,delay,lossrate,jitter):
        cmd_list = ["sudo",self.VENV+"tcset","--device",self.INTERFACE,"--delay","%s"%(delay),"--loss","%s"%(lossrate),"--rate","%sk"%(bandwidth),"--direction","outgoing","--delay-distro","%s"%(jitter)]
        subprocess.call(cmd_list)
        #if self.UNCONSTRAINED_PORT != -1:
        #self._configUnconstrainedPrevious(cmd_list)

    def _configBoth(self,bandwidth,delay,lossrate,jitter):
        self._configDownload(bandwidth,delay,lossrate,jitter)
        self._configUpload(bandwidth,delay,lossrate,jitter)
        #cmd_list = ["sudo",self.VENV+"tcset","--device",self.INTERFACE,"--delay","%s"%(delay),"--loss","%s"%(lossrate),"--rate","%sk"%(bandwidth),"--delay-distro","%s"%(jitter)]
        #subprocess.call(cmd_list)
        
    def set_network_condition(self, conf = NOT_CONSTRAINED):
        if conf != Tc.NOT_CONSTRAINED:
            logging.debug("[__"+self.__class__.__name__+"__]: setting network conditions")
            isResetting = False
        else:
            logging.debug("[__"+self.__class__.__name__+"__]: resetting network conditions")
            isResetting = True
            
        def _is_executable(fpath):
            return os.path.isfile(fpath) and os.access(fpath, os.X_OK)
        
        if not _is_executable(self.VENV+"tcdel"):
            logging.warn("[__"+self.__class__.__name__+"__]: ERROR setting network conditions: "+self.VENV+"tcset command not found (command was not executed)")
            return
        elif isResetting:
            self.reset_network_condition()
            return
        
        if self.DIRECTION == 'download':
            self._configDownload(conf["download"]["bandwidth"], conf["download"]["delay"], conf["download"]["lossrate"],conf["download"]["jitter"])
        if self.DIRECTION == 'upload':
            self._configUpload(conf["upload"]["bandwidth"], conf["upload"]["delay"], conf["upload"]["lossrate"], conf["upload"]["jitter"])
        if self.DIRECTION == 'both':
            self._configBoth(conf["upload"]["bandwidth"], conf["upload"]["delay"], conf["upload"]["lossrate"], conf["upload"]["jitter"])
       
    def reset_network_condition(self):
        logging.debug("[__"+self.__class__.__name__+"__]: network conditions reset")
        logging.info("[__"+self.__class__.__name__+"__]: tcdel: "+self.VENV+"tcdel")
        subprocess.call(["sudo", self.VENV+"tcdel", "--device", self.INTERFACE], stdout=open(os.devnull, 'w'), stderr=open(os.devnull, 'w'))


class TcEnvironment(object):
    def __init__(self, conf):
        '''
        Context manager for Tc config in order to use it with the python construct 'with' 
        (define sections where the network is controlled via Tcconfig)
        '''
        self.conf    = conf
        
    def __enter__(self):
        self.start()
        return self

    def start(self):
        self._tc = Tc()
        self._tc.set_network_condition(self.conf)

    def __exit__(self, exc_type, exc_value, exc_traceback):
        if exc_type is not None:
            #print exc_type, exc_value, traceback # display exception (if any)
            print traceback.format_exc()

        self.stop()

        return True # suppress exceptions (if any)

    def stop(self):
        self._tc.reset_network_condition(self.pipe_id)

'''
Resolve an url to it's ip address
'''
def resolve_url_to_ip(target_url):
    url = urlparse(target_url)
    target_url_netloc = url.netloc if url.netloc is not '' else url.path # take first part of url (the one that identifies an host and not a resource or some request parameters)

    # host supports only url with the network location part (see step before to ensure this)
    _cmd = "host \"{0}\" | grep -Eo \"[0-9]{{1,3}}\\.[0-9]{{1,3}}\\.[0-9]{{1,3}}\\.[0-9]{{1,3}}+\"".format(target_url_netloc)
    return exec_command(_cmd, None, True) # , verbose_mode = True

