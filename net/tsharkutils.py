# TSHARK utils

import time

from subprocessutils import exec_command


def _init_if_not_in(ip, conversations_index):
    if ip not in conversations_index:
        # ip2 not already in conversation index
        # add it
        conversations_index[ip] = {
            # target of single conversation index
            "with": { },
            # initialize total exchanged data statistics
            "total": {
                "received": {
                    "frames": 0,
                    "bytes":  0
                },
                "sent": {
                    "frames": 0,
                    "bytes":  0
                },
                "all": {
                    "frames": 0,
                    "bytes":  0
                }
            }
        }

def _get_ip_string(raw_conversation, ip_prefix):
    ip = "{}.{}.{}.{}".format(
        raw_conversation[ip_prefix + '1'],
        raw_conversation[ip_prefix + '2'],
        raw_conversation[ip_prefix + '3'],
        raw_conversation[ip_prefix + '4']
    )

def _to_conversations_index(raw_conversation_list):
    conversations_index = { }

    for raw_conversation in raw_conversation_list:
        ip1 = _get_ip_string(raw_conversation, 'ip1')
        ip2 = _get_ip_string(raw_conversation, 'ip2')

        _init_if_not_in(ip2, conversations_index)
        # taking the ip2 point of view
        conversation_ip2 = conversations_index[ip2]
        # update with
        conversation_ip2.update({
            # ip1
            "with": {
                ip1: {
                    "received": {
                        "frames": raw_conversation['ip1_to_ip2_frames'],
                        "bytes":  raw_conversation['ip1_to_ip2_bytes']
                    },
                    "sent": {
                        "frames": raw_conversation['ip2_to_ip1_frames'],
                        "bytes":  raw_conversation['ip2_to_ip1_bytes']
                    }
                }
            },
            # extend total
            "total": {
                "received": {
                    "frames": conversation_ip2['total']['received']['frames'] + raw_conversation['ip1_to_ip2_frames'],
                    "bytes":  conversation_ip2['total']['received']['bytes']  + raw_conversation['ip1_to_ip2_bytes']
                },
                "sent": {
                    "frames": conversation_ip2['total']['sent']['frames'] + raw_conversation['ip2_to_ip1_frames'],
                    "bytes":  conversation_ip2['total']['sent']['bytes']  + raw_conversation['ip2_to_ip1_bytes']
                }
            }
        })
        conversation_ip2['total']['all']['frames'] = conversation_ip2['total']['sent']['frames'] + conversation_ip2['total']['received']['frames']
        conversation_ip2['total']['all']['bytes']  = conversation_ip2['total']['sent']['bytes']  + conversation_ip2['total']['received']['bytes']


        _init_if_not_in(ip1, conversations_index)
        # taking the ip2 point of view
        conversation_ip1 = conversations_index[ip1]
        # update with
        conversation_ip1.update({
            # ip2 (symmetrical to ip1: use the same objects)
            "with": {
                ip2: {
                    "received": c['with'][ip1]['sent'],
                    "sent":     c['with'][ip1]['received']
                }
            },
            # extend total
            "total": {
                "received": {
                    "frames": conversation_ip1['total']['sent']['frames'] + raw_conversation['ip2_to_ip1_frames'],
                    "bytes":  conversation_ip1['total']['sent']['bytes']  + raw_conversation['ip2_to_ip1_bytes']
                },
                "sent": {
                    "frames": conversation_ip1['total']['received']['frames'] + raw_conversation['ip1_to_ip2_frames'],
                    "bytes":  conversation_ip1['total']['received']['bytes']  + raw_conversation['ip1_to_ip2_bytes']
                }
            }
        })
        conversation_ip1['total']['all']['frames'] = conversation_ip1['total']['sent']['frames'] + conversation_ip1['total']['received']['frames']
        conversation_ip1['total']['all']['bytes']  = conversation_ip1['total']['sent']['bytes']  + conversation_ip1['total']['received']['bytes']

    return conversations_index

'''
Count packets in a pcap file
'''
def get_conversations_index(in_pcap_filename, filter_string=None):
    print "count packets/"
    filter_string = "" if filter_string is None else "," + filter_string

    header_size = 6 # 5 (fixed header) + 1 (fixed footer)
    cmd = "tshark -r \"{}\" -z conv,ip{} -q | tail -n +{} | head -n -1"
    cmd = cmd.format(in_pcap_filename, filter_string, header_size)

    parser_string = \
"{ip11:d}.{ip12:d}.{ip13:d}.{ip14:<d}<->{ip21:d}.{ip22:d}.{ip23:d}.{ip24:d}\
{ip2_to_ip1_frames:>d}{ip2_to_ip1_bytes:>d}\
{ip1_to_ip2_frames:>d}{ip1_to_ip2_bytes:>d}\
{total_frames:>d}{total_bytes:^d}"

    raw_conversation_list = exec_command(cmd, parser_string) # , verbose_mode = True)

    # re arrange results
    conversations_index = _to_conversations_index(raw_conversation_list)

    print "count packets\\"
    return conversations_index


'''
Filter a pcap file based on an ip address (both destination and source)
'''
def filter_pcap_by_ip(in_pcap_filename, out_pcap_filename, target_addr, filter_start_time = None, filter_end_time = None):
    filter_string = "tcp && (ip.src == {0} || ip.dst == {0})".format(target_addr)

    if filter_start_time is not None:
        time_string = time.strftime("%b %d, %Y %H:%M:%S.{0:.0f}".format((filter_start_time % 1)*(10**9)), time.localtime(filter_start_time))
        filter_string += " && frame.time >= \"{}\"".format(time_string)
    if filter_end_time   is not None:
        time_string = time.strftime("%b %d, %Y %H:%M:%S.{0:.0f}".format((filter_end_time % 1)*(10**9)), time.localtime(filter_end_time))
        filter_string += " && frame.time <= \"{}\"".format(time_string)


    cmd = "tshark -r {0} -R '{2}' -w {1}".format(in_pcap_filename, out_pcap_filename, filter_string)
    exec_command(cmd)

'''
Calculate the outgoing throughput given a pcap file and the source ip for each time_bin_size interval

@arg in_pcap_filename input pcap file
@arg target_addr source ip address
@arg time_bin_size (optional - default = 1 second) size of each interval (in seconds)
'''
def calculate_outgoing_throughput(in_pcap_filename, target_addr, time_bin_size = 1):
    print "calculate_outgoing_throughput/"

    # get transmission volume for each second
    cmd = "tshark -r {0} -q -z io,stat,{1},\"SUM(frame.len)frame.len && ip.src == {2}\",\"COUNT(frame)frame && ip.src == {2}\" | tail -n +8 | head -n -1 ".format(in_pcap_filename, time_bin_size, target_addr)
    parser_string = "{begin:f}-{end:f}{bytes:>d}{frames:^d}"

    # each value in list is a dictionary with begin and end time of the bin, number of bytes in bin and number of frames in bin
    volume_list = exec_command(cmd, parser_string)

    if time_bin_size != 1:
        throughput_list = []
        for volume in volume_list:
            throughput_list.append(volume / time_bin_size)
    else:
        throughput_list = volume_list

    print "calculate_outgoing_throughput\\"

    return throughput_list

#TODO write calculate_ingoing_throughput...


def calculate_losses(in_pcap_filename, target_addr, time_bin_size = 1):
    print "calculate_losses/"
    header_size = 17 # 5 (fixed header) + 10 (number of columns) + 2 (fixed header)
    cmd = \
"tshark -r {0} -q -z io,stat,{1},\
\"COUNT(tcp)tcp && ip.src == {2}\",\
\"COUNT(tcp.analysis.retransmission)tcp.analysis.retransmission && ip.src == {2}\",\
\"COUNT(tcp.analysis.duplicate_ack)tcp.analysis.duplicate_ack && ip.src == {2}\",\
\"COUNT(tcp.analysis.lost_segment)tcp.analysis.lost_segment && ip.src == {2}\",\
\"COUNT(tcp.analysis.fast_retransmission)tcp.analysis.fast_retransmission && ip.src == {2}\",\
\"COUNT(tcp)tcp && ip.dst == {2}\",\
\"COUNT(tcp.analysis.retransmission)tcp.analysis.retransmission && ip.dst == {2}\",\
\"COUNT(tcp.analysis.duplicate_ack)tcp.analysis.duplicate_ack && ip.dst == {2}\",\
\"COUNT(tcp.analysis.lost_segment)tcp.analysis.lost_segment && ip.dst == {2}\",\
\"COUNT(tcp.analysis.fast_retransmission)tcp.analysis.fast_retransmission && ip.dst == {2}\"\
| tail -n +{3} | head -n -1"\
.format(in_pcap_filename, time_bin_size, target_addr, header_size)

    parser_string = \
"{begin:f}-{end:f}\
{src_transmitted:>d}{src_retransmissions:>d}{src_duplicate_acks:>d}{src_lost_segments:>d}{src_fast_retransmissions:>d}\
{dst_transmitted:>d}{dst_retransmissions:>d}{dst_duplicate_acks:>d}{dst_lost_segments:>d}{dst_fast_retransmissions:^d}"

    losses_list = exec_command(cmd, parser_string) #, verbose_mode = True)
    print "calculate_losses\\"
    return losses_list


def calculate_ack_rtt(in_pcap_filename, target_addr, time_bin_size = 1):
    print "calculate_ack_rtt/"

    header_size = 11 # 5 (fixed header) + 4 (number of columns) + 2 (fixed header)
    cmd = \
"tshark -r {0} -q -z io,stat,{1},\
\"COUNT(tcp.analysis.ack_rtt)tcp.analysis.ack_rtt && ip.src == {2}\",\
\"MIN(tcp.analysis.ack_rtt)tcp.analysis.ack_rtt && ip.src == {2}\",\
\"MAX(tcp.analysis.ack_rtt)tcp.analysis.ack_rtt && ip.src == {2}\",\
\"AVG(tcp.analysis.ack_rtt)tcp.analysis.ack_rtt && ip.src == {2}\"\
 | tail -n +{3} | head -n -1"\
.format(in_pcap_filename, time_bin_size, target_addr, header_size)

    parser_string = "{begin:f}-{end:f}{count:>d}{min:>f}{max:>f}{avg:^f}"

    res_list = exec_command(cmd, parser_string, verbose_mode = True)
    print "calculate_ack_rtt\\"
    return res_list
