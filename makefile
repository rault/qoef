SHELL:=/bin/bash
MAKE:=/bin/make

## Set your virtual environment (used for tcconfig in net.netutils when you use `make run`)
VENV=/home/rigelk/.virtualenvs/qoefactory

## Set the default interface to constrain
#INTERFACE="vboxnet1"

## Choose btw sourcing the venv or specifying absolute path of your venv
PYTHON=$(VENV)/bin/python
#PYTHON=python

CMD=$(PYTHON) main.py --venv $(VENV)

help:
	@echo "VENV, INTERFACE and PYTHON variables."
	@echo ""
	@echo "    usage"
	@echo "        Shows QoEF's usage (--help)"
	@echo "    clean-pyc"
	@echo "        Remove python artifacts."
	@echo "    clean-log"
	@echo "        Remove log artifacts."
	@echo "    clean"
	@echo "        Combines cleaning commands."
	@echo '  # run [VENV] [INTERFACE] [PYTHON]'
	@echo '        Run the `main.py` entry point on your local machine.'
	@echo '        You might want to run directly main.py to use all its options.'
	@echo '        Run `./main.py --help` for more information'
	@echo '    virtualenv-create'
	@echo '        Creates a virtualenv with python2.7 in .env/ that you can source.'
	@echo '    mongo-start'
	@echo '        Starts the MongoDB without the rest of the testbed.'
	@echo '        For analysis of the data after collection.'
	@echo '    mongo-connect'
	@echo '        Connects to a running MongoDB (with or without a running testbed).'
	@echo ''
	@echo '    run-vm (not yet implemented)'
	@echo '        Build and run the vagrant machine.'
	@echo "    isort (not yet implemented)"
	@echo "        Sort import statements."
	@echo "    lint (not yet implemented)"
	@echo "        Check style with flake8."
	@echo "    test (not yet implemented)"
	@echo "        Run py.test"

run:
ifndef INTERFACE
	$(info Please, select a network interface:)
	$(CMD) $(shell cd /sys/class/net && select i in *; do echo $$i; break; done)
else
	$(CMD) $(INTERFACE)
endif

run-%:
	$(MAKE) -C "experiment."$$(echo $@ | cut -c 5-)

usage:
	$(CMD) --help

clean-pyc:
	find . -name '*.pyc' -delete
	find . -name '*.pyo' -delete
	find . -name '*~' -delete

clean-log:
	rm -r log

clean: clean-pyc clean-log

virtualenv-create:
	virtualenv -p `which python2.7` .env
	.env/bin/easy_install distribute
	.env/bin/pip install -r requirements.txt
	@echo ""
	@echo "You can now set VENV=.env in the makefile."

mongo-start:
	$(PYTHON) db_start.py

mongo-connect:
	mongo 127.0.0.1:8094/testset

mongo-clean:
	mongo 127.0.0.1:8094/testset --eval "db.dropDatabase()"

