from __future__ import print_function
from pymitter import EventEmitter

from websocket.SimpleWebSocketServer import WebSocket

from utils.dotdict import dotdict
from utils import notices
import time
import logging

#TODO improve callback handling (possibility to remove it,...) - N.B.: must be synchronized! (I can't remove a callback when it could be utilized - @see http://stackoverflow.com/questions/4625182/python-lock-method-annotation)
"""
Binds an EventEmitter to the WebSocket class in order to receive events to the external of the SimpleWebSocketServer
"""
__flyweight_collection = dict()
def get_EventBasedWebSocket_class(ee = EventEmitter(), callback = None):

    class _EventBasedWebSocket(WebSocket):

        def __init__(self, server, sock, address):
            try:
                super(_EventBasedWebSocket, self).__init__(server, sock, address)

                self.ee = ee # Injected from outside
                self.is_connected = False

                ee.on(
                    EventBasedWebSocket.EVENTS.MESSAGE__SENDING_REQUESTED, 
                    lambda evt_args: self._on_message_sending_requested(evt_args)
                )

                ee.emit(
                    EventBasedWebSocket.EVENTS.INIT, 
                    self._build_evt_args()
                )

                # used to communicate outside the SimpleWebSocketServer (that hides completely this class)
                if callback is not None:
                    callback(self)
            except Exception:
                import traceback
                print("Exception in _EventBasedWebSocket __init__")
                print(traceback.format_exc())


        # @override
        def handleMessage(self):
            evt_args         = self._build_evt_args()
            evt_args['data'] = self.data
            
            logging.info("message received -> "+self.data)

            ee.emit(EventBasedWebSocket.EVENTS.MESSAGE__RECEIVED, evt_args)

        # @override
        def handleConnected(self):
            self.is_connected = True

            evt_args            = self._build_evt_args()
            evt_args['address'] = self.address
            
            notices.clientConnected("ws server",evt_args['address'])

            ee.emit(EventBasedWebSocket.EVENTS.CONNECTED, evt_args)

        # @override
        def handleClose(self):
            self.is_connected = False

            evt_args            = self._build_evt_args()
            evt_args['address'] = self.address

            ee.emit(EventBasedWebSocket.EVENTS.CLOSED, evt_args)

        # @override
        def sendMessage(self, data):
            evt_args         = self._build_evt_args()
            evt_args['data'] = data
            logging.info("[__"+self.__class__.__name__+"__]: starting call with data: %s",evt_args)
            super(_EventBasedWebSocket, self).sendMessage(data)

            ee.emit(EventBasedWebSocket.EVENTS.MESSAGE__SENT, evt_args)

        def _build_evt_args(self):
            return {
                'source':    self,
                'timestamp': time.time(),
                'server':    self.server # if hasattr(self, 'server') else None # source server (to be injected via callback)
            }

        def _on_message_sending_requested(self, evt_args):
            if evt_args['target'] is self and 'data' in evt_args:
                self.sendMessage(evt_args['data'])
            else:
                logging.info("[__"+self.__class__.__name__+"__]: cancelling call because target not self...")


    
    _EventBasedWebSocket.EVENTS = EventBasedWebSocket.EVENTS

 # returns the instance in collection if present or put a new one and then returns it
    return __flyweight_collection.setdefault((ee, callback), _EventBasedWebSocket)

# 
def EventBasedWebSocket(ee = EventEmitter(), callback = None):
    return get_EventBasedWebSocket_class(ee, callback)

EventBasedWebSocket.EVENTS = dotdict({})
EventBasedWebSocket.EVENTS.INIT                       = "websocket__init"
EventBasedWebSocket.EVENTS.CONNECTED                  = "websocket__connected"
EventBasedWebSocket.EVENTS.MESSAGE__RECEIVED          = "websocket__message_received"
EventBasedWebSocket.EVENTS.MESSAGE__SENDING_REQUESTED = "websocket__message_sending_request"
EventBasedWebSocket.EVENTS.MESSAGE__SENT              = "websocket__message_sent"
EventBasedWebSocket.EVENTS.CLOSED                     = "websocket__closed"
