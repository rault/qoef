
from threading import Thread

from websocket.SimpleWebSocketServer import SimpleWebSocketServer
from websocket.EventBasedWebSocket   import get_EventBasedWebSocket_class, EventBasedWebSocket


"""
Initialize an Event Based WebSocket Server

The returned object has to be started (call start()) to be active

@arg host String
@arg port Int
@arg ee   EventEmitter Nodejs like EventEmitter to receive events from the WebSocket (and the WebSocket itself to send messages)
@arg callback Callable (optional)
@arg name string (optional) Thread name
"""
class OotbWebSocketServer(object):
    def __init__(self, host, port, ee, callback = None, name = 'websocket_server'):
        self.host = host
        self.port = port
        self.ee = ee

        self._callback = self._setup_callback(callback)
        self._server_impl = SimpleWebSocketServer(host, port, get_EventBasedWebSocket_class(ee, callback=callback))

        self._th = Thread(target = self._server_impl.serveforever, name = name)

    def _setup_callback(self, callback = None):

        def bind_server_socket(ws):
            ws.server = self._server_impl

        if callback is None:
            # bind server to socket
            return bind_server_socket
        else:
            def on__websocket_created(ws):
                bind_server_socket(ws)
                callback(ws)

            return on__websocket_created

    def start(self):
        self._th.start()

    def stop(self):
        self._server_impl.close()

    def join(self):
        try:
            self._th.join()
        except:
            pass
