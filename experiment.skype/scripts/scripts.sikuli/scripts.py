###### Jython 2.7 program #####
## It assumes you have a camera
## and microphone, otherwise the
## Skype connection test will show
## a red flag whatever the real
## quality of the connection is.

## Todo:
## - auto answer call (not programmatically done, just modify the parameters manually inside the Skype that receives calls)
## - save image to local folder (done)
## - communicate with orchestrer? (done)
## - use the BindIp.cmd from https://superuser.com/questions/1004697/bind-application-to-a-specific-network-interface/1008389#1008389
##   to force the two Skype instances
##   to use different network adapters.

## Improvement possible: store login in an
## external .jar:
## storing Skype login credentials at external Java class
## This Java class has 2 methods, for retreiving skype login and skype password.
## Info is hardcoded in Java class and compiled as Java bytecode.
## Loading from Java class would prevent having login/password stored in
## plain text in this Python script.
##
## Please refer to SkypeCredentials.java for details how to prepare your
## skype login/password info.
##

## Setting the JYTHONPATH/PYTHONPATH
## So that modules installed via pip (namely ws4py and enum34)
## and local modules (client.py) are found at runtime, you need
## to either set the variable in the Windows terminal/env or set
## it programmatically here:
#sys.path.append("E:\programmes\Python2.7.13\Lib\site-packages")
sys.path.append(r"C:\Python27\Lib\site-packages")
#sys.path.append(r"E:\Documents\SikuliX\scripts.sikuli")
#sys.path.append(r"\\VBOXSVR\Barakat\qoe_1\experiment.skype\scripts\scripts.sikuli")
sys.path.append(r"\\VBOXSVR\vagrant\scripts\scripts.sikuli")
#sys.path.append(sys.path[0])
## ex. for setting the env variable in Windows:
# set JYTHONPATH=E:\programmes\Python2.7.13\Lib\site-packages;E:\Documents\SikuliX\scripts.sikuli

from client import Server, Client

import subprocess
import logging
import shutil
import os


def doCall(skypeIdCalling, userameToCall, secondsInCall = 120, experimentNumber = None ):
    # Path to Skype executable: 
    PathSkype = "C:\Program Files (x86)\Skype\Phone\Skype.exe"
    
    # Open Skype if not already there
    Skype = App("Skype")
    if not Skype.isRunning():
        App.open(PathSkype)
        while not Skype.isRunning():
            wait(5)

    App.focus("SkypeT? - "+skypeIdCalling) # the Skype ID with which you call is shown in the windows title and necessary to get the Skype window
    wait(3)
    
    logging.info("-- now starting the search inside Skype --")

    def hasCallHungUp():
        try:
            find("1495807896249.png")
            return True 
        # this icon cannot be seen while in call, so if we find it, returns True
        except:
            return False

    def doCallHangUp( hidden = False ):
        try:
            if hidden:
                hover(Pattern("1497233952636.png").similar(0.50))
                wait(1)
            return click("1495806515223.png")
        except:
            if not hidden:
                doCallHangUp( hidden = True )
            else:
                return False
    
    # We look for the user which we want to talk to
    try: 
         search = wait("1495806439807.png")
    except:
         print "couldn't find symbol [can still be in call from previous failed experiment!]"
         doCallHangUp()
    
    search.click()
    search.paste( userameToCall )
    search.below(60).click()
    
    # We call the user
    try:
         click("1495806451224.png")
    except:
         print "couldn't find symbol"
         return -1
    
    print "-- now calling the user --"
    # We wait within the 
    wait( secondsInCall )

    if not hasCallHungUp():
	    try:
	        hover(Pattern("1497233952636.png").similar(0.50))
	        
	        meterRegion = find("1496069066845.png")
	        
	    except:
                print "Unable to capture the indicator"
                return -1
	    pathToMeterScreen = capture(meterRegion)
	    newPathToMeterScreen = os.path.join(getBundlePath(), 
	            'captures', 
	            experimentNumber+".png" if experimentNumber else os.path.basename(pathToMeterScreen)
	            )
	    
	    # Actually moving the file to the script directory under "captures"
	    shutil.move(pathToMeterScreen, newPathToMeterScreen)
	    print newPathToMeterScreen
	    doCallHangUp()
	    return newPathToMeterScreen
    else:
        print "Error in finding a indicator"
        return '' # empty path is equivalent to False when str is tested

def getCallQuality(pathToImage):
    
    if pathToImage:
        cmd = r'python \\VBOXSVR\vagrant\scripts\scripts.sikuli\image_rec.py '+pathToImage
        my_env = os.environ
        my_env["C:\Python27"] = 'C:\Python27'#path to python enviroment
        proc = subprocess.Popen(cmd ,bufsize=0, executable=None, stdin=None, stdout=subprocess.PIPE, stderr=subprocess.PIPE, preexec_fn=None, close_fds=True, shell=True, env=my_env)
        quality_int = str(proc.communicate(proc.stdout))
        #f = open(r'C:\Script\Pierre\scripts.sikuli\text.txt', 'a')
        #f.write(quality_int[2] + '\n') 
        #f.close()
        print "CQI value => {} <=".format(quality_int[2])
        #for i in quality_int:
        #    print i
        return int(quality_int[2].strip())
    else:
        return -1

################################################################################
################################ MAIN ##########################################
################################################################################

# /!\ be careful, you cannot use the following block to make your main functions
# if __name__ == "__main__":
# It's because the scope of SikuliX is different, and __name__ isn't __main__

#1 setting up the logging (otherwise it won't show)
logging.getLogger().setLevel(logging.DEBUG)

#2 server setup
ws_server = Server(
    {
    "Experiment__ReadyToStart": { "code":  1, "type": "experiment" , "text": "experiment ready to start" },
    "Experiment__Completed":    { "code":  2, "type": "experiment" , "text": "experiment completed" },
    "Experiment__Error":        { "code":  3, "type": "experiment" , "text": "experiment failed" }
    }
)
#3 setup ws client
logging.info("[__MAIN__]: ws client setup")
ws = Client('ws://192.168.57.1:8092', ws_server)

#4 define callback
def callback():
    try:
        return getCallQuality(doCall( "pierre-antoine.rault", "Corentin Maj", secondsInCall = 30 ))
    except:
        return -1

ws.setup(experiment_callback = callback)
