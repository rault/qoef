import sys
import os
from PIL import Image
import time
from enum import Enum


class Indicator(Enum):
    GOOD   = 1
    MEDIUM = 2
    POOR = 3
    FAIL = 4

def getImageQuality(pathToImage):
	img =  Image.open(pathToImage)#input only red image
	red = False 
	white = False
	yellow = False
	nothing = False

	good = False
	medium = False
	poor = False
	no_connect = False
	#COLORS = img.convert('RGB').getcolors()
	#print COLORS

	for i,COLORS in img.convert('RGB').getcolors(): #get colors prints tuples (howmanytimes,(R,G,B))
		#Captured picture will contain a lot of colors so if we will have red/white/yellow/nothing
		if (255, 0, 0) == COLORS:
		# print 'red'
		 red = True
		if (255, 255, 255) == COLORS:
		 white = True
		if (34, 34, 34) == COLORS:
		 yellow = True
		else:
		 nothing = True
	#print COLORS
	if ((white == True) & (nothing == True)):#since our image will consist defenetly from color that we are searching for and different we need to have this cases
		good = True 
		#print 'Good'
		return Indicator.GOOD.value
	elif  ((red == True) & (nothing == True)):
		poor = True
		#print 'Poor'
		return Indicator.POOR.value
	elif  ((yellow == True) & (nothing == True)):
		medium = True
		#print 'MEDIUM'
		return Indicator.MEDIUM.value
	elif ((nothing == True) & (red == False) & (white == False) & (yellow == False)):#when we won't have red,white,yellow it means that we don't have connection
		no_connect = True
		#print 'No connection'
		return Indicator.FAIL.value
		

print getImageQuality(os.path.normpath(sys.argv[1]))
