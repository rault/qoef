from app.setup_app_configuration    import get_configuration
from app.setup_db                   import start_db_client

# from enum import Enum
#
# class Fields(Enum):
#     ID = 1
#     SESSION_ID = 2
#     TIME = 3
#
#     LABEL = 4
#     DOWNLOAD_THROUGHPUT = 5
#     DOWNLOAD_LOSSRATE = 6
#     RTT = 7
#
#     INFERRED = 8
#
# fields_map = {
#     Fields.ID: "_id",
#     Fields.SESSION_ID: "session_id",
#     Fields.TIME: "timestamp",
#     Fields.LABEL: "label",
#     Fields.DOWNLOAD_THROUGHPUT: "point.download_throughput",
#
# }
#
# TO_PRINT = [Fields.ID, Fields.SESSION_ID, Fields.TIME, Fields.LABEL, Fields.DOWNLOAD_THROUGHPUT, Fields.DOWNLOAD_LOSSRATE, Fields.RTT, Fields.INFERRED]

configuration = get_configuration()
(conn, db, collections) = start_db_client(configuration['db']['host'], configuration['db']['port'], configuration['db']['name'], configuration['db']['collections'])

#print header
print "id, session_id, timestamp, inferred, point.label, point.download_throughput, point.download_lossrate, point.jitter, point.rtt"

_filter = {}
entries = collections['labeled_points'].find(_filter)
for entry in entries:
    print "{},{},{},{},{},{},{},{},{}".format(
        entry['_id'],
        entry['session_id'],
        entry['timestamp'],
        True if 'inferred' in entry and entry['inferred'] else False,
        entry['point']['label'],
        entry['point']['download_throughput'],
        entry['point']['download_lossrate'],
        entry['point']['jitter'],
        entry['point']['rtt']
    )

try:
    db.stop()
except:
    pass
