'''
Code generator to visualize data in a 3D scatter plot
'''
import sys
from copy import deepcopy
from datetime import datetime

################################################################################
# CONFIGURATION
## output
OUT_PATH = "./"
OUT_NAME = "all_test" if len(sys.argv) == 1 else sys.argv[1]

## input (db)
DB_HOST = "localhost"
DB_PORT = 8094
DB_NAME = "testset"
DB_COLLECTIONS = {
    "labeled_points": 	"test_labeled",
    "samples": 			"test_samples"
}

#TODO define query_extension as an object with point.label inside
DATA_SERIES = [
    {
        'name': "Good",
        'label': 0
    },
    {
        'name': "Bad",
        'label': { '$gt': 0 }
    }
]

#TODO implement support for axis (this should also filter the query results)
AXIS = [
    {
        'name': "Download Throughput",
        'collection': "labeled_points",
        'collection_key': "point.download_throughput",
        'min': 0,
        'max': 10000
    },
    {
        'name': "RTT",
        'collection': "labeled_points",
        'collection_key': "point.rtt",
        'min': 0,
        'max': 5000
    },
    {
        'name': "Download Loss Rate",
        'collection': "labeled_points",
        'collection_key': "point.download_lossrate",
        'min': 0,
        'max': 0.5
    }
]

def get_data(collections, series):
    query = {}
    # query = { '__inferred__': { '$exists': True} } # remove inferred points
    # query = { 'timestamp': { '$gt': datetime(2015, 8, 27, 0, 0, 0) }, '__inferred__': { '$exists': False } }
    # query = {'session_id': "e1440848899"}

    result = perform_query(collections, series, query)

    return result

################################################################################

import pymongo

import logging

def start_db_client(host, port, name, collection_names):
    # setup connection
    try:
        logging.info("[__MAIN__]: DB client connecting")
        conn = pymongo.MongoClient(host, port)
        logging.info("[__MAIN__]: DB client connected")
        logging.info("[DB]: using database \"{}\"\n".format(name))
    except pymongo.errors.ConnectionFailure, e:
       logging.info("[__MAIN__]: DB client cannot connect: %s" % e)
       raise RuntimeException("DB Connection Failure")

    # setup db
    db = conn[name]

    # setup collections
    collections = {
        'labeled_points':   db[collection_names['labeled_points']],
        'samples':          db[collection_names['samples']]
    }

    return (conn, db, collections)

def perform_query(collections, series, query = {}):
    result = {}

    for serie in series:
        _q = deepcopy(query)
        _q['point.label'] = serie['label']

        result[serie['name']] = collections['labeled_points'].find(_q)

    return result

def write_point(fp, value):
    fp.write("[{},{},{}]".format(
        value['point']['download_throughput'],
        value['point']['rtt'],
        value['point']['download_lossrate']
    ))

def write_dataserie(fp, name, values):
    fp.write("{{ 'name': \"{}\", 'data': [".format(name))

    counter = values.count() - 1
    for value in values:
        write_point(fp, value)

        if counter > 0:
            fp.write(',')
        counter -= 1

    fp.write("] }")

def write_dataseries(fp, series, var_name = None):
    if var_name is not None:
        fp.write("var {} = ".format(var_name))

    fp.write("[\n")

    counter = len(series) - 1
    for name, values in series.iteritems():
        write_dataserie(fp, name, values)

        if counter > 0:
            fp.write(',\n')
        counter -= 1
    fp.write("\n]\n")


if __name__ == "__main__":

    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    logging.getLogger().addHandler(console)

    (conn, db, collections) = start_db_client(DB_HOST, DB_PORT, DB_NAME, DB_COLLECTIONS)


    logging.info("[__MAIN__]: writing json data file")
    dataseries = get_data(collections, DATA_SERIES)
    data_filepath = OUT_PATH + OUT_NAME + '.json'
    with open(data_filepath, 'w') as fp:
        write_dataseries(fp, dataseries)
    logging.info("[__MAIN__]: json data file created successfully")

    logging.info("[__MAIN__]: writing JavaScript data file")
    dataseries = get_data(collections, DATA_SERIES)
    js_data_filepath = OUT_PATH + OUT_NAME + '.js'
    var_name = "series"
    with open(js_data_filepath, 'w') as fp:
        write_dataseries(fp, dataseries, var_name)
    logging.info("[__MAIN__]: JavaScript data file created successfully (var name is {})".format(var_name))

    logging.info("[__MAIN__]: writing HTML file")
    with open(OUT_PATH + 'template.html', 'r') as template:
        with open(OUT_PATH + OUT_NAME + '.html', 'w') as fp:
            for line in template:
                # print line[:-2] + ' == ' + "<script src=\"{{}}\"></script> ? {}".format(line[:-2] == "<script src=\"{}\"></script>")

                if line[:-2] == "<script src=\"{}\"></script>":
                    line = "<script src=\"{}\"></script>\r\n".format(js_data_filepath)

                fp.write(line)
    logging.info("[__MAIN__]: HTML file created successfully")
