iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
chocolatey feature enable -n allowGlobalConfirmation
#choco install skype ## cannot be downloaded, as the version installed lacks important dlls. Rather use the ruby script provided along on your host
choco install sikulix
choco install python --version 2.7.2

function download_file([string]$url, [string]$d) {
	# Downloads a file if it doesn't already exist
	if(!(Test-Path $d -pathType leaf)) {
        $client = New-Object System.Net.WebClient
		# get the file
		write-host "Downloading $url to $d";
		$client.DownloadFile($url, $d);
	}
}

function get_pip {
	write-host "Installing pip"
	$setuptools_url = "https://bootstrap.pypa.io/get-pip.py"
	$get_pip = "get_pip.py"
	download_file $setuptools_url $get_pip
	python $get_pip
}

get_pip
