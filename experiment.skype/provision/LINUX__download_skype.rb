# -*- mode: ruby -*-
# vi: set ft=ruby :


SPECIFIC_SKYPE_VERSION="7.34.0.102"

Vagrant.require_version ">= 1.3.5"
if Vagrant::VERSION < '1.6.0'
  Vagrant.require_plugin "vagrant-windows"
end

# checks wether a SkypeSetup.msi locally exists to provision Windows guests.
unless (Dir.glob('./provision/binaries/SkypeSetup*.msi').any? && 
	((defined? SPECIFIC_SKYPE_VERSION)? Dir.glob("./provision/binaries/SkypeSetup_#{SPECIFIC_SKYPE_VERSION}.msi").any? : true))
  puts 'No Skype corresponding (if version speficied, the latest) installer was provided.'
  puts 'The latest/corresponding SkypeSetup.msi will be downloaded at http://www.skype.com/go/getskype-msi'
  
  tmp = (defined? SPECIFIC_SKYPE_VERSION)? "_#{SPECIFIC_SKYPE_VERSION}" : ""
  `wget --no-check-certificate -O ./provision/binaries/SkypeSetup#{tmp}.msi http://www.skype.com/go/getskype-msi`
end
