# -*- mode: ruby -*-
# vi: set ft=ruby :


SPECIFIC_SKYPE_VERSION="7.34.0.102"

Vagrant.require_version ">= 1.3.5"
if Vagrant::VERSION < '1.6.0'
  Vagrant.require_plugin "vagrant-windows"
end

# checks wether a SkypeSetup.msi locally exists to provision Windows guests.
unless (Dir.glob('./provision/binaries/SkypeSetup*.msi').any? && 
	((defined? SPECIFIC_SKYPE_VERSION)? Dir.glob("./provision/binaries/SkypeSetup_#{SPECIFIC_SKYPE_VERSION}.msi").any? : true))
  puts 'No Skype corresponding (if version speficied, the latest) installer was provided.'
  puts 'The latest/corresponding SkypeSetup.msi will be downloaded at http://www.skype.com/go/getskype-msi'
  
  tmp = (defined? SPECIFIC_SKYPE_VERSION)? "_#{SPECIFIC_SKYPE_VERSION}" : ""
  `wget --no-check-certificate -O ./provision/binaries/SkypeSetup#{tmp}.msi http://www.skype.com/go/getskype-msi`
end


Vagrant.configure("2") do |config|
  # All Vagrant configuration is done here. The most common configuration
  # options are documented and commented below. For a complete reference,
  # please see the online documentation at vagrantup.com.

  # Every Vagrant virtual environment requires a box to build off of.
  config.vm.box = "windows10"
  config.vm.boot_timeout = 400
  config.vm.graceful_halt_timeout = 400
  config.winrm.username = "IEUser"
  config.winrm.password = "Passw0rd!"
  
  # The url from where the 'config.vm.box' box will be fetched if it
  # doesn't already exist on the user's system.
  # See available boxes on http://www.vagrantbox.es/
  config.vm.box_url = "http://aka.ms/msedge.win10.vagrant"

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network :forwarded_port, guest: 80, host: 8080
  config.vm.network :forwarded_port, guest: 5985, host: 59851,
        id: "winrm", auto_correct:true
  # Ouverture du port du remote desktop protocol
  config.vm.network :forwarded_port, guest: 3389, host: 33891,
        id: "rdp", auto_correct:true

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  config.vm.network :private_network, type: "dhcp" #ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  config.vm.network :public_network

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  config.vm.synced_folder "./", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider :virtualbox do |vb|
    # Don't boot with headless mode
    vb.gui = true

    # Use VBoxManage to customize the VM. For example to change memory:
    vb.customize ["modifyvm", :id, "--memory", "2048"]
    vb.customize ["modifyvm", :id, "--cpus", "2"]
    vb.customize ["modifyvm", :id, "--audio", "none"]
    vb.customize ["modifyvm", :id, "--usb", "off"]
    vb.customize ['modifyvm', :id, '--clipboard', 'bidirectional']
  end

  config.vm.provider :vmware_fusion do |vb|
    # Don't boot with headless mode
    vb.gui = false
    
    # customize
    vb.vmx["memsize"] = "2048"
    vb.vmx["numvcpus"] = "2"
    vb.vmx["usb.present"] = "false"
    vb.vmx["sound.present"] = "false"
    vb.vmx["ide1:0.present"] = "false"
  end
  
  # In 2014, Windows support was added in v1.6.0 via the winrm comm.
  # Before that version we assume (we have checked previously) that
  # the vagrant-windows plugin is used, which includes WinRM comm.
  if Vagrant::VERSION >= '1.6.0'
    config.vm.communicator = "winrm"
    config.vm.guest = :windows
    config.winrm.transport = :plaintext
    config.winrm.basic_auth_only = true
  end
  #
  # View the documentation for the provider you're using for more
  # information on available options.

  # Enable provisioning with Batch scripts in a relative directory.
  #Dir.glob("provision/*.bat").each do |script|
  #  config.vm.provision "shell", :path => script, privileged: true
  #end
  #config.vm.provision :shell, path: "provision/choco.ps1", :powershell_elevated_interactive => true

  # Enable provisioning with Puppet stand alone.  Puppet manifests
  # are contained in a directory path relative to this Vagrantfile.
  # You will need to create the manifests directory and a manifest in
  # the file precise32_spw.pp in the manifests_path directory.
  #
  # An example Puppet manifest to provision the message of the day:
  #
  # # group { "puppet":
  # #   ensure => "present",
  # # }
  # #
  # # File { owner => 0, group => 0, mode => 0644 }
  # #
  # # file { '/etc/motd':
  # #   content => "Welcome to your Vagrant-built virtual machine!
  # #               Managed by Puppet.\n"
  # # }
  #
  # config.vm.provision :puppet do |puppet|
  #   puppet.manifests_path = "manifests"
  #   puppet.manifest_file  = "init.pp"
  # end

  # Enable provisioning with chef solo, specifying a cookbooks path, roles
  # path, and data_bags path (all relative to this Vagrantfile), and adding
  # some recipes and/or roles.
  #
  # config.vm.provision :chef_solo do |chef|
  #   chef.cookbooks_path = "../my-recipes/cookbooks"
  #   chef.roles_path = "../my-recipes/roles"
  #   chef.data_bags_path = "../my-recipes/data_bags"
  #   chef.add_recipe "mysql"
  #   chef.add_role "web"
  #
  #   # You may also specify custom JSON attributes:
  #   chef.json = { :mysql_password => "foo" }
  # end

  # Enable provisioning with chef server, specifying the chef server URL,
  # and the path to the validation key (relative to this Vagrantfile).
  #
  # The Opscode Platform uses HTTPS. Substitute your organization for
  # ORGNAME in the URL and validation key.
  #
  # If you have your own Chef Server, use the appropriate URL, which may be
  # HTTP instead of HTTPS depending on your configuration. Also change the
  # validation key to validation.pem.
  #
  # config.vm.provision :chef_client do |chef|
  #   chef.chef_server_url = "https://api.opscode.com/organizations/ORGNAME"
  #   chef.validation_key_path = "ORGNAME-validator.pem"
  # end
  #
  # If you're using the Opscode platform, your validator client is
  # ORGNAME-validator, replacing ORGNAME with your organization name.
  #
  # If you have your own Chef Server, the default validation client name is
  # chef-validator, unless you changed the configuration.
  #
  #   chef.validation_client_name = "ORGNAME-validator"
end
