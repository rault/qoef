# Fourier Amplitude Sensitivity Test (FAST) - point generator
from SALib.sample import fast_sampler

"""
Converts a space definition (dict<name:String, AxisDefinition{ 'begin': float, 'end': float}> ) to the format used in the FAST library
"""
# example of axis format:
# {
# 	"download_throughput": 	{ "begin": 0, "end": 10000 },
# 	"download_lossrate": 	{ "begin": 0, "end": 0.5 },
# 	"rtt": 					{ "begin": 0, "end": 5000 }
# }
def configuration_to_SALib(axis):
	names 	= []
	bounds 	= []

	for name, ax in axis.iteritems():
		names.append(name)
		bounds.append([ax['begin'], ax['end']])

	return { 'names': names, 'bounds': bounds, 'num_vars': len(bounds) }

def get_fast_generator(axis, Y = 1000, M = 4, continuous = False):
    return FastIterator(
        configuration_to_SALib(axis),
        Y,
        M,
        continuous
    )

class FastIterator(object):
    """Fast Iterable Sampling Points Generator"""
    def __init__(self, axis, Y = 1000, M = 4, continuous = False):
        self.axis = axis
        self.Y = Y
        self.M = M
        self.continuous = continuous

        self.points = fast_sampler.sample(axis, Y, M)
        self._iter = iter(self.points)

    def __iter__(self):
        return self

    def next(self):
        try:
            point = self._iter.next()

            out = {}

            for i, name in enumerate(self.axis['names']):
                out[name] = point[i]

            return out
        except StopIteration:
            if self.continuous is True:
                # start new iteration
                self.points = fast_sampler.sample(self.axis, self.Y, self.M)
                self._iter = iter(self.points)
                return next(self)

            # end of samples
            return
