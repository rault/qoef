# Timed Event Generator in context manager
from threading import _Timer
import time
import traceback

#TODO thread_manager support?
'''
Define a Context Manager where a timed action can occour
'''
class TimedContext(_Timer):
    def __init__(self, timeout_secs, timeout_handler, name = None, args=[], kwargs={}):
        self.timeout_handler = timeout_handler
        self.timeout_secs    = timeout_secs

        super(TimedContext, self).__init__(
            timeout_secs, timeout_handler, args=args, kwargs=kwargs
        )

        if name is not None:
            self.setName(name)

    def enter(self):
        self.start()
        print "[TIMER-{}] started".format(self.name)

    def exit(self):
        self.cancel()
        print "[TIMER-{}] stop".format(self.name)

    def __enter__(self):
        self.enter()
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        if exc_type is not None:
            #print exc_type, exc_value, traceback # display exception (if any)
            print traceback.format_exc()

        self.exit()
        return True # suppress exceptions (if any)


'''
Event Based Context Manager (launch an event in the EventEmitter after the timeout)

@arg ee EventEmitter (see from pymitter import EventEmitter)
'''
class EventTimedContext(TimedContext):
    def __init__(self, ee, timeout_secs, timeout_event_name = "TIMEOUT", thread_manager = None, name = None):
        self.ee                 = ee
        self.timeout_event_name = timeout_event_name

        _self = self
        def timeout_handler(evt_args = {}):
            print "[TIMER-{}]: raising event {}".format(timeout_event_name, self.name)
            _self.ee.emit(
                _self.timeout_event_name,
                {
                    'source': _self, 'timestamp': time.time()
                }
            )

        self.timeout_handler = timeout_handler

        super(EventTimedContext, self).__init__(timeout_secs, timeout_handler, name)
